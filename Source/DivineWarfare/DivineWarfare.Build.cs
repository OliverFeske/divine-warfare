// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class DivineWarfare : ModuleRules
{
	public DivineWarfare(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
		
		//~ Begin Engine Dependencies
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core", 
				"CoreUObject", 
				"Engine", 
				"InputCore",
				"GameplayAbilities",
				"GameplayTags",
				"GameplayTasks",
				"EnhancedInput",
				"ModularGameplay",
				"ModularGameplayActors",
				"MotionWarping",
				"SunPosition",
				"OFSaveSystem",
				"OFGameDifficulty",
				"OFThirdPersonCamera",
				"OFGameplayAbilitySystemCore",
				"OFInventorySystem",
				"OFWeaponSystem"
			});
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"GameFeatures",
				"RHI",
				"RenderCore",
				"SlateCore",
			});
		//~ End Engine Dependencies
		
		//~ Begin Project Dependencies
		PublicIncludePaths.AddRange(
			new string[]
			{
				"DivineWarfare/Public/Characters",
				"DivineWarfare/Public/Characters/Hero",
				"DivineWarfare/Public/Characters/Movement",
				"DivineWarfare/Public/Core",
				"DivineWarfare/Public/GameplayAbilitySystem",
				"DivineWarfare/Public/GameplayAbilitySystem/Core",
				"DivineWarfare/Public/GameplayAbilitySystem/Core/Attributes",
				"DivineWarfare/Public/GameplayAbilitySystem/Core/EffectCalculations",
				"DivineWarfare/Public/Lighting",
				"DivineWarfare/Public/SaveSystem",
			});
		
		PrivateIncludePaths.AddRange(
			new string[]
			{
			});
		//~ End Project Dependencies

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true

		OptimizeCode = CodeOptimization.Never;
	}
}

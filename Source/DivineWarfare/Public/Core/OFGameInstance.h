﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "CoreMinimal.h"
#include "OFGameDifficultyStructs.h"
#include "Engine/GameInstance.h"
#include "OFGameInstance.generated.h"

class UOFDivineWarfareSaveGame;

UCLASS(Config = OFGameInstance)
class UOFGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void ChangeGameDifficulty(EOFGameDifficulty NewDifficulty) { GameDifficulty = NewDifficulty; }
	
	EOFGameDifficulty GetGameDifficulty() const { return GameDifficulty; }
	
protected:
	//~ Begin UGameInstance interface
	virtual void Init() override;
	virtual void Shutdown() override;
	//~ End UGameInstance interface

	UPROPERTY(BlueprintReadOnly)
	UOFDivineWarfareSaveGame* CurrentLoadedSaveGame = nullptr;

	UPROPERTY(BlueprintReadOnly, Config, SaveGame)
	EOFGameDifficulty GameDifficulty = EOFGameDifficulty::Normal;
};
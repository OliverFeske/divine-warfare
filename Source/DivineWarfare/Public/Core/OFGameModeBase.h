﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ModularGameMode.h"
#include "OFGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DIVINEWARFARE_API AOFGameModeBase : public AModularGameModeBase
{
	GENERATED_BODY()
	
public:
	//~ Begin AGameModeBase Interface
	virtual void StartPlay() override;
	//~ End AGameModeBase Interface

	UFUNCTION(BlueprintCallable)
	void ToggleGameFeaturePlugin(FString GameFeaturePluginName, bool bEnable);

};

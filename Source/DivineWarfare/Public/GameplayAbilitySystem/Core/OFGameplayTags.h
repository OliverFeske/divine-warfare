﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#define TAG_EFFECT_DAMAGE FGameplayTag::RequestGameplayTag(TEXT("Effect.Damage"), true)
#define TAG_EFFECT_DAMAGE_CRITICALHIT FGameplayTag::RequestGameplayTag(TEXT("Effect.Damage.CriticalHit"), true)
#define TAG_EFFECT_REMOVEONDEATH FGameplayTag::RequestGameplayTag(TEXT("Effect.RemoveOnDeath"), true)

#define TAG_STATE_DEAD FGameplayTag::RequestGameplayTag(TEXT("State.Dead"), true)
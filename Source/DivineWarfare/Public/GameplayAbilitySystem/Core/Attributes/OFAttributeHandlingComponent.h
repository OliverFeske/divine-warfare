﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameplayEffectTypes.h"
#include "OFAttributeHandlingComponent.generated.h"

class UGameplayEffect;
class UOFAttributeSet;
class UAbilitySystemComponent;
class UOFAbilitySystemComponent;

struct FOnAttributeChangeData;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class DIVINEWARFARE_API UOFAttributeHandlingComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UOFAttributeHandlingComponent();

	void Initialize(UAbilitySystemComponent* AbilitySystemComponent, UOFAttributeSet* AttributeSet);

	const FString& GetAttributeSetName() const { return AttributeSetName; }

protected:
	/** The name of the Attribute Set this component will set up teh listeners for. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FString AttributeSetName = TEXT("Default");
	
	UPROPERTY()
	UOFAttributeSet* AttributeSet;

	UPROPERTY()
	UAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY()
	UOFAbilitySystemComponent* OFAbilitySystemComponent;

	bool bStartupEffectsApplied = false;

	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Level
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UFUNCTION(BlueprintCallable, Category = "OFAttributeHandlingComponent|Level")
	int32 GetLevel() const;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLevelChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLevelIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLevelDecreased, float, OldValue, float, NewValue);

	UPROPERTY(BlueprintAssignable)
	FOnLevelChanged OnLevelChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnLevelIncreased OnLevelIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnLevelDecreased OnLevelDecreased;
	
protected:
	void LevelChanged(const FOnAttributeChangeData& Data);
	
	FDelegateHandle OnLevelAttributeChanged;
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Health
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Health")
	virtual bool IsAlive() const;
		
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Health")
	bool IsDead() const { return !IsAlive(); }
	
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Health")
	float GetHealth() const;
	
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Health")
	float GetMaxHealth() const;
	
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Health")
	float GetHealthRegenRate() const;
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthDecreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthDepleted, float, OldValue, float, NewValue);

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxHealthChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxHealthIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxHealthDecreased, float, OldValue, float, NewValue);
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthRegenRateChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthRegenRateIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthRegenRateDecreased, float, OldValue, float, NewValue);
	
	UPROPERTY(BlueprintAssignable)
	FOnHealthChanged OnHealthChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnHealthIncreased OnHealthIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnHealthDecreased OnHealthDecreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnHealthDepleted OnHealthDepleted;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxHealthChanged OnMaxHealthChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxHealthIncreased OnMaxHealthIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxHealthDecreased OnMaxHealthDecreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnHealthRegenRateChanged OnHealthRegenRateChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnHealthRegenRateIncreased OnHealthRegenRateIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnHealthRegenRateDecreased OnHealthRegenRateDecreased;
	
protected:
	virtual void HealthChanged(const FOnAttributeChangeData& Data);
	virtual void MaxHealthChanged(const FOnAttributeChangeData& Data);
	virtual void HealthRegenRateChanged(const FOnAttributeChangeData& Data);

	FDelegateHandle OnHealthAttributeChanged;
	FDelegateHandle OnMaxHealthAttributeChanged;
	FDelegateHandle OnHealthRegenRateAttributeChanged;

	virtual void Die();

	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Stamina
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Stamina")
	float GetStamina() const;
	
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Stamina")
	float GetMaxStamina() const;
	
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Stamina")
	float GetStaminaRegenRate() const;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaDecreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaDepleted, float, OldValue, float, NewValue);

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxStaminaChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxStaminaIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxStaminaDecreased, float, OldValue, float, NewValue);
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaRegenRateChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaRegenRateIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaRateDecreased, float, OldValue, float, NewValue);

	UPROPERTY(BlueprintAssignable)
	FOnStaminaChanged OnStaminaChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnStaminaIncreased OnStaminaIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnStaminaDecreased OnStaminaDecreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnStaminaDepleted OnStaminaDepleted;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxStaminaChanged OnMaxStaminaChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxStaminaIncreased OnMaxStaminaIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxStaminaDecreased OnMaxStaminaDecreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnStaminaRegenRateChanged OnStaminaRegenRateChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnStaminaRegenRateIncreased OnStaminaRegenRateIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnStaminaRateDecreased OnStaminaRateDecreased;
	
protected:
	virtual void StaminaChanged(const FOnAttributeChangeData& Data);
	virtual void MaxStaminaChanged(const FOnAttributeChangeData& Data);
	virtual void StaminaRegenRateChanged(const FOnAttributeChangeData& Data);

	FDelegateHandle OnStaminaAttributeChanged;
	FDelegateHandle OnMaxStaminaAttributeChanged;
	FDelegateHandle OnStaminaRegenRateAttributeChanged;

	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Armor
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Health")
	float GetArmor() const;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnArmorChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnArmorIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnArmorDecreased, float, OldValue, float, NewValue);

	UPROPERTY(BlueprintAssignable)
	FOnArmorChanged OnArmorChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnArmorIncreased OnArmorIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnArmorDecreased OnArmorDecreased;

protected:
	virtual void ArmorChanged(const FOnAttributeChangeData& Data);

	FDelegateHandle OnArmorAttributeChanged;

	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Shield
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Shield")
	float GetShield() const;
	
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Shield")
	float GetMaxShield() const;
	
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Shield")
	float GetShieldRegenRate() const;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldDecreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldDepleted, float, OldValue, float, NewValue);

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxShieldChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxShieldIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxShieldDecreased, float, OldValue, float, NewValue);
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldRegenRateChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldRegenRateIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldRegenRateDecreased, float, OldValue, float, NewValue);

	UPROPERTY(BlueprintAssignable)
	FOnShieldChanged OnShieldChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnShieldIncreased OnShieldIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnShieldDecreased OnShieldDecreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnShieldDepleted OnShieldDepleted;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxShieldChanged OnMaxShieldChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxShieldIncreased OnMaxShieldIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxShieldDecreased OnMaxShieldDecreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnShieldRegenRateChanged OnShieldRegenRateChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnShieldRegenRateIncreased OnShieldRegenRateIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnShieldRegenRateDecreased OnShieldRegenRateDecreased;
	
protected:
	virtual void ShieldChanged(const FOnAttributeChangeData& Data);
	virtual void MaxShieldChanged(const FOnAttributeChangeData& Data);
	virtual void ShieldRegenRateChanged(const FOnAttributeChangeData& Data);

	FDelegateHandle OnShieldAttributeChanged;
	FDelegateHandle OnMaxShieldAttributeChanged;
	FDelegateHandle OnShieldRegenRateAttributeChanged;

	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Dark Energy
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Dark Energy")
	float GetDarkEnergy() const;
	
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Dark Energy")
	float GetMaxDarkEnergy() const;
	
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Dark Energy")
	float GetDarkEnergyRegenRate() const;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDarkEnergyChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDarkEnergyIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDarkEnergyDecreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDarkEnergyDepleted, float, OldValue, float, NewValue);

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxDarkEnergyChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxDarkEnergyIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxDarkEnergyDecreased, float, OldValue, float, NewValue);
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDarkEnergyRegenRateChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDarkEnergyRegenRateIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDarkEnergyRegenRateDecreased, float, OldValue, float, NewValue);

	UPROPERTY(BlueprintAssignable)
	FOnDarkEnergyChanged OnDarkEnergyChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnDarkEnergyIncreased OnDarkEnergyIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnDarkEnergyDecreased OnDarkEnergyDecreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnDarkEnergyDepleted OnDarkEnergyDepleted;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxDarkEnergyChanged OnMaxDarkEnergyChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxDarkEnergyIncreased OnMaxDarkEnergyIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxDarkEnergyDecreased OnMaxDarkEnergyDecreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnDarkEnergyRegenRateChanged OnDarkEnergyRegenRateChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnDarkEnergyRegenRateIncreased OnDarkEnergyRegenRateIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnDarkEnergyRegenRateDecreased OnDarkEnergyRegenRateDecreased;
	
protected:
	virtual void DarkEnergyChanged(const FOnAttributeChangeData& Data);
	virtual void MaxDarkEnergyChanged(const FOnAttributeChangeData& Data);
	virtual void DarkEnergyRegenRateChanged(const FOnAttributeChangeData& Data);
	
	FDelegateHandle OnDarkEnergyAttributeChanged;
	FDelegateHandle OnMaxDarkEnergyAttributeChanged;
	FDelegateHandle OnDarkEnergyRegenRateAttributeChanged;

	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Light Energy
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Light Energy")
	float GetLightEnergy() const;
	
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Light Energy")
	float GetMaxLightEnergy() const;
	
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Light Energy")
	float GetLightEnergyRegenRate() const;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLightEnergyChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLightEnergyIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLightEnergyDecreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLightEnergyDepleted, float, OldValue, float, NewValue);

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxLightEnergyChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxLightEnergyIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxLightEnergyDecreased, float, OldValue, float, NewValue);
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLightEnergyRegenRateChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLightEnergyRegenRateIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLightEnergyRegenRateDecreased, float, OldValue, float, NewValue);

	UPROPERTY(BlueprintAssignable)
	FOnLightEnergyChanged OnLightEnergyChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnLightEnergyIncreased OnLightEnergyIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnLightEnergyDecreased OnLightEnergyDecreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnLightEnergyDepleted OnLightEnergyDepleted;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxLightEnergyChanged OnMaxLightEnergyChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxLightEnergyIncreased OnMaxLightEnergyIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnMaxLightEnergyDecreased OnMaxLightEnergyDecreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnLightEnergyRegenRateChanged OnLightEnergyRegenRateChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnLightEnergyRegenRateIncreased OnLightEnergyRegenRateIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnLightEnergyRegenRateDecreased OnLightEnergyRegenRateDecreased;
	
protected:
	virtual void LightEnergyChanged(const FOnAttributeChangeData& Data);
	virtual void MaxLightEnergyChanged(const FOnAttributeChangeData& Data);
	virtual void LightEnergyRegenRateChanged(const FOnAttributeChangeData& Data);

	FDelegateHandle OnLightEnergyAttributeChanged;
	FDelegateHandle OnMaxLightEnergyAttributeChanged;
	FDelegateHandle OnLightEnergyRegenRateAttributeChanged;

	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Movement Speed
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UFUNCTION(BlueprintPure, Category = "OFAttributeHandlingComponent|Movement Speed")
	float GetMovementSpeed() const;
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMovementSpeedChanged, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMovementSpeedIncreased, float, OldValue, float, NewValue);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMovementSpeedDecreased, float, OldValue, float, NewValue);
	
	UPROPERTY(BlueprintAssignable)
	FOnMovementSpeedChanged OnMovementSpeedChanged;
	
	UPROPERTY(BlueprintAssignable)
	FOnMovementSpeedIncreased OnMovementSpeedIncreased;
	
	UPROPERTY(BlueprintAssignable)
	FOnMovementSpeedDecreased OnMovementSpeedDecreased;
	
protected:
	virtual void MovementSpeedChanged(const FOnAttributeChangeData& Data);

	FDelegateHandle OnMovementSpeedAttributeChanged;
};

﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "OFAttributeSet.generated.h"

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

enum class EOFGameDifficulty : uint8;

/**
 * 
 */
UCLASS()
class DIVINEWARFARE_API UOFAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
	
public:
	UOFAttributeSet() {}
	
	//~ Begin UAttributeSet Interface
	virtual void InitFromMetaDataTable(const UDataTable* DataTable) override;
	//~ End UAttributeSet Interface

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Level")
	FGameplayAttributeData Level;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, Level)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Health")
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, MaxHealth)
	
	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Health")
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, Health)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Health")
	FGameplayAttributeData HealthRegenRate;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, HealthRegenRate)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Stamina")
	FGameplayAttributeData MaxStamina;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, MaxStamina)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Stamina")
	FGameplayAttributeData Stamina;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, Stamina)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Stamina")
	FGameplayAttributeData StaminaRegenRate;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, StaminaRegenRate)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Armor")
	FGameplayAttributeData Armor;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, Armor)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Shield")
	FGameplayAttributeData MaxShield;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, MaxShield)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Shield")
	FGameplayAttributeData Shield;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, Shield)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Shield")
	FGameplayAttributeData ShieldRegenRate;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, ShieldRegenRate)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Dark Energy")
	FGameplayAttributeData MaxDarkEnergy;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, MaxDarkEnergy)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Dark Energy")
	FGameplayAttributeData DarkEnergy;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, DarkEnergy)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Dark Energy")
	FGameplayAttributeData DarkEnergyRegenRate;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, DarkEnergyRegenRate)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Light Energy")
	FGameplayAttributeData MaxLightEnergy;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, MaxLightEnergy)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Light Energy")
	FGameplayAttributeData LightEnergy;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, LightEnergy)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Light Energy")
	FGameplayAttributeData LightEnergyRegenRate;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, LightEnergyRegenRate)

	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|MovementSpeed")
	FGameplayAttributeData MovementSpeed;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, MovementSpeed)
	
	UPROPERTY(BlueprintReadOnly, Category = "OFAttributeSetBase|Damage")
	FGameplayAttributeData Damage;
	ATTRIBUTE_ACCESSORS(UOFAttributeSet, Damage)
	
	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;
	
protected:
	void AdjustAttributeForChangeByName(const FString& AttributeName, FGameplayAttributeData* AttributeData, const float NewValue, const float OldValue);	
	
	void AdjustAttributeForMaxChange(const FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty) const;
};

USTRUCT(BlueprintType)
struct DIVINEWARFARE_API FOFAttributeMetaData : public FTableRowBase
{
	GENERATED_BODY()

public:
	FOFAttributeMetaData() {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay Attribute")
	float Easy = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay Attribute")
	float Normal = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay Attribute")
	float Hard = 0.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gameplay Attribute")
	float VeryHard = 0.f;
};

USTRUCT(BlueprintType)
struct DIVINEWARFARE_API FOFDamageData : public FTableRowBase
{
	GENERATED_BODY()
	
public:
	FOFDamageData() {}
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Damage = 0.f;
};

UCLASS()
class DIVINEWARFARE_API UOFAttributeMetaDataFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintPure, meta = (WorldContext = "WorldContextObject"))
	static float GetDamageFromString(const UObject* WorldContextObject, const TMap<EOFGameDifficulty, UDataTable*>& DamageDataTables, FString RowName = TEXT("None"));
};
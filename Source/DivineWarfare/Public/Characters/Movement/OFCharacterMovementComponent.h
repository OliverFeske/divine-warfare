﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "OFCharacterMovementComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class DIVINEWARFARE_API UOFCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	UOFCharacterMovementComponent();
};

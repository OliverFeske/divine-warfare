﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemInterface.h"
#include "ModularCharacter.h"
#include "OFInventoryComponent.h"
#include "OFItemHolderInterface.h"
#include "OFCharacterBase.generated.h"

class UOFAbilitySystemComponent;
class UOFAttributeHandlingComponent;
class UMotionWarpingComponent;

UCLASS(Abstract)
class DIVINEWARFARE_API AOFCharacterBase : public AModularCharacter, public IAbilitySystemInterface, public IOFItemHolderInterface
{
	GENERATED_BODY()
	
public:
	AOFCharacterBase(const FObjectInitializer& ObjectInitializer);
	
public:
	//~ Begin AActor Interface
	virtual void PostInitializeComponents() override;
	//~ End AActor Interface
	
	//~ Begin IAbilitySystemInterface Interface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	//~ End IAbilitySystemInterface Interface
	
	//~ Begin IOFItemHolderInterface
	virtual void GetItems_Implementation(TArray<TScriptInterface<IOFItemInterface>>& OutItems) const override
	{
		IOFItemHolderInterface::Execute_GetItems(InventoryComponent, OutItems);
	}
	virtual bool AddItem_Implementation(const TScriptInterface<IOFItemInterface>& ItemToAdd) override
	{
		return IOFItemHolderInterface::Execute_AddItem(InventoryComponent, ItemToAdd);
	}
	virtual bool RemoveItem_Implementation(const TScriptInterface<IOFItemInterface>& ItemToRemove) override
	{
		return IOFItemHolderInterface::Execute_RemoveItem(InventoryComponent, ItemToRemove);
	}
	virtual bool ContainsItem_Implementation(const TScriptInterface<IOFItemInterface>& Item) const override
	{
		return IOFItemHolderInterface::Execute_ContainsItem(InventoryComponent, Item);
	}
	virtual void GetItemsOfClass_Implementation(TSubclassOf<UObject> ItemClass, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const override
	{
		IOFItemHolderInterface::Execute_GetItemsOfClass(InventoryComponent, ItemClass, OutItems);
	}
	virtual void GetItemsWithTag_Implementation(FGameplayTag ItemTag, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const override
	{
		IOFItemHolderInterface::Execute_GetItemsWithTag(InventoryComponent, ItemTag, bMatchExact, OutItems);
	}
	virtual void GetItemsWithAnyTag_Implementation(FGameplayTagContainer ItemTags, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const override
	{
		IOFItemHolderInterface::Execute_GetItemsWithAnyTag(InventoryComponent, ItemTags, bMatchExact, OutItems);
	}
	virtual void GetItemsWithAllTags_Implementation(FGameplayTagContainer ItemTags, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const override
	{
		IOFItemHolderInterface::Execute_GetItemsWithAllTags(InventoryComponent, ItemTags, bMatchExact, OutItems);
	}
	virtual void SetCurrentEquippedItem_Implementation(const TScriptInterface<IOFItemInterface>& ItemToEquip) override
	{
		CurrentEquippedItem = ItemToEquip;
	}
	virtual void GetCurrentEquippedItem_Implementation(TScriptInterface<IOFItemInterface>& EquippedItem) const override
	{
		EquippedItem = CurrentEquippedItem;
	}
	//~ End IOFItemHolderInterface
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Ability System
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UOFAbilitySystemComponent* GetOFAbilitySystemComponent() const { return OFAbilitySystemComponent; }
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UOFAbilitySystemComponent* OFAbilitySystemComponent;
	
	UFUNCTION()
	void OnDefaultAbilitiesAndAttributesGranted();
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Attribute Handling
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UOFAttributeHandlingComponent* GetAttributeHandlingComponent() const { return AttributeHandlingComponent; }
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UOFAttributeHandlingComponent* AttributeHandlingComponent;
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Motion Warping
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UMotionWarpingComponent* GetMotionWarpingComponent() const { return MotionWarpingComponent; }
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UMotionWarpingComponent* MotionWarpingComponent;
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Inventory / Items
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UOFInventoryComponent* GetInventoryComponent() const { return InventoryComponent; }
	const FString& GetItemAttachmentName() const { return ItemAttachmentName;} 
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UOFInventoryComponent* InventoryComponent;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FString ItemAttachmentName = FString(TEXT("None"));
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TScriptInterface<IOFItemInterface> CurrentEquippedItem = nullptr;
};

﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OFWeapon.h"
#include "OFHeroWeapon.generated.h"

enum class EOFGameDifficulty : uint8;

UENUM(BlueprintType)
enum class EHeroWeaponForm : uint8
{
	Mace,
	Axe,
	SwordAndShield,
	Spear,
	FullBodyEnergyInfusion,
	RageMode
};

UCLASS()
class DIVINEWARFARE_API AOFHeroWeapon : public AOFWeapon
{
	GENERATED_BODY()
	
public:
	AOFHeroWeapon();

	//~ Begin IOFWeaponInterface
	virtual float GetDamageByName_Implementation(const FString& DamageName) const override;
	//~ End IOFWeaponInterface
	
	const TMap<EOFGameDifficulty, UDataTable*>& GetDamageDataTables() const { return DamageDataTables; }
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TMap<EOFGameDifficulty, UDataTable*> DamageDataTables;
};

﻿// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "OFCharacterBase.h"
#include "OFHeroCharacter.generated.h"

class UInputAction;
class UInputMappingContext;
class UOFThirdPersonCameraComponent;
class UOFAbilityInputBindingComponent;
class UWorldPartitionStreamingSourceComponent;

struct FInputActionValue;

UCLASS()
class DIVINEWARFARE_API AOFHeroCharacter : public AOFCharacterBase
{
	GENERATED_BODY()
	
public:
	AOFHeroCharacter(const FObjectInitializer& ObjectInitializer);
	
	//~ Begin AActor Interface
	virtual void BeginPlay() override;
	//~ End AActor Interface
	
	//~ Begin APawn Interface
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void UnPossessed() override;
	//~ End APawn Interface
	
protected:
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Movement
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	
protected:
	UPROPERTY()
	UCharacterMovementComponent* CharacterMovementComponent;
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Camera
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UOFThirdPersonCameraComponent* GetOFThirdPersonCamera() const { return OFThirdPersonCamera; }
	
protected:
	UPROPERTY(EditDefaultsOnly)
	UOFThirdPersonCameraComponent* OFThirdPersonCamera;
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Input
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	UInputMappingContext* GetBaseMappingContext() const { return BaseMappingContext; }
	int32 GetBaseMappingPriority() const { return BaseMappingPriority; }
	UOFAbilityInputBindingComponent * GetOFAbilityInputBindingComponent() const { return OFAbilityInputBindingComponent; }

	UFUNCTION(BlueprintPure)
	FVector GetMovementVector() const;
	
protected:
	void MoveForwardBackward(const FInputActionValue& Value);
	void MoveLeftRight(const FInputActionValue& Value);
	void LookUp(const FInputActionValue& Value);
	void LookUpAtRate(const FInputActionValue& Value);
	void Turn(const FInputActionValue& Value);
	void TurnAtRate(const FInputActionValue& Value);
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "OFHeroCharacter|Controls|Input")
	UInputAction* MoveForwardBackwardAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "OFHeroCharacter|Controls|Input")
	UInputAction* MoveLeftRightAction;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "OFHeroCharacter|Controls|Input")
	UInputAction* LookUpAction;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "OFHeroCharacter|Controls|Input")
	UInputAction* LookUpAtRateAction;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "OFHeroCharacter|Controls|Input")
	float LookUpRate = 45.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "OFHeroCharacter|Controls|Input")
	UInputAction* TurnAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "OFHeroCharacter|Controls|Input")
	UInputAction* TurnAtRateAction;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "OFHeroCharacter|Controls|Input")
	float TurnRate = 45.f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "OFHeroCharacter|Controls|Input")
	UInputMappingContext* BaseMappingContext;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "OFHeroCharacter|Controls|Input")
	int32 BaseMappingPriority = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UOFAbilityInputBindingComponent* OFAbilityInputBindingComponent;

	float ForwardBackwardValue = 0.f;
	float LeftRightValue = 0.f;

	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	World Partition Streaming
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UWorldPartitionStreamingSourceComponent* WorldPartitionStreamingSourceComponent;
};
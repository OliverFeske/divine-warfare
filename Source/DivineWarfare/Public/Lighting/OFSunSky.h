﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "OFSunSky.generated.h"

class UDirectionalLightComponent;

UENUM(BlueprintType)
enum class ESeason : uint8
{
	Spring,
	Summer,
	Fall,
	Winter
};

UCLASS()
class DIVINEWARFARE_API AOFSunSky : public AActor
{
	GENERATED_BODY()

public:
	AOFSunSky();

	//~ Begin AActor Interface
	virtual void Tick(float DeltaTime) override;
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void PostActorCreated() override;
	//~ End AActor Interface
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Sky Sphere
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	
protected:
	void RefreshMaterial();

	void UpdateSunDirection() const;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	float SunHeight = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	FLinearColor HorizonColor = FLinearColor(1.98f, 2.59f, 3.f, 1.f);
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	FLinearColor ZenithColor = FLinearColor(.03f, .11f, .295f, 1.f);
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	FLinearColor OverallColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	FLinearColor CloudColor = FLinearColor(.86f, .91f, 1.f, 1.f);
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	float HorizonFalloff = 3.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	float CloudSpeed = 1.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	float SunBrightness = 50.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	float CloudOpacity = .7f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	float StarsBrightness = .1f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	bool bColorsDeterminedBySunPosition = true;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	UMaterialInterface* SkyMaterial = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	UCurveLinearColor* HorizonColorCurve = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	UCurveLinearColor* ZenithColorCurve = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "OFSunSky|Sky Sphere")
	UCurveLinearColor* CloudColorCurve = nullptr;

	UPROPERTY(Transient)
	UMaterialInstanceDynamic* SkyMaterialInstance = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UStaticMeshComponent* SkySphereMesh = nullptr;

	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//	Shared
	//-----------------------------------------------------------------------------------------------------------------------------------------------
public:
	
protected:
	UFUNCTION(BlueprintCallable)
	void UpdateProperties();

	void UpdateLighting() const;
	
	UFUNCTION(BlueprintCallable)
	void AddSeconds(const float AddedSeconds);
	
	UFUNCTION(BlueprintCallable)
	void AddMinutes(const int32 AddedMinutes);
	
	UFUNCTION(BlueprintCallable)
	void AddHours(const int32 AddedHours);
	
	UFUNCTION(BlueprintCallable)
	void AddDays(const int32 AddedDays);
	
	UFUNCTION(BlueprintCallable)
	void AddMonths(const int32 AddedMonths);
	
	UFUNCTION(BlueprintCallable)
	void AddYears(const int32 AddedYears);
	
	void AdvanceTime(const float DeltaTime);
	
	/** Returns the current time of day in a format ranging from 0 to 24. Hour and Minutes are converted to a range from 0 to 1. */
	UFUNCTION(BlueprintPure)
	float GetTimeOfDay() const { return Hour + (((Minute * SecondsPerMinute) + Second) / SecondsPerHour); }
	
	constexpr static int32 SecondsPerDay = 86400;
	constexpr static int32 MinutesPerDay = 1440;
	constexpr static int32 HoursPerDay = 24;
	constexpr static int32 MinutesPerHour = 60;
	constexpr static int32 SecondsPerHour = 3600;
	constexpr static int32 SecondsPerMinute = 60;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky", meta = (ClampMin = "0"))
	int32 Year = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky", meta = (ClampMin = "1"))
	int32 Month = 1;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky", meta = (ClampMin = "1"))
	int32 Day = 1;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky", meta = (ClampMin = "0", ClampMax = "23"))
	int32 Hour = 13;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky", meta = (ClampMin = "0", ClampMax = "59"))
	int32 Minute = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky", meta = (ClampMin = "0", ClampMax = "59"))
	float Second = 0.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky")
	float TimeSpeed = 12.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky", meta = (ClampMin = "1"))
	int32 MonthsPerYear = 12;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFSunSky", meta = (ClampMin = "1"))
	int32 DaysPerMonth = 30;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "OFSunSky")
	UCurveFloat* CurveSunHeightByTimeOfDay = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "OFSunSky")
	UCurveFloat* CurveSunLightIntensityByTimeOfDay = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "OFSunSky")
	UCurveFloat* CurveSkyLightIntensityByTimeOfDay = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USceneComponent* SceneRoot = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UDirectionalLightComponent* DirectionalLightComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USkyLightComponent* SkyLightComponent = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USkyAtmosphereComponent* SkyAtmosphereComponent = nullptr;
};

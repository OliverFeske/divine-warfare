﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFHeroCharacter.h"
#include "OFAbilityInputBindingComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "OFThirdPersonCameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/WorldPartitionStreamingSourceComponent.h"

#include "Kismet/KismetSystemLibrary.h"

AOFHeroCharacter::AOFHeroCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	OFThirdPersonCamera = CreateDefaultSubobject<UOFThirdPersonCameraComponent>(TEXT("OFThirdPersonCamera"));
	OFThirdPersonCamera->SetupAttachment(RootComponent);

	OFAbilityInputBindingComponent = CreateDefaultSubobject<UOFAbilityInputBindingComponent>(TEXT("OFAbilityInputBindingComponent"));

	WorldPartitionStreamingSourceComponent = CreateDefaultSubobject<UWorldPartitionStreamingSourceComponent>(TEXT("WorldPartitionStreamingSourceComponent"));
}

void AOFHeroCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AOFHeroCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (UEnhancedInputComponent* PlayerEnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		if (MoveForwardBackwardAction)
		{
			PlayerEnhancedInputComponent->BindAction(MoveForwardBackwardAction, ETriggerEvent::Triggered, this, &AOFHeroCharacter::MoveForwardBackward);
		}

		if (MoveLeftRightAction)
		{
			PlayerEnhancedInputComponent->BindAction(MoveLeftRightAction, ETriggerEvent::Triggered, this, &AOFHeroCharacter::MoveLeftRight);
		}

		if (LookUpAction)
		{
			PlayerEnhancedInputComponent->BindAction(LookUpAction, ETriggerEvent::Triggered, this, &AOFHeroCharacter::LookUp);
		}

		if (LookUpAtRateAction)
		{
			PlayerEnhancedInputComponent->BindAction(LookUpAtRateAction, ETriggerEvent::Triggered, this, &AOFHeroCharacter::LookUpAtRate);
		}

		if (TurnAction)
		{
			PlayerEnhancedInputComponent->BindAction(TurnAction, ETriggerEvent::Triggered, this, &AOFHeroCharacter::Turn);
		}

		if (TurnAtRateAction)
		{
			PlayerEnhancedInputComponent->BindAction(TurnAtRateAction, ETriggerEvent::Triggered, this, &AOFHeroCharacter::TurnAtRate);
		}
	}
}

void AOFHeroCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if (const APlayerController* PlayerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->ClearAllMappings();

			Subsystem->AddMappingContext(BaseMappingContext, BaseMappingPriority);
		}
	}
}

void AOFHeroCharacter::UnPossessed()
{
	if (const APlayerController* PlayerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->ClearAllMappings();
		}
	}
	
	Super::UnPossessed();
}

FVector AOFHeroCharacter::GetMovementVector() const
{
	const FVector InputVector = FVector(ForwardBackwardValue, LeftRightValue, 0.f);

	FVector AdjustedInputVector = InputVector;
	if(InputVector.Size() < .586f)
	{
		AdjustedInputVector = InputVector.GetClampedToSize(.287f, .332f);
	}

	return AdjustedInputVector;
}

void AOFHeroCharacter::MoveForwardBackward(const FInputActionValue& Value)
{
	ForwardBackwardValue = Value[0];

	if(Controller)
	{
		const FRotator ControlRotation = Controller->GetControlRotation();
		const FVector ForwardDirection = FRotationMatrix(FRotator(0.f, ControlRotation.Yaw, 0.f)).GetUnitAxis(EAxis::X);
		AddMovementInput(ForwardDirection, GetMovementVector().X);
	}
}

void AOFHeroCharacter::MoveLeftRight(const FInputActionValue& Value)
{
	LeftRightValue = Value[0];

	if(Controller)
	{
		const FRotator ControlRotation = Controller->GetControlRotation();
		const FVector RightDirection = FRotationMatrix(FRotator(0.f, ControlRotation.Yaw, 0.f)).GetUnitAxis(EAxis::Y);
		AddMovementInput(RightDirection, GetMovementVector().Y);
	}
}

void AOFHeroCharacter::LookUp(const FInputActionValue& Value)
{
	AddControllerPitchInput(Value[0]);
}

void AOFHeroCharacter::LookUpAtRate(const FInputActionValue& Value)
{
	if(const UWorld* World = GetWorld())
	{
		AddControllerPitchInput(Value[0] * LookUpRate * World->GetDeltaSeconds());
	}
}

void AOFHeroCharacter::Turn(const FInputActionValue& Value)
{
	AddControllerYawInput(Value[0]);
}

void AOFHeroCharacter::TurnAtRate(const FInputActionValue& Value)
{
	if(const UWorld* World = GetWorld())
	{
		AddControllerYawInput(Value[0] * TurnRate * World->GetDeltaSeconds());
	}
}

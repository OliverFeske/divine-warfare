﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFHeroWeapon.h"
#include "OFAttributeSet.h"

AOFHeroWeapon::AOFHeroWeapon()
{
	PrimaryActorTick.bCanEverTick = false;
}

float AOFHeroWeapon::GetDamageByName_Implementation(const FString& DamageName) const
{
	return UOFAttributeMetaDataFunctionLibrary::GetDamageFromString(this, DamageDataTables, DamageName);
}

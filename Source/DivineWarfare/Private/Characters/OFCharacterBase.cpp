﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFCharacterBase.h"

#include "MotionWarpingComponent.h"
#include "Components/CapsuleComponent.h"
#include "OFAbilitySystemComponent.h"
#include "OFAttributeHandlingComponent.h"
#include "OFAttributeSet.h"
#include "OFInventoryComponent.h"
#include "OFCharacterMovementComponent.h"

AOFCharacterBase::AOFCharacterBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UOFCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Overlap);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);

	OFAbilitySystemComponent = CreateDefaultSubobject<UOFAbilitySystemComponent>(TEXT("OFAbilitySystemComponent"));

	AttributeHandlingComponent = CreateDefaultSubobject<UOFAttributeHandlingComponent>(TEXT("AttributeHandlingComponent"));

	MotionWarpingComponent = CreateDefaultSubobject<UMotionWarpingComponent>(TEXT("MotionWarpingComponent"));

	InventoryComponent = CreateDefaultSubobject<UOFInventoryComponent>(TEXT("InventoryComponent"));
}

void AOFCharacterBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if(ensure(AttributeHandlingComponent) && ensure(OFAbilitySystemComponent))
	{
		if(UOFAttributeSet* OFAttributeSet = Cast<UOFAttributeSet>(OFAbilitySystemComponent->GetAttributeSetFromName(AttributeHandlingComponent->GetAttributeSetName())))
		{
			AttributeHandlingComponent->Initialize(OFAbilitySystemComponent, OFAttributeSet);
		}
		else
		{
			OFAbilitySystemComponent->OnDefaultAbilitiesAndAttributesGranted.AddDynamic(this, &AOFCharacterBase::OnDefaultAbilitiesAndAttributesGranted);
		}
	}
}

UAbilitySystemComponent* AOFCharacterBase::GetAbilitySystemComponent() const
{
	return OFAbilitySystemComponent;
}

void AOFCharacterBase::OnDefaultAbilitiesAndAttributesGranted()
{
	if(ensure(AttributeHandlingComponent) && ensure(OFAbilitySystemComponent))
	{
		if(UOFAttributeSet* OFAttributeSet = Cast<UOFAttributeSet>(OFAbilitySystemComponent->GetAttributeSetFromName(AttributeHandlingComponent->GetAttributeSetName())))
		{
			AttributeHandlingComponent->Initialize(OFAbilitySystemComponent, OFAttributeSet);
			OFAbilitySystemComponent->OnDefaultAbilitiesAndAttributesGranted.RemoveDynamic(this, &AOFCharacterBase::OnDefaultAbilitiesAndAttributesGranted);
		}
	}
}

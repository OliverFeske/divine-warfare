﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFCharacterMovementComponent.h"

UOFCharacterMovementComponent::UOFCharacterMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	MaxAcceleration = 500.f;
	BrakingFrictionFactor = 0.f;
	GroundFriction = 4.f;
	MaxWalkSpeed = 147.f;
	BrakingDecelerationWalking = 1000.f;
	JumpZVelocity = 600.f;
	AirControl = 0.f;
	AirControlBoostMultiplier = 0.f;
	AirControlBoostVelocityThreshold = 0.f;
	BrakingDecelerationFlying = 1000.f;
	RotationRate = FRotator::ZeroRotator;
}
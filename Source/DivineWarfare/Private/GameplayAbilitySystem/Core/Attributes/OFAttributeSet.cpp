﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFAttributeSet.h"

#include "GameplayEffectExtension.h"
#include "OFAttributeHandlingComponent.h"
#include "OFCharacterBase.h"
#include "OFGameInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"

void UOFAttributeSet::InitFromMetaDataTable(const UDataTable* DataTable)
{
	if(!ensure(DataTable)) { return; }
	
	for(TFieldIterator<FProperty> It(GetClass(), EFieldIteratorFlags::IncludeSuper); It; ++It)
	{
		FProperty* Property = *It;
		const FString PropertyName = Property->GetName();
		
		if(PropertyName == TEXT("Damage")) { return; }
		
		if (FGameplayAttribute::IsGameplayAttributeDataProperty(Property))
		{
			FString RowNameStr = FString::Printf(TEXT("%s"), *PropertyName);
			
			if (const FAttributeMetaData* MetaData = DataTable->FindRow<FAttributeMetaData>(FName(*RowNameStr), *FString(__FUNCTION__), false))
			{
				const FStructProperty* StructProperty = CastField<FStructProperty>(Property);
				check(StructProperty);
				FGameplayAttributeData* DataPtr = StructProperty->ContainerPtrToValuePtr<FGameplayAttributeData>(this);
				check(DataPtr);
				
				AdjustAttributeForChangeByName(PropertyName, DataPtr, MetaData->BaseValue, DataPtr->GetCurrentValue());
				
				// MARK_PROPERTY_DIRTY(this, StructProperty);
			}
			else
			{
				const UObject* DataTableOuter = DataTable->GetOuter();
				const FString OuterName = DataTableOuter ? DataTableOuter->GetName() : TEXT("NULL");
				UE_LOG(LogTemp, Warning, TEXT("%s: %s is not defined in %s for %s"), *FString(__FUNCTION__), *PropertyName, *DataTable->GetName(), *OuterName);
			}
		}
	}

	// TArray<FGameplayAttribute> AttributesToInitializeInOrder = { GetLevelAttribute(), GetHealthAttribute(), };
	
}

void UOFAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);

	if(Attribute == GetMaxHealthAttribute())
	{
		AdjustAttributeForMaxChange(Health, MaxHealth, NewValue, GetHealthAttribute());
	}
	else if(Attribute == GetMaxStaminaAttribute())
	{
		AdjustAttributeForMaxChange(Stamina, MaxStamina, NewValue, GetStaminaAttribute());
	}
	else if(Attribute == GetMaxShieldAttribute())
	{
		AdjustAttributeForMaxChange(Shield, MaxShield, NewValue, GetShieldAttribute());
	}
	else if(Attribute == GetMaxDarkEnergyAttribute())
	{
		AdjustAttributeForMaxChange(DarkEnergy, MaxDarkEnergy, NewValue, GetDarkEnergyAttribute());
	}
	else if(Attribute == GetMaxLightEnergyAttribute())
	{
		AdjustAttributeForMaxChange(LightEnergy, MaxLightEnergy, NewValue, GetLightEnergyAttribute());
	}
}

void UOFAttributeSet::PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue)
{
	Super::PostAttributeChange(Attribute, OldValue, NewValue);

	if(Attribute == GetMovementSpeedAttribute())
	{
		if (const ACharacter* TargetCharacter = GetTypedOuter<ACharacter>())
		{
			if (UCharacterMovementComponent* MovementComponent = TargetCharacter->GetCharacterMovement())
			{
				MovementComponent->MaxWalkSpeed = NewValue;
			}
		}
	}
}

void UOFAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);
	
	if(Data.EvaluatedData.Attribute == GetDamageAttribute())
	{
		const float LocalDamageDone = GetDamage();
		SetDamage(0.f);
		
		if(LocalDamageDone <= 0.f)
		{
			return;
		}
		
		const FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
		const UAbilitySystemComponent* SourceAbilitySystemComponent = Context.GetOriginalInstigatorAbilitySystemComponent();
		const UAbilitySystemComponent* OwningAbilitySystemComponent = GetOwningAbilitySystemComponent();
		const FGameplayTagContainer& SourceTags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();
		FGameplayTagContainer SpecAssetTags;
		Data.EffectSpec.GetAllAssetTags(SpecAssetTags);

		AActor* TargetActor = nullptr;
		const AController* TargetController = nullptr;
		const AOFCharacterBase* TargetCharacter = nullptr;
		if(Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
		{
			TargetActor = Data.Target.AbilityActorInfo->AvatarActor.Get();
			TargetController = Data.Target.AbilityActorInfo->PlayerController.Get();
			TargetCharacter = Cast<AOFCharacterBase>(TargetActor);
		}

		const AActor* SourceActor = nullptr;
		const AController* SourceController = nullptr;
		const AOFCharacterBase* SourceCharacter = nullptr;
		if(SourceAbilitySystemComponent && SourceAbilitySystemComponent->AbilityActorInfo.IsValid() && SourceAbilitySystemComponent->AbilityActorInfo->AvatarActor.IsValid())
		{
			SourceActor = SourceAbilitySystemComponent->AbilityActorInfo->AvatarActor.Get();
			SourceController = SourceAbilitySystemComponent->AbilityActorInfo->PlayerController.Get();
			if(!SourceController && SourceActor)
			{
				if(const APawn* SourcePawn = Cast<APawn>(SourceActor))
				{
					SourceController = SourcePawn->GetController();
				}
			}

			if(SourceController)
			{
				SourceCharacter = Cast<AOFCharacterBase>(SourceController->GetPawn());
			}
			else
			{
				SourceCharacter = Cast<AOFCharacterBase>(SourceActor);
			}
		
			if(const AActor* EffectCauser = Context.GetEffectCauser())
			{
				SourceActor = EffectCauser;
			}
		}
		
		//---Damage Calculation--------------------------------------------------------------------
		bool bWasAlive = true;
		
		if(const UOFAttributeHandlingComponent* AttributeHandlingComponent = TargetActor ? TargetActor->FindComponentByClass<UOFAttributeHandlingComponent>() : nullptr)
		{
			bWasAlive = AttributeHandlingComponent->IsAlive();	
		}
		else if(OwningAbilitySystemComponent)
		{
			bWasAlive = GetHealth() > 0.f;
		}

		if(bWasAlive)
		{
			const float OldShield = GetShield();
			const float DamageAfterShield = LocalDamageDone - OldShield;
			if(OldShield > 0.f)
			{
				const float NewShield = OldShield - LocalDamageDone;
				SetShield(FMath::Clamp(NewShield, 0.f, GetMaxShield()));
			}

			if(DamageAfterShield > 0.f)
			{
				const float NewHealth = GetHealth() - DamageAfterShield;
				SetHealth(FMath::Clamp(NewHealth, 0.f, GetMaxHealth()));
			}

			if(GetHealth() <= 0.f && SourceController != TargetController && SourceAbilitySystemComponent)
			{
				// UGameplayEffect* GEBounty = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("Bounty")));
				// GEBounty->DurationPolicy = EGameplayEffectDurationType::Instant;
				//
				// const int32 Index = GEBounty->Modifiers.Num();
				// GEBounty->Modifiers.SetNum(Index + 1);
				//
				// FGameplayModifierInfo& InfoXP = GEBounty->Modifiers[Index];
				// InfoXP.ModifierMagnitude = FScalableFloat(GetXPBounty());
				// InfoXP.ModifierOp = EGameplayModOp::Additive;
				// InfoXP.Attribute = GetXPAttribute();
				//
				// SourceAbilitySystemComponent->ApplyGameplayEffectToSelf(GEBounty, 1.f, SourceAbilitySystemComponent->MakeEffectContext());
			}
		}
	}
	else if(Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		SetHealth(FMath::Clamp(GetHealth(), 0.f, GetMaxHealth()));
	}
	else if(Data.EvaluatedData.Attribute == GetStaminaAttribute())
	{
		SetStamina(FMath::Clamp(GetStamina(), 0.f, GetMaxStamina()));
	}
	else if(Data.EvaluatedData.Attribute == GetShieldAttribute())
	{
		SetShield(FMath::Clamp(GetShield(), 0.f, GetMaxShield()));
	}
	else if(Data.EvaluatedData.Attribute == GetDarkEnergyAttribute())
	{
		SetDarkEnergy(FMath::Clamp(GetDarkEnergy(), 0.f, GetMaxDarkEnergy()));
	}
	else if(Data.EvaluatedData.Attribute == GetLightEnergyAttribute())
	{
		SetLightEnergy(FMath::Clamp(GetLightEnergy(), 0.f, GetMaxLightEnergy()));
	}
}

void UOFAttributeSet::AdjustAttributeForChangeByName(const FString& AttributeName, FGameplayAttributeData* AttributeData, float NewValue, const float OldValue)
{
	FGameplayAttribute Attribute;
	if(TEXT("Level") == AttributeName)
	{
		Attribute = GetLevelAttribute();
	}
	else if(TEXT("Health") == AttributeName)
	{
		Attribute = GetHealthAttribute();
	}
	else if(TEXT("MaxHealth") == AttributeName)
	{
		Attribute = GetMaxHealthAttribute();
	}
	else if(TEXT("HealthRegenRate") == AttributeName)
	{
		Attribute = GetHealthRegenRateAttribute();
	}
	else if(TEXT("Stamina") == AttributeName)
	{
		Attribute = GetStaminaAttribute();
	}
	else if(TEXT("MaxStamina") == AttributeName)
	{
		Attribute = GetMaxStaminaAttribute();
	}
	else if(TEXT("StaminaRegenRate") == AttributeName)
	{
		Attribute = GetStaminaRegenRateAttribute();
	}
	else if(TEXT("Armor") == AttributeName)
	{
		Attribute = GetArmorAttribute();
	}
	else if(TEXT("Shield") == AttributeName)
	{
		Attribute = GetShieldAttribute();
	}
	else if(TEXT("MaxShield") == AttributeName)
	{
		Attribute = GetMaxShieldAttribute();
	}
	else if(TEXT("ShieldRegenRate") == AttributeName)
	{
		Attribute = GetShieldRegenRateAttribute();
	}
	else if(TEXT("DarkEnergy") == AttributeName)
	{
		Attribute = GetDarkEnergyAttribute();
	}
	else if(TEXT("MaxDarkEnergy") == AttributeName)
	{
		Attribute = GetMaxDarkEnergyAttribute();
	}
	else if(TEXT("DarkEnergyRegenRate") == AttributeName)
	{
		Attribute = GetDarkEnergyRegenRateAttribute();
	}
	else if(TEXT("LightEnergy") == AttributeName)
	{
		Attribute = GetLightEnergyAttribute();
	}
	else if(TEXT("MaxLightEnergy") == AttributeName)
	{
		Attribute = GetMaxLightEnergyAttribute();
	}
	else if(TEXT("LightEnergyRegenRate") == AttributeName)
	{
		Attribute = GetLightEnergyRegenRateAttribute();
	}
	else if(TEXT("MovementSpeed") == AttributeName)
	{
		Attribute = GetMovementSpeedAttribute();
	}
	else
	{
		return;
	}

	AttributeData->SetBaseValue(NewValue);
	PreAttributeChange(Attribute, NewValue);
	AttributeData->SetCurrentValue(NewValue);
	PostAttributeChange(Attribute, OldValue, NewValue);
}

void UOFAttributeSet::AdjustAttributeForMaxChange(const FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty) const
{
	UAbilitySystemComponent* AbilitySystemComponent = GetOwningAbilitySystemComponent();
	const float CurrentMaxValue = MaxAttribute.GetCurrentValue();
	
	if(AbilitySystemComponent && !FMath::IsNearlyEqual(CurrentMaxValue, NewMaxValue))
	{
		const float CurrentValue = AffectedAttribute.GetCurrentValue();
		const float NewDelta = CurrentMaxValue > 0.f ? (CurrentValue * NewMaxValue / CurrentMaxValue) - CurrentValue : NewMaxValue;

		AbilitySystemComponent->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Additive, NewDelta);
	}
}

float UOFAttributeMetaDataFunctionLibrary::GetDamageFromString(const UObject* WorldContextObject, const TMap<EOFGameDifficulty, UDataTable*>& DamageDataTables, FString RowName)
{
	const UOFGameInstance* OFGameInstance = Cast<UOFGameInstance>(UGameplayStatics::GetGameInstance(WorldContextObject));
	if(!ensure(OFGameInstance) || DamageDataTables.Num() <= 0 || TEXT("None") == RowName) { return 0.f; }
	
	const EOFGameDifficulty GameDifficulty = OFGameInstance->GetGameDifficulty();

	const UDataTable* DataTable = nullptr;
	if(DamageDataTables.Contains(GameDifficulty))
	{
		DataTable = DamageDataTables.FindRef(GameDifficulty);
	}
	
	if(!IsValid(DataTable) && DamageDataTables.Contains(EOFGameDifficulty::Shared))
	{
		DataTable = DamageDataTables.FindRef(EOFGameDifficulty::Shared);
	}
	
	if(!IsValid(DataTable))
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't find a DataTable for neither the given [%s] nor the [%s]"),
			*StaticEnum<EOFGameDifficulty>()->GetValueAsString(GameDifficulty),
			*StaticEnum<EOFGameDifficulty>()->GetValueAsString(EOFGameDifficulty::Shared));
		
		return 0.f;
	}
	
	if (const FOFDamageData* MetaData = DataTable->FindRow<FOFDamageData>(FName(RowName), *FString(__FUNCTION__), false))
	{
		return MetaData->Damage;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't find [%s] in DataTable[%s]"), *DataTable->GetName(), *RowName);
		return 0.f;
	}
}
﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFAttributeHandlingComponent.h"
#include "OFGameplayTags.h"
#include "OFAttributeSet.h"
#include "OFAbilitySystemComponent.h"

UOFAttributeHandlingComponent::UOFAttributeHandlingComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	AttributeSet = CreateDefaultSubobject<UOFAttributeSet>(TEXT("AttributeSetBase"));
}

void UOFAttributeHandlingComponent::Initialize(UAbilitySystemComponent* NewAbilitySystemComponent, UOFAttributeSet* NewAttributeSet)
{
	AttributeSet = nullptr;
	AbilitySystemComponent = nullptr;
	OFAbilitySystemComponent = nullptr;
	
	if(ensureMsgf(NewAttributeSet, TEXT("%s of %s called %s with invalid AttributeSet!"), *GetName(), *FString(GetOwner() ? GetOwner()->GetName() : TEXT("NULL OWNER")), __FUNCTION__))
	{
		AttributeSet = NewAttributeSet;
	}
	else
	{
		return;
	}
	
	if(ensureMsgf(NewAbilitySystemComponent, TEXT("%s of %s called %s with invalid AbilitySystemComponent!"), *GetName(), *FString(GetOwner() ? GetOwner()->GetName() : TEXT("NULL OWNER")), __FUNCTION__))
	{
		AbilitySystemComponent = NewAbilitySystemComponent;
		OFAbilitySystemComponent = Cast<UOFAbilitySystemComponent>(NewAbilitySystemComponent);
	}
	else
	{
		return;
	}

	OnLevelAttributeChanged.Reset();
	OnLevelAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetLevelAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::LevelChanged);

	OnHealthAttributeChanged.Reset();
	OnHealthAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetHealthAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::HealthChanged);
	OnMaxHealthAttributeChanged.Reset();
	OnMaxHealthAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxHealthAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::MaxHealthChanged);
	OnHealthRegenRateAttributeChanged.Reset();
	OnHealthRegenRateAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetHealthRegenRateAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::HealthRegenRateChanged);

	OnStaminaAttributeChanged.Reset();
	OnStaminaAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetStaminaAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::StaminaChanged);
	OnMaxStaminaAttributeChanged.Reset();
	OnMaxStaminaAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxStaminaAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::MaxStaminaChanged);
	OnStaminaRegenRateAttributeChanged.Reset();
	OnStaminaRegenRateAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetStaminaRegenRateAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::StaminaRegenRateChanged);

	OnArmorAttributeChanged.Reset();
	OnArmorAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetArmorAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::ArmorChanged);

	OnShieldAttributeChanged.Reset();
	OnShieldAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetShieldAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::ShieldChanged);
	OnMaxShieldAttributeChanged.Reset();
	OnMaxShieldAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxShieldAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::MaxShieldChanged);
	OnShieldRegenRateAttributeChanged.Reset();
	OnShieldRegenRateAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetShieldRegenRateAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::ShieldRegenRateChanged);

	OnDarkEnergyAttributeChanged.Reset();
	OnDarkEnergyAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetDarkEnergyAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::DarkEnergyChanged);
	OnMaxDarkEnergyAttributeChanged.Reset();
	OnMaxDarkEnergyAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxDarkEnergyAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::MaxDarkEnergyChanged);
	OnDarkEnergyRegenRateAttributeChanged.Reset();
	OnDarkEnergyRegenRateAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetDarkEnergyRegenRateAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::DarkEnergyRegenRateChanged);

	OnLightEnergyAttributeChanged.Reset();
	OnLightEnergyAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetLightEnergyAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::LightEnergyChanged);
	OnMaxLightEnergyAttributeChanged.Reset();
	OnMaxLightEnergyAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxLightEnergyAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::MaxLightEnergyChanged);
	OnLightEnergyRegenRateAttributeChanged.Reset();
	OnLightEnergyRegenRateAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetLightEnergyRegenRateAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::LightEnergyRegenRateChanged);

	OnMovementSpeedAttributeChanged.Reset();
	OnMovementSpeedAttributeChanged = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMovementSpeedAttribute()).AddUObject(this, &UOFAttributeHandlingComponent::MovementSpeedChanged);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------
//	Level
//-----------------------------------------------------------------------------------------------------------------------------------------------

int32 UOFAttributeHandlingComponent::GetLevel() const
{
	return FMath::RoundToInt(IsValid(AttributeSet) ? AttributeSet->GetLevel() : 0.f);
}

void UOFAttributeHandlingComponent::LevelChanged(const FOnAttributeChangeData& Data)
{
	OnLevelChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnLevelIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnLevelDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------
//	Health
//-----------------------------------------------------------------------------------------------------------------------------------------------

bool UOFAttributeHandlingComponent::IsAlive() const
{
	if(IsValid(AbilitySystemComponent))
	{
		return !AbilitySystemComponent->HasMatchingGameplayTag(TAG_STATE_DEAD) || GetHealth() > 0.f;
	}

	return GetHealth() > 0.f;
}

float UOFAttributeHandlingComponent::GetHealth() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetHealth() : 0.f;
}

float UOFAttributeHandlingComponent::GetMaxHealth() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetMaxHealth() : 0.f;
}

float UOFAttributeHandlingComponent::GetHealthRegenRate() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetHealthRegenRate() : 0.f;
}

void UOFAttributeHandlingComponent::HealthChanged(const FOnAttributeChangeData& Data)
{
	OnHealthChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnHealthIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnHealthDecreased.Broadcast(Data.OldValue, Data.NewValue);
		if(IsDead())
		{
			Die();
			OnHealthDepleted.Broadcast(Data.OldValue, Data.NewValue);
		}
	}
}

void UOFAttributeHandlingComponent::MaxHealthChanged(const FOnAttributeChangeData& Data)
{
	OnMaxHealthChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnMaxHealthIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnMaxHealthDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

void UOFAttributeHandlingComponent::HealthRegenRateChanged(const FOnAttributeChangeData& Data)
{
	OnHealthRegenRateChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnHealthRegenRateIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnHealthRegenRateDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

void UOFAttributeHandlingComponent::Die()
{
	if(IsValid(AbilitySystemComponent))
	{
		AbilitySystemComponent->CancelAllAbilities();

		FGameplayTagContainer EffectTagsToRemove;
		EffectTagsToRemove.AddTag(TAG_EFFECT_REMOVEONDEATH);
		AbilitySystemComponent->RemoveActiveEffectsWithTags(EffectTagsToRemove);

		AbilitySystemComponent->AddLooseGameplayTag(TAG_STATE_DEAD);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------
//	Stamina
//-----------------------------------------------------------------------------------------------------------------------------------------------

float UOFAttributeHandlingComponent::GetStamina() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetStamina() : 0.f;
}

float UOFAttributeHandlingComponent::GetMaxStamina() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetMaxStamina() : 0.f;
}

float UOFAttributeHandlingComponent::GetStaminaRegenRate() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetStaminaRegenRate() : 0.f;
}

void UOFAttributeHandlingComponent::StaminaChanged(const FOnAttributeChangeData& Data)
{
	OnStaminaChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnStaminaIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnStaminaDecreased.Broadcast(Data.OldValue, Data.NewValue);
		if(Data.NewValue <= 0.f)
		{
			OnStaminaDepleted.Broadcast(Data.OldValue, Data.NewValue);
		}
	}
}

void UOFAttributeHandlingComponent::MaxStaminaChanged(const FOnAttributeChangeData& Data)
{
	OnMaxStaminaChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnMaxStaminaIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnMaxStaminaDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

void UOFAttributeHandlingComponent::StaminaRegenRateChanged(const FOnAttributeChangeData& Data)
{
	OnStaminaRegenRateChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnStaminaRegenRateIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnStaminaRateDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------
//	Armor
//-----------------------------------------------------------------------------------------------------------------------------------------------

float UOFAttributeHandlingComponent::GetArmor() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetArmor() : 0.f;
}

void UOFAttributeHandlingComponent::ArmorChanged(const FOnAttributeChangeData& Data)
{
	OnArmorChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnArmorIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnArmorDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------
//	Shield
//-----------------------------------------------------------------------------------------------------------------------------------------------

float UOFAttributeHandlingComponent::GetShield() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetShield() : 0.f;
}

float UOFAttributeHandlingComponent::GetMaxShield() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetMaxShield() : 0.f;
}

float UOFAttributeHandlingComponent::GetShieldRegenRate() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetShieldRegenRate() : 0.f;
}

void UOFAttributeHandlingComponent::ShieldChanged(const FOnAttributeChangeData& Data)
{
	OnShieldChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnShieldIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnShieldDecreased.Broadcast(Data.OldValue, Data.NewValue);
		if(Data.NewValue <= 0.f)
		{
			OnShieldDepleted.Broadcast(Data.OldValue, Data.NewValue);
		}
	}
}

void UOFAttributeHandlingComponent::MaxShieldChanged(const FOnAttributeChangeData& Data)
{
	OnMaxShieldChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnMaxShieldIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnMaxShieldDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

void UOFAttributeHandlingComponent::ShieldRegenRateChanged(const FOnAttributeChangeData& Data)
{
	OnShieldRegenRateChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnShieldRegenRateIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnShieldRegenRateDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------
//	Dark Energy
//-----------------------------------------------------------------------------------------------------------------------------------------------

float UOFAttributeHandlingComponent::GetDarkEnergy() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetDarkEnergy() : 0.f;
}

float UOFAttributeHandlingComponent::GetMaxDarkEnergy() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetMaxDarkEnergy() : 0.f;
}

float UOFAttributeHandlingComponent::GetDarkEnergyRegenRate() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetMaxDarkEnergy() : 0.f;
}

void UOFAttributeHandlingComponent::DarkEnergyChanged(const FOnAttributeChangeData& Data)
{
	OnDarkEnergyChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnDarkEnergyIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnDarkEnergyDecreased.Broadcast(Data.OldValue, Data.NewValue);
		if(Data.NewValue <= 0.f)
		{
			OnDarkEnergyDepleted.Broadcast(Data.OldValue, Data.NewValue);
		}
	}
}

void UOFAttributeHandlingComponent::MaxDarkEnergyChanged(const FOnAttributeChangeData& Data)
{
	OnMaxDarkEnergyChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnMaxDarkEnergyIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnMaxDarkEnergyDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

void UOFAttributeHandlingComponent::DarkEnergyRegenRateChanged(const FOnAttributeChangeData& Data)
{
	OnDarkEnergyRegenRateChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnDarkEnergyRegenRateIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnDarkEnergyRegenRateDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------
//	Light Energy
//-----------------------------------------------------------------------------------------------------------------------------------------------

float UOFAttributeHandlingComponent::GetLightEnergy() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetLightEnergy() : 0.f;
}

float UOFAttributeHandlingComponent::GetMaxLightEnergy() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetMaxLightEnergy() : 0.f;
}

float UOFAttributeHandlingComponent::GetLightEnergyRegenRate() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetLightEnergyRegenRate() : 0.f;
}

void UOFAttributeHandlingComponent::LightEnergyChanged(const FOnAttributeChangeData& Data)
{
	OnLightEnergyChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnLightEnergyIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnLightEnergyDecreased.Broadcast(Data.OldValue, Data.NewValue);
		if(Data.NewValue <= 0.f)
		{
			OnLightEnergyDepleted.Broadcast(Data.OldValue, Data.NewValue);
		}
	}
}

void UOFAttributeHandlingComponent::MaxLightEnergyChanged(const FOnAttributeChangeData& Data)
{
	OnMaxLightEnergyChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnMaxLightEnergyIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnMaxLightEnergyDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

void UOFAttributeHandlingComponent::LightEnergyRegenRateChanged(const FOnAttributeChangeData& Data)
{
	OnLightEnergyRegenRateChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnLightEnergyRegenRateIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnLightEnergyRegenRateDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------
//	Movement Speed
//-----------------------------------------------------------------------------------------------------------------------------------------------

float UOFAttributeHandlingComponent::GetMovementSpeed() const
{
	return IsValid(AttributeSet) ? AttributeSet->GetMovementSpeed() : 0.f;
}

void UOFAttributeHandlingComponent::MovementSpeedChanged(const FOnAttributeChangeData& Data)
{
	OnMovementSpeedChanged.Broadcast(Data.OldValue, Data.NewValue);

	if(Data.NewValue > Data.OldValue)
	{
		OnMovementSpeedIncreased.Broadcast(Data.OldValue, Data.NewValue);
	}
	else if(!FMath::IsNearlyEqual(Data.OldValue, Data.NewValue))
	{
		OnMovementSpeedDecreased.Broadcast(Data.OldValue, Data.NewValue);
	}
}

﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFSunSky.h"

#include "Components/DirectionalLightComponent.h"
#include "Components/SkyAtmosphereComponent.h"
#include "Components/SkyLightComponent.h"
#include "Curves/CurveLinearColor.h"
#include "Kismet/KismetMathLibrary.h"

AOFSunSky::AOFSunSky()
{
	PrimaryActorTick.bCanEverTick = true;

	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = SceneRoot;
	SceneRoot->Mobility = EComponentMobility::Static;

	SkySphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SkySphereMesh"));
	SkySphereMesh->SetupAttachment(SceneRoot);
	SkySphereMesh->Mobility = EComponentMobility::Static;
	SkySphereMesh->SetRelativeScale3D(FVector(400.f, 400.f, 400.f));
	SkySphereMesh->SetGenerateOverlapEvents(false);
	SkySphereMesh->SetCollisionProfileName(TEXT("NoCollision"));
	SkySphereMesh->CastShadow = false;
	SkySphereMesh->bVisibleInRayTracing = false;
	SkySphereMesh->bReceivesDecals = false;
	
	DirectionalLightComponent = CreateDefaultSubobject<UDirectionalLightComponent>(TEXT("DirectionalLightComponent"));
	DirectionalLightComponent->SetupAttachment(SceneRoot);
	DirectionalLightComponent->Mobility = EComponentMobility::Movable;
	DirectionalLightComponent->SetRelativeLocation(FVector(0.f, 0.f, 100.f));
	DirectionalLightComponent->Intensity = 10.f;
	DirectionalLightComponent->LightSourceAngle = 0.5f;
	DirectionalLightComponent->bAtmosphereSunLight = true;
	DirectionalLightComponent->DynamicShadowCascades = 5;
	DirectionalLightComponent->CascadeDistributionExponent = 1.4f;

	SkyLightComponent = CreateDefaultSubobject<USkyLightComponent>(TEXT("SkyLightComponent"));
	SkyLightComponent->SetupAttachment(SceneRoot);
	SkyLightComponent->SetRelativeLocation(FVector(0.f, 0.f, 150.f));
	SkyLightComponent->Mobility = EComponentMobility::Movable;
	SkyLightComponent->bRealTimeCapture = true;
	SkyLightComponent->bLowerHemisphereIsBlack = false;
	SkyLightComponent->bTransmission = true;
	SkyLightComponent->CastRaytracedShadow = ECastRayTracedShadow::Enabled;
	SkyLightComponent->SamplesPerPixel = 2;

	SkyAtmosphereComponent = CreateDefaultSubobject<USkyAtmosphereComponent>(TEXT("SkyAtmosphereComponent"));
	SkyAtmosphereComponent->SetupAttachment(SceneRoot);
}

void AOFSunSky::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(ensure(DirectionalLightComponent) && EComponentMobility::Movable == DirectionalLightComponent->Mobility)
	{
		AdvanceTime(DeltaTime);
		UpdateProperties();
	}
}

void AOFSunSky::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if(!SkyMaterialInstance)
	{
		SkyMaterialInstance = UMaterialInstanceDynamic::Create(SkyMaterial, this);
		SkySphereMesh->SetMaterial(0, SkyMaterialInstance);
	}
	
	UpdateProperties();
}

void AOFSunSky::PostActorCreated()
{
	Super::PostActorCreated();

	if(!SkyMaterialInstance)
	{
		SkyMaterialInstance = UMaterialInstanceDynamic::Create(SkyMaterial, this);
		SkySphereMesh->SetMaterial(0, SkyMaterialInstance);
	}
	
	UpdateProperties();
}

void AOFSunSky::RefreshMaterial()
{
	if(!SkyMaterialInstance)
	{
		SkyMaterialInstance = UMaterialInstanceDynamic::Create(SkyMaterial, this);
		SkySphereMesh->SetMaterial(0, SkyMaterialInstance);
	}
	
	if(!ensure(DirectionalLightComponent) || !ensure(HorizonColorCurve) || !ensure(ZenithColorCurve) || !ensure(CloudColorCurve) || !ensure(CurveSunHeightByTimeOfDay))
	{
		return;
	}
	
	const FRotator DirectionalLightRotation = DirectionalLightComponent->GetComponentRotation();
	
	SkyMaterialInstance->SetVectorParameterValue(TEXT("Light direction"), FLinearColor(DirectionalLightRotation.Vector()));
	SkyMaterialInstance->SetVectorParameterValue(TEXT("Sun color"), FLinearColor(DirectionalLightComponent->LightColor));
	
	SunHeight = CurveSunHeightByTimeOfDay->GetFloatValue(GetTimeOfDay());

	if(bColorsDeterminedBySunPosition)
	{
		SkyMaterialInstance->SetVectorParameterValue(TEXT("Horizon color"), HorizonColorCurve->GetClampedLinearColorValue(SunHeight));
		SkyMaterialInstance->SetVectorParameterValue(TEXT("Zenith Color"), ZenithColorCurve->GetClampedLinearColorValue(SunHeight));
		SkyMaterialInstance->SetVectorParameterValue(TEXT("Cloud color"), CloudColorCurve->GetClampedLinearColorValue(SunHeight));
		SkyMaterialInstance->SetScalarParameterValue(TEXT("Horizon Falloff"), UKismetMathLibrary::Lerp(3.f, 8.f, FMath::Abs(SunHeight)));
	}
	else
	{
		SkyMaterialInstance->SetVectorParameterValue(TEXT("Horizon color"), HorizonColor);
		SkyMaterialInstance->SetVectorParameterValue(TEXT("Zenith color"), ZenithColor);
		SkyMaterialInstance->SetVectorParameterValue(TEXT("Overall Color"), OverallColor);
		SkyMaterialInstance->SetVectorParameterValue(TEXT("Cloud Color"), CloudColor);
		SkyMaterialInstance->SetScalarParameterValue(TEXT("Horizon Falloff"), HorizonFalloff);
	}

	SkyMaterialInstance->SetScalarParameterValue(TEXT("Cloud speed"), CloudSpeed);
	SkyMaterialInstance->SetScalarParameterValue(TEXT("Sun brightness"), SunBrightness);
	SkyMaterialInstance->SetScalarParameterValue(TEXT("Sun height"), FMath::Abs(SunHeight));
	SkyMaterialInstance->SetScalarParameterValue(TEXT("Cloud opacity"), CloudOpacity);
	SkyMaterialInstance->SetScalarParameterValue(TEXT("Stars Brightness"), StarsBrightness);
}

void AOFSunSky::UpdateSunDirection() const
{
	if(!ensure(DirectionalLightComponent)) { return; }
	
	const float CurrentTimeInSeconds = (static_cast<float>(Hour) * SecondsPerHour) + (static_cast<float>(Minute) * SecondsPerMinute) + Second;
	
	const float NewPitch = FMath::GetMappedRangeValueUnclamped(FVector2D(0.f, 1.f), FVector2D(90.f, 450.f), CurrentTimeInSeconds / SecondsPerDay);
	
	DirectionalLightComponent->SetWorldRotation(FRotator(NewPitch, 0.f, 0.f));
}

void AOFSunSky::UpdateProperties()
{
	UpdateSunDirection();
	RefreshMaterial();
	UpdateLighting();
}

void AOFSunSky::UpdateLighting() const
{
	if(!ensure(DirectionalLightComponent) || !ensure(SkyLightComponent) || !ensure(CurveSunLightIntensityByTimeOfDay) || !ensure(CurveSkyLightIntensityByTimeOfDay)) { return; }

	DirectionalLightComponent->SetIntensity(CurveSunLightIntensityByTimeOfDay->GetFloatValue(GetTimeOfDay()));
	SkyLightComponent->SetIntensity(CurveSkyLightIntensityByTimeOfDay->GetFloatValue(GetTimeOfDay()));
}

void AOFSunSky::AdvanceTime(const float DeltaTime)
{
	const float AddedSeconds = DeltaTime * TimeSpeed;

	AddSeconds(AddedSeconds);
}

void AOFSunSky::AddSeconds(const float AddedSeconds)
{
	if(AddedSeconds <= 0.f) { return; }
	
	Second += AddedSeconds;
	if(Second >= static_cast<float>(SecondsPerMinute))
	{
		const float NewSecond = FMath::Fmod(Second, SecondsPerMinute);
		AddMinutes(static_cast<int32>((Second - NewSecond) / SecondsPerMinute));
		Second = NewSecond;
	}
}

void AOFSunSky::AddMinutes(const int32 AddedMinutes)
{
	if(AddedMinutes <= 0) { return; }
	
	Minute += AddedMinutes;
	if(Minute >= MinutesPerHour)
	{
		const int32 NewMinute = Minute % MinutesPerHour;
		AddHours((Minute - NewMinute) / MinutesPerHour);
		Minute = NewMinute;
	}
}

void AOFSunSky::AddHours(const int32 AddedHours)
{
	if(AddedHours <= 0) { return; }
	
	Hour += AddedHours;
	if(Hour >= HoursPerDay)
	{
		const int32 NewHour = Hour % HoursPerDay;
		AddDays((Hour - NewHour) / HoursPerDay);
		Hour = NewHour;
	}
}

void AOFSunSky::AddDays(const int32 AddedDays)
{
	if(AddedDays <= 0) { return; }
	
	Day += AddedDays;
	if(Day >= DaysPerMonth)
	{
		const int32 NewDay = Day % DaysPerMonth;
		AddMonths((Day - NewDay) / DaysPerMonth);
		Day = NewDay;
	}
}

void AOFSunSky::AddMonths(const int32 AddedMonths)
{
	if(AddedMonths <= 0) { return; }
	
	Month += AddedMonths;
	if(Month >= MonthsPerYear)
	{
		const int32 NewMonth = Month % MonthsPerYear;
		AddYears((Month - NewMonth) / MonthsPerYear);
		Month = NewMonth;
	}
}

void AOFSunSky::AddYears(const int32 AddedYears)
{
	if(AddedYears <= 0) { return; }
	
	Year += AddedYears;
}

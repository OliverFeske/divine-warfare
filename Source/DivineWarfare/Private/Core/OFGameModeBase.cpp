﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFGameModeBase.h"
#include "Engine/Engine.h"
#include "GameFeaturesSubsystem.h"

void AOFGameModeBase::StartPlay()
{
	GEngine->BlockTillLevelStreamingCompleted(GetWorld());
	Super::StartPlay();
}

void AOFGameModeBase::ToggleGameFeaturePlugin(FString GameFeaturePluginName, bool bEnable)
{
	FString PluginURL;
	
	UGameFeaturesSubsystem::Get().GetPluginURLForBuiltInPluginByName(GameFeaturePluginName, PluginURL);

	bEnable ? UGameFeaturesSubsystem::Get().LoadAndActivateGameFeaturePlugin(PluginURL, FGameFeaturePluginLoadComplete()) : UGameFeaturesSubsystem::Get().UnloadGameFeaturePlugin(PluginURL);
}

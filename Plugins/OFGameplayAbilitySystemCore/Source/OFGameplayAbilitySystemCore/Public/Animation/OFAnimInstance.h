﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Animation/AnimInstance.h"
#include "GameplayEffectTypes.h"
#include "OFAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class OFGAMEPLAYABILITYSYSTEMCORE_API UOFAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
	FGameplayTagBlueprintPropertyMap GameplayTagPropertyMap;

public:
	//~ Begin UAnimInstance interface
	virtual void NativeInitializeAnimation() override;
	//~ End UAnimInstance interface

	void InitializeWithAbilitySystem(UAbilitySystemComponent* AbilityComponent);

	UFUNCTION(BlueprintImplementableEvent, DisplayName = "Initialize with Ability System")
	void ReceiveAbilitySystem(UAbilitySystemComponent* AbilitySystem);
};

﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AbilitySystemInterface.h"

#include "OFASCActorBase.generated.h"

class UOFAbilitySystemComponent;

UCLASS()
class OFGAMEPLAYABILITYSYSTEMCORE_API AOFASCActorBase : public AActor, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	AOFASCActorBase();
	
	//~ Begin AActor Interface
	virtual void BeginPlay() override;
	//~ End AActor Interface

	//~ Begin IAbilitySystemInterface Interface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	//~ End IAbilitySystemInterface Interface

	UOFAbilitySystemComponent* GetOFAbilitySystemComponent() const { return AbilitySystemComponent; }
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AOFASCActorBase|Ability System")
	UOFAbilitySystemComponent* AbilitySystemComponent;
};

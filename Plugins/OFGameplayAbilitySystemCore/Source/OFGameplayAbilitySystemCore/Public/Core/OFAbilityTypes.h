﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameplayEffectTypes.h"
#include "Abilities/GameplayAbilityTargetTypes.h"

#include "OFAbilityTypes.generated.h"

USTRUCT(BlueprintType)
struct FOFGameplayEffectContainer
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFGameplayEffectContainerSpec")
	TArray<TSubclassOf<UGameplayEffect>> GameplayEffectClasses;
};

USTRUCT(BlueprintType)
struct FOFGameplayEffectContainerSpec
{
	GENERATED_BODY()

public:
	FOFGameplayEffectContainerSpec() {}

	bool HasValidEffects() const { return TargetGameplayEffectSpecs.Num() > 0; }
	bool HasValidTargets() const { return TargetData.Num() > 0; }
	void AddTargets(const TArray<FGameplayAbilityTargetDataHandle>& InTargetData, const TArray<FHitResult>& InHitResults, const TArray<AActor*>& InTargetActors);
	void ClearTargets() { TargetData.Clear(); }

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFGameplayEffectContainerSpec")
	FGameplayAbilityTargetDataHandle TargetData;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFGameplayEffectContainerSpec")
	TArray<FGameplayEffectSpecHandle> TargetGameplayEffectSpecs;
};
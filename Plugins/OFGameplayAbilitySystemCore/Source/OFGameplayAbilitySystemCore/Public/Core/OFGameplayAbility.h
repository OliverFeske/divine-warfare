﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "OFAbilityTypes.h"
#include "OFGameplayAbility.generated.h"

class UInputAction;
class UInputMappingContext;
class UOFAbilityActivationCondition;

UCLASS()
class OFGAMEPLAYABILITYSYSTEMCORE_API UOFGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:
	UOFGameplayAbility();

	//~ Begin UGameplayAbility interface
	virtual void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
	virtual bool CheckCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, OUT FGameplayTagContainer* OptionalRelevantTags = nullptr) const override;
	virtual void ApplyCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) const override;
	//~ End UGameplayAbility interface
	
	UFUNCTION(BlueprintCallable, Category = "OFGameplayAbility")
	FGameplayAbilityTargetDataHandle MakeGameplayAbilityTargetDataHandleFromActorArray(const TArray<AActor*> TargetActors);
	
	UFUNCTION(BlueprintCallable, Category = "OFGameplayAbility")
	FGameplayAbilityTargetDataHandle MakeGameplayAbilityTargetDataHandleFromHitResults(const TArray<FHitResult> HitResults);
	
	UFUNCTION(BlueprintCallable, Category = "OFGameplayAbility", meta = (AutoCreateRefTerm = "EventData"))
	virtual FOFGameplayEffectContainerSpec MakeEffectContainerSpecFromEffectContainer(const FOFGameplayEffectContainer& Container, const FGameplayEventData& EventData, int32 OverrideGameplayLevel = -1);
	
	UFUNCTION(BlueprintCallable, Category = "OFGameplayAbility")
	virtual TArray<FActiveGameplayEffectHandle> ApplyEffectContainerSpec(const FOFGameplayEffectContainerSpec& ContainerSpec);
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "OFGameplayAbility", meta = (DisplayName = "Get Source Object"))
	UObject* BP_GetSourceObject(FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo& ActorInfo) const { return GetSourceObject(Handle, &ActorInfo); }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFGameplayAbility")
	bool OFCheckCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo& ActorInfo, const FGameplayTagContainer& OptionalRelevantTags) const;
	virtual bool OFCheckCost_Implementation(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo& ActorInfo, const FGameplayTagContainer& OptionalRelevantTags) const;
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFGameplayAbility")
	void OFApplyCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo& ActorInfo, const FGameplayAbilityActivationInfo& ActivationInfo) const;
	virtual void OFApplyCost_Implementation(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo& ActorInfo, const FGameplayAbilityActivationInfo& ActivationInfo) const {};
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "OFGameplayAbility")
	bool bActivateAbilityOnGranted = false;
};

﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "OFGASUtilities.h"
#include "OFAbilitySystemComponent.generated.h"

class UOFAttributeSetBase;

struct FOFAbilityInputCombo;

USTRUCT(BlueprintType)
struct FOFAttributeApplication
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere)
	FName AttributeName = TEXT("Default");
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UAttributeSet> AttributeSetType;
	
	UPROPERTY(EditAnywhere)
	UDataTable* Attributes = nullptr;
};

UCLASS(meta=(BlueprintSpawnableComponent))
class OFGAMEPLAYABILITYSYSTEMCORE_API UOFAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()

public:
	UOFAbilitySystemComponent();

	//~ Begin UObject interface
	virtual void BeginDestroy() override;
	//~ End UObject interface
	
	//~ Begin UAbilitySystemComponent Interface
	virtual void InitAbilityActorInfo(AActor* InOwnerActor, AActor* InAvatarActor) override;
	//~ End UAbilitySystemComponent Interface

	UFUNCTION(BlueprintCallable)
	bool TryActivateAbilityWithEventData(TSubclassOf<UGameplayAbility> InAbilityToActivate, const FGameplayEventData& Payload);

	static UOFAbilitySystemComponent* GetOFAbilitySystemComponentFromActor(const AActor* Actor, bool LookForComponent = false);

	UFUNCTION(BlueprintPure)
	UAttributeSet* GetAttributeSetFromName(FString AttributeSetName);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "OFAbilitySystemComponent", Meta = (DisplayName = "Get Tag Count", ScriptName = "GetTagCount"))
	int32 BP_GetTagCount(FGameplayTag TagToCheck) const { return GetTagCount(TagToCheck); }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "OFAbilitySystemComponent")
	FGameplayAbilitySpecHandle FindAbilitySpecHandleForClass(TSubclassOf<UGameplayAbility> AbilityClass, UObject* OptionalSourceObject = nullptr);

	UFUNCTION(BlueprintCallable, Category = "OFAbilitySystemComponent", meta = (DisplayName = "AddLooseGameplayTag"))
	void BP_AddLooseGameplayTag(const FGameplayTag& GameplayTag, int32 Count = 1) { AddLooseGameplayTag(GameplayTag, Count); }

	UFUNCTION(BlueprintCallable, Category = "OFAbilitySystemComponent", meta = (DisplayName = "AddLooseGameplayTags"))
	void BP_AddLooseGameplayTags(const FGameplayTagContainer& GameplayTags, int32 Count = 1) { AddLooseGameplayTags(GameplayTags, Count); }

	UFUNCTION(BlueprintCallable, Category = "OFAbilitySystemComponent", meta = (DisplayName = "RemoveLooseGameplayTag"))
	void BP_RemoveLooseGameplayTag(const FGameplayTag& GameplayTag, int32 Count = 1) { RemoveLooseGameplayTag(GameplayTag, Count); }

	UFUNCTION(BlueprintCallable, Category = "OFAbilitySystemComponent", meta = (DisplayName = "RemoveLooseGameplayTags"))
	void BP_RemoveLooseGameplayTags(const FGameplayTagContainer& GameplayTags, int32 Count = 1) { RemoveLooseGameplayTags(GameplayTags, Count); }

	UFUNCTION(BlueprintCallable, Category = "OFAbilitySystemComponent", meta = (AutoCreateRefTerm = "GameplayCueParameters", GameplayTagFilter = "GameplayCue"))
	void ExecuteGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);
	
	UFUNCTION(BlueprintCallable, Category = "OFAbilitySystemComponent", meta = (AutoCreateRefTerm = "GameplayCueParameters", GameplayTagFilter = "GameplayCue"))
	void AddGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);
	
	UFUNCTION(BlueprintCallable, Category = "OFAbilitySystemComponent", meta = (AutoCreateRefTerm = "GameplayCueParameters", GameplayTagFilter = "GameplayCue"))
	void RemoveGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);
	
	UFUNCTION(BlueprintCallable, Category = "OFAbilitySystemComponent")
	FGameplayAbilitySpecHandle GrantAbilityOfType(const TSubclassOf<UGameplayAbility> AbilityType, const bool bRemoveAfterActivation);
	
	UFUNCTION(BlueprintImplementableEvent, Blueprintpure)
	TArray<FOFAttributeApplication> GetCustomDefaultAttributes() const;
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDefaultAbilitiesAndAttributesGranted);
	
	UPROPERTY(BlueprintAssignable)
	FOnDefaultAbilitiesAndAttributesGranted OnDefaultAbilitiesAndAttributesGranted;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<TSubclassOf<UGameplayAbility>> DefaultAbilities;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FOFAbilityInputCombo> DefaultAbilitiesWithInputBinding;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FOFAttributeApplication> DefaultAttributes;
	
protected:
	void GrantDefaultAbilitiesAndAttributes();
	
	UFUNCTION()
	void OnPawnControllerChanged(APawn* Pawn, AController* NewController);
	
	TArray<FGameplayAbilitySpecHandle> DefaultAbilitySpecHandles;
	
	UPROPERTY(transient)
	TArray<UAttributeSet*> AddedAttributes;
};

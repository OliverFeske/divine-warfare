﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InputTriggers.h"
#include "Components/PawnComponent.h"
#include "OFPlayerControlsComponent.generated.h"

class UEnhancedInputLocalPlayerSubsystem;
class UEnhancedInputComponent;
class UInputMappingContext;
class UInputAction;

UCLASS(Blueprintable, BlueprintType, meta = (BlueprintSpawnableComponent))
class OFGAMEPLAYABILITYSYSTEMCORE_API UOFPlayerControlsComponent : public UPawnComponent
{
	GENERATED_BODY()
	
public:
	//~ Begin UActorComponent interface
	virtual void OnRegister() override;
	virtual void OnUnregister() override;
	//~ End UActorComponent interface

	/** Input mapping to add to the input system */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Controls")
	UInputMappingContext* InputMappingContext = nullptr;

	/** Priority to bind mapping context with */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Controls")
	int InputPriority = 0;

protected:
	/** Native/BP Event to set up player controls */
	UFUNCTION(BlueprintNativeEvent, Category = "Player Controls")
	void SetupPlayerControls(UEnhancedInputComponent* PlayerInputComponent);
	virtual void SetupPlayerControls_Implementation(UEnhancedInputComponent* PlayerInputComponent) {}

	/** Native/BP Event to undo control setup */
	UFUNCTION(BlueprintNativeEvent, Category = "Player Controls")
	void TeardownPlayerControls(UEnhancedInputComponent* PlayerInputComponent);
	virtual void TeardownPlayerControls_Implementation(UEnhancedInputComponent* PlayerInputComponent) {}

	/** Wrapper function for binding to this input component */
	template<class UserClass, typename FuncType>
	bool BindInputAction(const UInputAction* Action, const ETriggerEvent EventType, UserClass* Object, FuncType Func);
	
	/** Called when pawn restarts, bound to dynamic delegate */
	UFUNCTION()
	virtual void OnPawnRestarted(APawn* Pawn);

	/** Called when pawn restarts, bound to dynamic delegate */
	UFUNCTION()
	virtual void OnControllerChanged(APawn* Pawn, AController* OldController, AController* NewController);

	virtual void SetupInputComponent(APawn* Pawn);
	virtual void ReleaseInputComponent(AController* OldController = nullptr);
	UEnhancedInputLocalPlayerSubsystem* GetEnhancedInputSubsystem(AController* OldController = nullptr) const;

	/** The bound input component. */
	UPROPERTY(transient)
	UEnhancedInputComponent* InputComponent;
};
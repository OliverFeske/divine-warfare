﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OFPlayerControlsComponent.h"
#include "GameplayAbilitySpec.h"
#include "EnhancedInputComponent.h"
#include "OFAbilityInputBindingComponent.generated.h"

class UInputAction;
class UAbilitySystemComponent;
class UEnhancedInputComponent;

struct FOFAbilityInputID
{
protected:
	uint32 ID = 0;
	static uint32 NextID;

public:
	FOFAbilityInputID()
	{
		ID = TNumericLimits<uint32>().Max() == NextID ? 0 : ++NextID;
	}

	uint32 GetID() const { return ID; }
	
	friend uint32 GetTypeHash(const FOFAbilityInputID& A) { return GetTypeHash(A.ID); }
	bool operator==(const FOFAbilityInputID& Other) const { return ID == Other.ID; }
	bool operator!=(const FOFAbilityInputID& Other) const { return ID != Other.ID; }
};

USTRUCT()
struct FOFAbilityInputBinding
{
	GENERATED_BODY()

	int32  InputID = 0;
	uint32 OnPressedHandle = 0;
	uint32 OnReleasedHandle = 0;
	TArray<FGameplayAbilitySpecHandle> BoundAbilitiesStack;
};

/**
 *	This is a copy of the AncientGame AbilityInputBindingComponent with some improvements and some fixes.
 */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class OFGAMEPLAYABILITYSYSTEMCORE_API UOFAbilityInputBindingComponent : public UOFPlayerControlsComponent
{
	GENERATED_BODY()
	
public:
	UOFAbilityInputBindingComponent();
	
	//~ Begin UActorComponent Interface
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	//~ End UActorComponent Interface
	
	UFUNCTION(BlueprintCallable, Category = "AncientGame|Abilities")
	//~ Begin OF Change: Make functions virtual
	virtual bool SetInputBinding(UInputAction* InputAction, FGameplayAbilitySpecHandle AbilityHandle);
	
	UFUNCTION(BlueprintCallable, Category = "AncientGame|Abilities")
	virtual void ClearInputBinding(FGameplayAbilitySpecHandle AbilityHandle);
	
	UFUNCTION(BlueprintCallable, Category = "AncientGame|Abilities")
	virtual void ClearAbilityBindings(UInputAction* InputAction);
	//~ End OF Change: Make functions virtual
	
	//~ Begin UPlayerControlsComponent interface
	virtual void SetupPlayerControls_Implementation(UEnhancedInputComponent* PlayerInputComponent) override;
	virtual void ReleaseInputComponent(AController* OldController) override;
	//~ End UPlayerControlsComponent interface
	
private:
	void ResetBindings();
	void RunAbilitySystemSetup();
	void OnAbilityInputPressed(UInputAction* InputAction);
	void OnAbilityInputReleased(UInputAction* InputAction);
	
	void RemoveEntry(const UInputAction* InputAction);
	
	FGameplayAbilitySpec* FindAbilitySpec(const FGameplayAbilitySpecHandle Handle) const;
	void TryBindAbilityInput(UInputAction* InputAction, FOFAbilityInputBinding& AbilityInputBinding);
	
	UPROPERTY(transient)
	UAbilitySystemComponent* AbilityComponent;
	
	UPROPERTY(transient)
	TMap<UInputAction*, FOFAbilityInputBinding> MappedAbilities;
	
	static constexpr int32 InvalidInputID = 0;
	
	UPROPERTY()
	TMap<FGameplayAbilitySpecHandle, UInputAction*> UnresolvedAbilities;
};

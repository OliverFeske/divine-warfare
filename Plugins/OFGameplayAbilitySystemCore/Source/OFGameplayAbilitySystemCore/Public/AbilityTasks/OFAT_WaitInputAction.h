﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "InputAction.h"
#include "OFAT_WaitInputAction.generated.h"

class UInputAction;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInputActionEvent, const FInputActionValue&, InputActionValue);

/**
 * 
 */
UCLASS()
class OFGAMEPLAYABILITYSYSTEMCORE_API UOFAT_WaitInputAction : public UAbilityTask
{
	GENERATED_BODY()

public:
	UOFAT_WaitInputAction() {}
	
	//~ Begin UGameplayTask Interface
	virtual void Activate() override;
	virtual FString GetDebugString() const override;
	//~ End UGameplayTask Interface

	UFUNCTION(BlueprintCallable, Category = "Ability|Tasks", meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static UOFAT_WaitInputAction* WaitForInputAction(
		UGameplayAbility* OwningAbility,
		FName TaskInstanceName,
		UInputAction* InputAction);
	
	UPROPERTY(BlueprintAssignable)
	FOnInputActionEvent OnActionStarted;

	UPROPERTY(BlueprintAssignable)
	FOnInputActionEvent OnActionTriggered;

	UPROPERTY(BlueprintAssignable)
	FOnInputActionEvent OnActionOngoing;

	UPROPERTY(BlueprintAssignable)
	FOnInputActionEvent OnActionCompleted;

	UPROPERTY(BlueprintAssignable)
	FOnInputActionEvent OnActionCanceled;

	UPROPERTY(BlueprintAssignable)
	FOnInputActionEvent OnActionNone;
	
private:
	UFUNCTION()
	void ActionStarted(const FInputActionValue& Value);

	UFUNCTION()
	void ActionTriggered(const FInputActionValue& Value);

	UFUNCTION()
	void ActionOngoing(const FInputActionValue& Value);

	UFUNCTION()
	void ActionCompleted(const FInputActionValue& Value);

	UFUNCTION()
	void ActionCanceled(const FInputActionValue& Value);

	UFUNCTION()
	void ActionNone(const FInputActionValue& Value);
	
	UPROPERTY()
	UInputAction* InputAction = nullptr;
};

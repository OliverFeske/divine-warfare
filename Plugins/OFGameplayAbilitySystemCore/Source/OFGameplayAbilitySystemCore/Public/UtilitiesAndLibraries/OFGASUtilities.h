﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilitySpec.h"
#include "GameplayEffectTypes.h"
#include "OFGASUtilities.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogOFGenericGASApplication, Log, All);

class UInputAction;
class UGameplayAbility;
class UGameplayEffect;

struct FGameplayTagContainer;

USTRUCT(BlueprintType)
struct FOFAbilityInputCombo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<UGameplayAbility> Ability;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputAction* ActivationInput = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bRemoveAbilityAfterActivation = false;
};

USTRUCT(BlueprintType)
struct FOFGenericGASApplicationData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<UGameplayEffect>> EffectsToAdd;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<UGameplayEffect>> EffectsToRemove;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FGameplayTagContainer> EffectsToRemoveByTags;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<UGameplayAbility>> AbilitiesToAdd;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FOFAbilityInputCombo> AbilitiesWithInputToAdd;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<UGameplayAbility>> AbilitiesToRemove;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FGameplayTagContainer> AbilitiesToRemoveByTags;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FGameplayTagContainer> AbilitiesToRemoveByTagsExact;

	bool ContainsData() const
	{
		return EffectsToAdd.Num() > 0 || EffectsToRemove.Num() > 0 || EffectsToRemoveByTags.Num() > 0  || AbilitiesToAdd.Num() > 0
		|| AbilitiesWithInputToAdd.Num() > 0 || AbilitiesToRemove.Num() > 0 || AbilitiesToRemoveByTags.Num() > 0 || AbilitiesToRemoveByTagsExact.Num() > 0;
	}
};

USTRUCT(BlueprintType)
struct FOFGenericGASApplicationDataInfo
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FActiveGameplayEffectHandle> AddedEffects;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 RemovedEffectsByClass = 0;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 RemovedEffectsByTags = 0;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FGameplayAbilitySpecHandle> AddedAbilityHandles;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FGameplayAbilitySpecHandle> AddedAbilityWithInputHandles;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FGameplayAbilitySpecHandle> RemovedAbilityHandles;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FGameplayAbilitySpecHandle> RemovedAbilityByTagsHandles;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FGameplayAbilitySpecHandle> RemovedAbilityByTagsExactHandles;
	
	void Reset()
	{
		AddedEffects.Reset();
		RemovedEffectsByClass = 0;
		RemovedEffectsByTags = 0;
		AddedAbilityHandles.Reset();
		AddedAbilityWithInputHandles.Reset();
		RemovedAbilityHandles.Reset();
		RemovedAbilityByTagsHandles.Reset();
		RemovedAbilityByTagsExactHandles.Reset();
	}
};

UCLASS()
class OFGAMEPLAYABILITYSYSTEMCORE_API UOFGASUtilities : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"))
	static void ApplyGASApplicationDataToTarget(UObject* WorldContextObject, AActor* Instigator, UObject* AddedAbilitiesSourceObject,
		AActor* Target, const FOFGenericGASApplicationData& ApplicationData, FOFGenericGASApplicationDataInfo& OutInfo);
	
	UFUNCTION(BlueprintCallable)
	static void PrintGenericGASApplicationDataToLog(const FOFGenericGASApplicationDataInfo& GenericGASApplicationDataInfo);
};
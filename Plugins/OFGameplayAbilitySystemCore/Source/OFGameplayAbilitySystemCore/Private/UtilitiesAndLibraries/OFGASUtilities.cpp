﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFGASUtilities.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"
#include "OFAbilityInputBindingComponent.h"

DEFINE_LOG_CATEGORY(LogOFGenericGASApplication);

void UOFGASUtilities::ApplyGASApplicationDataToTarget(UObject* WorldContextObject, AActor* Instigator, UObject* AddedAbilitiesSourceObject,
	AActor* Target, const FOFGenericGASApplicationData& ApplicationData, FOFGenericGASApplicationDataInfo& OutInfo)
{
	OutInfo.Reset();
	
	UAbilitySystemComponent* TargetAbilitySystemComponent = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Target, true);
	if(!ensure(WorldContextObject) || !ensure(TargetAbilitySystemComponent) || ! ApplicationData.ContainsData() || !ensure(Instigator)
		|| !ensure(AddedAbilitiesSourceObject))
	{
		return;
	}
	
#if WITH_EDITOR
	auto LogWarning = [WorldContextObject, Instigator, AddedAbilitiesSourceObject, Target](const FString& Message)
	{
		UE_LOG(LogOFGenericGASApplication, Warning, TEXT("%s [WorldContextObject: %s | Instigator: %s | AddedAbilitiesSourceObject: %s | Target: %s]"),
			*Message,
			*FString(WorldContextObject ? WorldContextObject->GetName() : TEXT("NULL")),
			*FString(Instigator ? Instigator->GetName() : TEXT("NULL")),
			*FString(AddedAbilitiesSourceObject ? AddedAbilitiesSourceObject->GetName() : TEXT("NULL")),
			*FString(Target ? Target->GetName() : TEXT("NULL")));
	};
#endif
	
	for(TSubclassOf<UGameplayEffect> GameplayEffect : ApplicationData.EffectsToAdd)
	{
		if(GameplayEffect)
		{
			OutInfo.AddedEffects.Add(
				TargetAbilitySystemComponent->ApplyGameplayEffectToSelf(Cast<UGameplayEffect>(GameplayEffect->ClassDefaultObject),
					1.f, TargetAbilitySystemComponent->MakeEffectContext()));
		}
#if WITH_EDITOR
		else
		{
			LogWarning(TEXT("Empty GameplayEffect in: [EffectsToAdd]"));
		}
#endif
	}
	
	for(TSubclassOf<UGameplayEffect> GameplayEffect : ApplicationData.EffectsToRemove)
	{
		if(GameplayEffect)
		{
			TargetAbilitySystemComponent->RemoveActiveGameplayEffectBySourceEffect(GameplayEffect, TargetAbilitySystemComponent);
			
			OutInfo.RemovedEffectsByClass++;
		}
#if WITH_EDITOR
		else
		{
			LogWarning(TEXT("Empty GameplayEffect in: [EffectsToRemove]"));
		}
#endif
	}
	
	for(const FGameplayTagContainer& TagContainer : ApplicationData.EffectsToRemoveByTags)
	{
		if(!TagContainer.IsEmpty())
		{
			OutInfo.RemovedEffectsByTags = TargetAbilitySystemComponent->RemoveActiveEffectsWithTags(TagContainer);
		}
#if WITH_EDITOR
		else
		{
			LogWarning(TEXT("Empty TagContainer in: [EffectsToRemoveByTags]"));
		}
#endif
	}
	
	for(TSubclassOf<UGameplayAbility> Ability : ApplicationData.AbilitiesToAdd)
	{
		if(Ability)
		{
			FGameplayAbilitySpec GameplayAbilitySpec = FGameplayAbilitySpec(Ability, 1, 0, AddedAbilitiesSourceObject);
			OutInfo.AddedAbilityHandles.Add(TargetAbilitySystemComponent->GiveAbility(GameplayAbilitySpec));
		}
#if WITH_EDITOR
		else
		{
			LogWarning(TEXT("Invalid Ability in: [AbilitiesToAdd]"));
		}
#endif
	}
	
	for(const FOFAbilityInputCombo& AbilityInputCombo : ApplicationData.AbilitiesWithInputToAdd)
	{
		if(AbilityInputCombo.Ability)
		{
			FGameplayAbilitySpec GameplayAbilitySpec = FGameplayAbilitySpec(AbilityInputCombo.Ability, 1, 0, AddedAbilitiesSourceObject);
			GameplayAbilitySpec.RemoveAfterActivation = AbilityInputCombo.bRemoveAbilityAfterActivation;
			
			const FGameplayAbilitySpecHandle AbilitySpecHandle = TargetAbilitySystemComponent->GiveAbility(GameplayAbilitySpec);
			OutInfo.AddedAbilityWithInputHandles.Add(AbilitySpecHandle);
			
			if(AbilityInputCombo.ActivationInput)
			{
				if(UOFAbilityInputBindingComponent* OFAbilityInputBindingComponent = Instigator->FindComponentByClass<UOFAbilityInputBindingComponent>())
				{
					OFAbilityInputBindingComponent->SetInputBinding(AbilityInputCombo.ActivationInput, AbilitySpecHandle);
				}
			}
		}
#if WITH_EDITOR
		else
		{
			LogWarning(TEXT("Invalid AbilityInputCombo in: [AbilitiesWithInputToAdd]"));
		}
#endif
	}
	
	for(TSubclassOf<UGameplayAbility> Ability : ApplicationData.AbilitiesToRemove)
	{
		if(Ability)
		{
			const UGameplayAbility* const InAbilityCDO = Ability.GetDefaultObject();
			
			TArray<FGameplayAbilitySpecHandle> AbilityHandles;
			TargetAbilitySystemComponent->GetAllAbilities(AbilityHandles);
			
			for(const FGameplayAbilitySpecHandle& Handle : AbilityHandles)
			{
				if(const FGameplayAbilitySpec* AbilitySpec = TargetAbilitySystemComponent->FindAbilitySpecFromHandle(Handle))
				{
					if(AbilitySpec->Ability == InAbilityCDO && AbilitySpec->SourceObject == AddedAbilitiesSourceObject)
					{
						TargetAbilitySystemComponent->ClearAbility(Handle);
						OutInfo.RemovedAbilityHandles.Add(Handle);
						break;
					}
				}
			}
		}
#if WITH_EDITOR
		else
		{
			LogWarning(TEXT("Invalid Ability in: [AbilitiesToRemove]"));
		}
#endif
	}
	
	for(const FGameplayTagContainer& TagContainer : ApplicationData.AbilitiesToRemoveByTags)
	{
		if(!TagContainer.IsEmpty())
		{
			TArray<FGameplayAbilitySpecHandle> FoundAbilities;
			TargetAbilitySystemComponent->FindAllAbilitiesWithTags(FoundAbilities, TagContainer, false);
			for(const FGameplayAbilitySpecHandle& AbilitySpecHandle : FoundAbilities)
			{
				TargetAbilitySystemComponent->ClearAbility(AbilitySpecHandle);
				OutInfo.RemovedAbilityByTagsHandles.Add(AbilitySpecHandle);
			}
		}
#if WITH_EDITOR
		else
		{
			LogWarning(TEXT("Empty TagContainer in: [AbilitiesToRemoveByTags]"));
		}
#endif
	}
	
	for(const FGameplayTagContainer& TagContainer : ApplicationData.AbilitiesToRemoveByTagsExact)
	{
		if(!TagContainer.IsEmpty())
		{
			TArray<FGameplayAbilitySpecHandle> FoundAbilities;
			TargetAbilitySystemComponent->FindAllAbilitiesWithTags(FoundAbilities, TagContainer, true);
			for(const FGameplayAbilitySpecHandle& AbilitySpecHandle : FoundAbilities)
			{
				TargetAbilitySystemComponent->ClearAbility(AbilitySpecHandle);
				OutInfo.RemovedAbilityByTagsExactHandles.Add(AbilitySpecHandle);
			}
		}
#if WITH_EDITOR
		else
		{
			LogWarning(TEXT("Empty TagContainer in: [AbilitiesToRemoveByTagsExact]"));
		}
#endif
	}
}

void UOFGASUtilities::PrintGenericGASApplicationDataToLog(const FOFGenericGASApplicationDataInfo& GenericGASApplicationDataInfo)
{
#if WITH_EDITOR
	auto ActiveGameplayEffectHandleArrayToString = [](const TArray<FActiveGameplayEffectHandle>& ActiveGameplayEffectHandleArray)
	{
		FString ReturnString = TEXT("");
		
		if(ActiveGameplayEffectHandleArray.Num() > 0)
		{
			for(const FActiveGameplayEffectHandle& ActiveGameplayEffectHandle : ActiveGameplayEffectHandleArray)
			{
				ReturnString += FString::Printf(TEXT("[%s]"), *ActiveGameplayEffectHandle.ToString());
			}
		}
		
		return ReturnString;
	};
	
	auto GameplayAbilitySpecHandleArrayToString = [](const TArray<FGameplayAbilitySpecHandle>& GameplayAbilitySpecHandleArray)
	{
		FString ReturnString = TEXT("");
	
		if(GameplayAbilitySpecHandleArray.Num() > 0)
		{
			for(const FGameplayAbilitySpecHandle& AbilitySpecHandle : GameplayAbilitySpecHandleArray)
			{
				ReturnString += FString::Printf(TEXT("[%s]"), *AbilitySpecHandle.ToString());
			}
		}
	
		return ReturnString;
	};

	UE_LOG(LogOFGenericGASApplication, Log, TEXT("---------------------------------------------------------------------------------------------------"))
	
	UE_LOG(LogOFGenericGASApplication, Log, TEXT("AddedEffects:%s -> %s"),
		*FString::FromInt(GenericGASApplicationDataInfo.AddedEffects.Num()),
		*ActiveGameplayEffectHandleArrayToString(GenericGASApplicationDataInfo.AddedEffects));
	
	UE_LOG(LogOFGenericGASApplication, Log, TEXT("RemovedEffectsByClass:%s"),
		*FString::FromInt(GenericGASApplicationDataInfo.RemovedEffectsByClass));
	
	UE_LOG(LogOFGenericGASApplication, Log, TEXT("RemovedEffectsByClass:%s"),
		*FString::FromInt(GenericGASApplicationDataInfo.RemovedEffectsByTags));
	
	UE_LOG(LogOFGenericGASApplication, Log, TEXT("AddedAbilityHandles:%s -> %s"),
		*FString::FromInt(GenericGASApplicationDataInfo.AddedAbilityHandles.Num()),
		*GameplayAbilitySpecHandleArrayToString(GenericGASApplicationDataInfo.AddedAbilityHandles));
	
	UE_LOG(LogOFGenericGASApplication, Log, TEXT("AddedAbilityWithInputHandles:%s -> %s"),
		*FString::FromInt(GenericGASApplicationDataInfo.AddedAbilityWithInputHandles.Num()),
		*GameplayAbilitySpecHandleArrayToString(GenericGASApplicationDataInfo.AddedAbilityWithInputHandles));
	
	UE_LOG(LogOFGenericGASApplication, Log, TEXT("RemovedAbilityHandles:%s -> %s"),
		*FString::FromInt(GenericGASApplicationDataInfo.RemovedAbilityHandles.Num()),
		*GameplayAbilitySpecHandleArrayToString(GenericGASApplicationDataInfo.RemovedAbilityHandles));
	
	UE_LOG(LogOFGenericGASApplication, Log, TEXT("RemovedAbilityByTagsHandles:%s -> %s"),
		*FString::FromInt(GenericGASApplicationDataInfo.RemovedAbilityByTagsHandles.Num()),
		*GameplayAbilitySpecHandleArrayToString(GenericGASApplicationDataInfo.RemovedAbilityByTagsHandles));
	
	UE_LOG(LogOFGenericGASApplication, Log, TEXT("RemovedAbilityByTagsExactHandles:%s -> %s"),
		*FString::FromInt(GenericGASApplicationDataInfo.RemovedAbilityByTagsExactHandles.Num()),
		*GameplayAbilitySpecHandleArrayToString(GenericGASApplicationDataInfo.RemovedAbilityByTagsExactHandles));
	
	UE_LOG(LogOFGenericGASApplication, Log, TEXT("---------------------------------------------------------------------------------------------------"))
#endif
}
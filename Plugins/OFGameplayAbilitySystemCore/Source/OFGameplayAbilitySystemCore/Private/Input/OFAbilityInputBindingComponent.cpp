﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFAbilityInputBindingComponent.h"

#include "AbilitySystemComponent.h"
#include "EnhancedInputComponent.h"
#include "AbilitySystemGlobals.h"
#include "InputAction.h"
#include "Kismet/KismetSystemLibrary.h"

uint32 FOFAbilityInputID::NextID = 0;

UOFAbilityInputBindingComponent::UOFAbilityInputBindingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UOFAbilityInputBindingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UKismetSystemLibrary::PrintString(this, FString(TEXT("UnresolvedAbilities: ") + FString::FromInt(UnresolvedAbilities.Num())), true, true, FColor::Cyan, 0.f);
	
	TArray<FGameplayAbilitySpecHandle> AbilitySpecHandlesToRemove;
	TMap<FGameplayAbilitySpecHandle, UInputAction*> UnresolvedAbilities_C = UnresolvedAbilities;
	for(const TTuple<FGameplayAbilitySpecHandle, UInputAction*>& AbilityInputDuo : UnresolvedAbilities_C)
	{
		if(SetInputBinding(AbilityInputDuo.Value, AbilityInputDuo.Key))
		{
			AbilitySpecHandlesToRemove.Add(AbilityInputDuo.Key);
		}
	}

	for(const FGameplayAbilitySpecHandle AbilitySpecHandle : AbilitySpecHandlesToRemove)
	{
		UnresolvedAbilities.Remove(AbilitySpecHandle);
	}
	
	if(UnresolvedAbilities.Num() <= 0)
	{
		SetComponentTickEnabled(false);
	}
}

bool UOFAbilityInputBindingComponent::SetInputBinding(UInputAction* InputAction, FGameplayAbilitySpecHandle AbilityHandle)
{
	if(!InputAction) { return false; }
	
	FGameplayAbilitySpec* BindingAbility = FindAbilitySpec(AbilityHandle);
	
	/** This needs to be done, because in some cases AbilityScopeLockCount > 0, which will not add the ability in this frame, but later. */
	if(!BindingAbility && !UnresolvedAbilities.Contains(AbilityHandle))
	{
		UnresolvedAbilities.Add(AbilityHandle, InputAction);
		SetComponentTickEnabled(true);
		return false;
	}
	
	FOFAbilityInputBinding* AbilityInputBinding = MappedAbilities.Find(InputAction);
	if (AbilityInputBinding)
	{
		FGameplayAbilitySpec* OldBoundAbility = FindAbilitySpec(AbilityInputBinding->BoundAbilitiesStack.Top());
		if (OldBoundAbility && OldBoundAbility->InputID == AbilityInputBinding->InputID)
		{
			OldBoundAbility->InputID = InvalidInputID;
		}
	}
	else
	{
		AbilityInputBinding = &MappedAbilities.Add(InputAction);
		AbilityInputBinding->InputID = FOFAbilityInputID().GetID();
	}
	
	if (BindingAbility)
	{
		BindingAbility->InputID = AbilityInputBinding->InputID;
	}
	
	AbilityInputBinding->BoundAbilitiesStack.Push(AbilityHandle);
	TryBindAbilityInput(InputAction, *AbilityInputBinding);

	return true;
}

void UOFAbilityInputBindingComponent::ClearInputBinding(FGameplayAbilitySpecHandle AbilityHandle)
{
	if (FGameplayAbilitySpec* FoundAbility = FindAbilitySpec(AbilityHandle))
	{
		// Find the mapping for this ability
		auto MappedIterator = MappedAbilities.CreateIterator();
		while (MappedIterator)
		{
			if (MappedIterator.Value().InputID == FoundAbility->InputID)
			{
				break;
			}

			++MappedIterator;
		}

		if (MappedIterator)
		{
			FOFAbilityInputBinding& AbilityInputBinding = MappedIterator.Value();

			if (AbilityInputBinding.BoundAbilitiesStack.Remove(AbilityHandle) > 0)
			{
				if (AbilityInputBinding.BoundAbilitiesStack.Num() > 0)
				{
					FGameplayAbilitySpec* StackedAbility = FindAbilitySpec(AbilityInputBinding.BoundAbilitiesStack.Top());
					if (StackedAbility && StackedAbility->InputID == 0)
					{
						StackedAbility->InputID = AbilityInputBinding.InputID;
					}
				}
				else
				{
					// NOTE: This will invalidate the `AbilityInputBinding` ref above
					RemoveEntry(MappedIterator.Key());
				}
				// DO NOT act on `AbilityInputBinding` after here (it could have been removed)


				FoundAbility->InputID = InvalidInputID;
			}
		}
	}
}

void UOFAbilityInputBindingComponent::ClearAbilityBindings(UInputAction* InputAction)
{
	RemoveEntry(InputAction);
}

void UOFAbilityInputBindingComponent::ResetBindings()
{
	for (TTuple<UInputAction*, FOFAbilityInputBinding>& InputBinding : MappedAbilities)
	{
		if (InputComponent)
		{
			InputComponent->RemoveBindingByHandle(InputBinding.Value.OnPressedHandle);
			InputComponent->RemoveBindingByHandle(InputBinding.Value.OnReleasedHandle);
		}

		if (AbilityComponent)
		{
			const int32 ExpectedInputID = InputBinding.Value.InputID;

			for (const FGameplayAbilitySpecHandle AbilityHandle : InputBinding.Value.BoundAbilitiesStack)
			{
				FGameplayAbilitySpec* FoundAbility = AbilityComponent->FindAbilitySpecFromHandle(AbilityHandle);
				if (FoundAbility && FoundAbility->InputID == ExpectedInputID)
				{
					FoundAbility->InputID = InvalidInputID;
				}
			}
		}
	}

	AbilityComponent = nullptr;
}

void UOFAbilityInputBindingComponent::SetupPlayerControls_Implementation(UEnhancedInputComponent* PlayerInputComponent)
{
	ResetBindings();

	for (const TTuple<UInputAction*, FOFAbilityInputBinding>& Ability : MappedAbilities)
	{
		UInputAction* InputAction = Ability.Key;

		InputComponent->BindAction(InputAction, ETriggerEvent::Started, this, &UOFAbilityInputBindingComponent::OnAbilityInputPressed, InputAction);

		InputComponent->BindAction(InputAction, ETriggerEvent::Completed, this, &UOFAbilityInputBindingComponent::OnAbilityInputReleased, InputAction);
	}

	RunAbilitySystemSetup();
}

void UOFAbilityInputBindingComponent::ReleaseInputComponent(AController* OldController)
{
	ResetBindings();

	Super::ReleaseInputComponent();
}

void UOFAbilityInputBindingComponent::RunAbilitySystemSetup()
{
	AActor* MyOwner = GetOwner();
	check(MyOwner);

	AbilityComponent = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(MyOwner);
	if (AbilityComponent)
	{
		for (TTuple<UInputAction*, FOFAbilityInputBinding>& InputBinding : MappedAbilities)
		{
			const int32 NewInputID = FOFAbilityInputID().GetID();
			InputBinding.Value.InputID = NewInputID;

			for (const FGameplayAbilitySpecHandle AbilityHandle : InputBinding.Value.BoundAbilitiesStack)
			{
				if (FGameplayAbilitySpec* FoundAbility = AbilityComponent->FindAbilitySpecFromHandle(AbilityHandle))
				{
					FoundAbility->InputID = NewInputID;
				}
			}
		}
	}
}

void UOFAbilityInputBindingComponent::OnAbilityInputPressed(UInputAction* InputAction)
{
	// The AbilitySystemComponent may not have been valid when we first bound input... try again.
	if (!AbilityComponent)
	{
		RunAbilitySystemSetup();
	}

	if (AbilityComponent)
	{
		FOFAbilityInputBinding* FoundBinding = MappedAbilities.Find(InputAction);
		if (FoundBinding && ensure(FoundBinding->InputID != InvalidInputID))
		{
			AbilityComponent->AbilityLocalInputPressed(FoundBinding->InputID);
		}
	}
}

void UOFAbilityInputBindingComponent::OnAbilityInputReleased(UInputAction* InputAction)
{
	if (AbilityComponent)
	{
		FOFAbilityInputBinding* FoundBinding = MappedAbilities.Find(InputAction);
		if (FoundBinding && ensure(FoundBinding->InputID != InvalidInputID))
		{
			AbilityComponent->AbilityLocalInputReleased(FoundBinding->InputID);
		}
	}
}

void UOFAbilityInputBindingComponent::RemoveEntry(const UInputAction* InputAction)
{
	if (FOFAbilityInputBinding* Bindings = MappedAbilities.Find(InputAction))
	{
		if (InputComponent)
		{
			InputComponent->RemoveBindingByHandle(Bindings->OnPressedHandle);
			InputComponent->RemoveBindingByHandle(Bindings->OnReleasedHandle);
		}

		for (const FGameplayAbilitySpecHandle AbilityHandle : Bindings->BoundAbilitiesStack)
		{
			FGameplayAbilitySpec* AbilitySpec = FindAbilitySpec(AbilityHandle);
			if (AbilitySpec && AbilitySpec->InputID == Bindings->InputID)
			{
				AbilitySpec->InputID = InvalidInputID;
			}
		}

		MappedAbilities.Remove(InputAction);
	}
}

FGameplayAbilitySpec* UOFAbilityInputBindingComponent::FindAbilitySpec(const FGameplayAbilitySpecHandle Handle) const
{
	FGameplayAbilitySpec* FoundAbility = nullptr;
	if (AbilityComponent)
	{
		FoundAbility = AbilityComponent->FindAbilitySpecFromHandle(Handle);
	}
	return FoundAbility;
}

void UOFAbilityInputBindingComponent::TryBindAbilityInput(UInputAction* InputAction, FOFAbilityInputBinding& AbilityInputBinding)
{
	if (InputComponent)
	{
		if (AbilityInputBinding.OnPressedHandle == 0)
		{
			AbilityInputBinding.OnPressedHandle = InputComponent->BindAction(InputAction, ETriggerEvent::Started, this, &UOFAbilityInputBindingComponent::OnAbilityInputPressed, InputAction).GetHandle();
		}

		if (AbilityInputBinding.OnReleasedHandle == 0)
		{
			AbilityInputBinding.OnReleasedHandle = InputComponent->BindAction(InputAction, ETriggerEvent::Completed, this, &UOFAbilityInputBindingComponent::OnAbilityInputReleased, InputAction).GetHandle();
		}
	}
}

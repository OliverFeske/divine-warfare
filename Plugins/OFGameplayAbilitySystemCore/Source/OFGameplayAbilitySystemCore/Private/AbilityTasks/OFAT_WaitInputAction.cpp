﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFAT_WaitInputAction.h"
#include "EnhancedInputComponent.h"

void UOFAT_WaitInputAction::Activate()
{
	if(!InputAction)
	{
		EndTask();
		return;
	}
	
	const FGameplayAbilityActorInfo* CurrentActorInfo = Ability ? Ability->GetCurrentActorInfo() : nullptr;
	APawn* AvatarPawn = CurrentActorInfo ? Cast<APawn>(CurrentActorInfo->AvatarActor) : nullptr;
	UEnhancedInputComponent* PlayerEnhancedInputComponent = AvatarPawn ? Cast<UEnhancedInputComponent>(AvatarPawn->InputComponent) : nullptr;
	
	if(!PlayerEnhancedInputComponent)
	{
		EndTask();
		return;
	}
	
	PlayerEnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Started, this, &UOFAT_WaitInputAction::ActionStarted);
	PlayerEnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Triggered, this, &UOFAT_WaitInputAction::ActionTriggered);
	PlayerEnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Ongoing, this, &UOFAT_WaitInputAction::ActionOngoing);
	PlayerEnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Completed, this, &UOFAT_WaitInputAction::ActionCompleted);
	PlayerEnhancedInputComponent->BindAction(InputAction, ETriggerEvent::Canceled, this, &UOFAT_WaitInputAction::ActionCanceled);
	PlayerEnhancedInputComponent->BindAction(InputAction, ETriggerEvent::None, this, &UOFAT_WaitInputAction::ActionNone);
}

FString UOFAT_WaitInputAction::GetDebugString() const
{
	return FString::Printf(TEXT("WaitInputAction. InputAction: %s"), *GetNameSafe(InputAction));
}

UOFAT_WaitInputAction* UOFAT_WaitInputAction::WaitForInputAction(UGameplayAbility* OwningAbility, FName TaskInstanceName, UInputAction* InputAction)
{
	UOFAT_WaitInputAction* NewTask = NewAbilityTask<UOFAT_WaitInputAction>(OwningAbility, TaskInstanceName);
	NewTask->InputAction = InputAction;

	return NewTask;
}

void UOFAT_WaitInputAction::ActionStarted(const FInputActionValue& Value)
{
	if(ShouldBroadcastAbilityTaskDelegates())
	{
		OnActionStarted.Broadcast(Value);
	}
}

void UOFAT_WaitInputAction::ActionTriggered(const FInputActionValue& Value)
{
	if(ShouldBroadcastAbilityTaskDelegates())
	{
		OnActionTriggered.Broadcast(Value);
	}
}

void UOFAT_WaitInputAction::ActionOngoing(const FInputActionValue& Value)
{
	if(ShouldBroadcastAbilityTaskDelegates())
	{
		OnActionOngoing.Broadcast(Value);
	}
}

void UOFAT_WaitInputAction::ActionCompleted(const FInputActionValue& Value)
{
	if(ShouldBroadcastAbilityTaskDelegates())
	{
		OnActionCompleted.Broadcast(Value);
	}
}

void UOFAT_WaitInputAction::ActionCanceled(const FInputActionValue& Value)
{
	if(ShouldBroadcastAbilityTaskDelegates())
	{
		OnActionCanceled.Broadcast(Value);
	}
}

void UOFAT_WaitInputAction::ActionNone(const FInputActionValue& Value)
{
	if(ShouldBroadcastAbilityTaskDelegates())
	{
		OnActionNone.Broadcast(Value);
	}
}

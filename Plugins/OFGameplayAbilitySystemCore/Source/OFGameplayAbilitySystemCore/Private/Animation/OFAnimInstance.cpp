﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFAnimInstance.h"

#include "AbilitySystemComponent.h"

void UOFAnimInstance::NativeInitializeAnimation()
{
	if (const AActor* MyActor = GetOwningActor())
	{
		if (UAbilitySystemComponent* AbilityComponent = MyActor->FindComponentByClass<UAbilitySystemComponent>())
		{
			InitializeWithAbilitySystem(AbilityComponent);
		}
	}
	
	Super::NativeInitializeAnimation();
}

void UOFAnimInstance::InitializeWithAbilitySystem(UAbilitySystemComponent* AbilityComponent)
{
	GameplayTagPropertyMap.Initialize(this, AbilityComponent);
	ReceiveAbilitySystem(AbilityComponent);
}

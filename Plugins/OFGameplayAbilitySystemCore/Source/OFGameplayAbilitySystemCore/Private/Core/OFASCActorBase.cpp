﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFASCActorBase.h"

#include "OFAbilitySystemComponent.h"

AOFASCActorBase::AOFASCActorBase()
{
	PrimaryActorTick.bCanEverTick = false;

	AbilitySystemComponent = CreateDefaultSubobject<UOFAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
}

void AOFASCActorBase::BeginPlay()
{
	Super::BeginPlay();
}

UAbilitySystemComponent* AOFASCActorBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFGameplayAbility.h"

#include "AbilitySystemComponent.h"

UOFGameplayAbility::UOFGameplayAbility()
{
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
}

void UOFGameplayAbility::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	Super::OnAvatarSet(ActorInfo, Spec);

	if(ensure(ActorInfo) && ensure(ActorInfo->AbilitySystemComponent.Get()) && bActivateAbilityOnGranted)
	{
		ActorInfo->AbilitySystemComponent->TryActivateAbility(Spec.Handle, false);
	}
}

bool UOFGameplayAbility::CheckCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, FGameplayTagContainer* OptionalRelevantTags) const
{
	return Super::CheckCost(Handle, ActorInfo, OptionalRelevantTags) && OFCheckCost(Handle, *ActorInfo, *OptionalRelevantTags);
}

void UOFGameplayAbility::ApplyCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) const
{
	OFApplyCost(Handle, *ActorInfo, ActivationInfo);
	Super::ApplyCost(Handle, ActorInfo, ActivationInfo);
}

FGameplayAbilityTargetDataHandle UOFGameplayAbility::MakeGameplayAbilityTargetDataHandleFromActorArray(const TArray<AActor*> TargetActors)
{
	if(TargetActors.Num() > 0)
	{
		FGameplayAbilityTargetData_ActorArray* ActorArray = new FGameplayAbilityTargetData_ActorArray();
		ActorArray->TargetActorArray.Append(TargetActors);
		return FGameplayAbilityTargetDataHandle(ActorArray);
	}

	return FGameplayAbilityTargetDataHandle();
}

FGameplayAbilityTargetDataHandle UOFGameplayAbility::MakeGameplayAbilityTargetDataHandleFromHitResults(const TArray<FHitResult> HitResults)
{
	FGameplayAbilityTargetDataHandle TargetDataHandle;

	for(const FHitResult& HitResult : HitResults)
	{
		FGameplayAbilityTargetData_SingleTargetHit* SingleTargetHit = new FGameplayAbilityTargetData_SingleTargetHit(HitResult);
		TargetDataHandle.Add(SingleTargetHit);
	}

	return TargetDataHandle;
}

FOFGameplayEffectContainerSpec UOFGameplayAbility::MakeEffectContainerSpecFromEffectContainer(const FOFGameplayEffectContainer& Container, const FGameplayEventData& EventData, int32 OverrideGameplayLevel)
{
	FOFGameplayEffectContainerSpec ReturnSpec;
	
	if(INDEX_NONE == OverrideGameplayLevel)
	{
		OverrideGameplayLevel = GetAbilityLevel();
	}

	for(const TSubclassOf<UGameplayEffect>& GameplayEffectClass : Container.GameplayEffectClasses)
	{
		ReturnSpec.TargetGameplayEffectSpecs.Add(MakeOutgoingGameplayEffectSpec(GameplayEffectClass, OverrideGameplayLevel));
	}

	return ReturnSpec;
}

TArray<FActiveGameplayEffectHandle> UOFGameplayAbility::ApplyEffectContainerSpec(const FOFGameplayEffectContainerSpec& ContainerSpec)
{
	TArray<FActiveGameplayEffectHandle> AllEffects;

	for(const FGameplayEffectSpecHandle& SpecHandle : ContainerSpec.TargetGameplayEffectSpecs)
	{
		AllEffects.Append(K2_ApplyGameplayEffectSpecToTarget(SpecHandle, ContainerSpec.TargetData));
	}
	
	return AllEffects;
}

bool UOFGameplayAbility::OFCheckCost_Implementation(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo& ActorInfo, const FGameplayTagContainer& OptionalRelevantTags) const
{
	return true;
}

﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFAbilitySystemComponent.h"

#include "AbilitySystemGlobals.h"
#include "GameplayCueManager.h"
#include "OFAbilityInputBindingComponent.h"
#include "OFAnimInstance.h"
#include "OFGameplayAbility.h"
#include "OFGameplayAbilitySystemCoreTags.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

UOFAbilitySystemComponent::UOFAbilitySystemComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UOFAbilitySystemComponent::BeginDestroy()
{
	if(AbilityActorInfo && AbilityActorInfo->OwnerActor.IsValid())
	{
		if (UGameInstance* GameInstance = AbilityActorInfo->OwnerActor->GetGameInstance())
		{
			GameInstance->GetOnPawnControllerChanged().RemoveAll(this);
		}
	}
	
	Super::BeginDestroy();
}

void UOFAbilitySystemComponent::InitAbilityActorInfo(AActor* InOwnerActor, AActor* InAvatarActor)
{
	Super::InitAbilityActorInfo(InOwnerActor, InAvatarActor);

	if(!AbilityActorInfo) { return; }
	
	if(!AbilityActorInfo->AnimInstance.IsValid())
	{
		AbilityActorInfo->AnimInstance = AbilityActorInfo->GetAnimInstance();
	}

	if(UOFAnimInstance* OFAnimInstance = Cast<UOFAnimInstance>(AbilityActorInfo->AnimInstance))
	{
		OFAnimInstance->InitializeWithAbilitySystem(this);
	}

	if(AbilityActorInfo->SkeletalMeshComponent.IsValid())
	{
		const USkeletalMeshComponent* SkeletalMeshComponent = AbilityActorInfo->SkeletalMeshComponent.Get();

		const TArray<UAnimInstance*>& AnimLayers = SkeletalMeshComponent->GetLinkedAnimInstances();
		for(UAnimInstance* AnimLayer : AnimLayers)
		{
			if(UOFAnimInstance* OFAnimInstance = Cast<UOFAnimInstance>(AnimLayer))
			{
				OFAnimInstance->InitializeWithAbilitySystem(this);
			}
		}
	}

	if(UGameInstance* GameInstance = InOwnerActor->GetGameInstance())
	{
		GameInstance->GetOnPawnControllerChanged().AddDynamic(this, &UOFAbilitySystemComponent::OnPawnControllerChanged);
	}

	GrantDefaultAbilitiesAndAttributes();
}

bool UOFAbilitySystemComponent::TryActivateAbilityWithEventData(TSubclassOf<UGameplayAbility> InAbilityToActivate, const FGameplayEventData& Payload)
{
	bool bSuccess = false;
	
	if(!InAbilityToActivate) { return bSuccess; }
		
	const UGameplayAbility* const InAbilityCDO = InAbilityToActivate.GetDefaultObject();
	
	for (const FGameplayAbilitySpec& Spec : ActivatableAbilities.Items)
	{
		if (Spec.Ability == InAbilityCDO)
		{
			bSuccess = TriggerAbilityFromGameplayEvent(FindAbilitySpecHandleForClass(InAbilityToActivate), AbilityActorInfo.Get(), TAG_ABILITY_TRIGGER_DEFAULT, &Payload, *this);
			break;
		}
	}
	
	return bSuccess;
}

UOFAbilitySystemComponent* UOFAbilitySystemComponent::GetOFAbilitySystemComponentFromActor(const AActor* Actor, bool LookForComponent)
{
	return Cast<UOFAbilitySystemComponent>(UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Actor, LookForComponent));
}

UAttributeSet* UOFAbilitySystemComponent::GetAttributeSetFromName(const FString AttributeSetName)
{
	for(UAttributeSet* AttributeSet : GetSpawnedAttributes_Mutable())
	{
		if(AttributeSet && AttributeSet->GetName() == AttributeSetName)
		{
			return AttributeSet;
		}
	}

	return nullptr;
}

FGameplayAbilitySpecHandle UOFAbilitySystemComponent::FindAbilitySpecHandleForClass(TSubclassOf<UGameplayAbility> AbilityClass, UObject* OptionalSourceObject)
{
	ABILITYLIST_SCOPE_LOCK();
	for(const FGameplayAbilitySpec& Spec : ActivatableAbilities.Items)
	{
		if(Spec.Ability->GetClass() == AbilityClass)
		{
			if (!OptionalSourceObject || (OptionalSourceObject && Spec.SourceObject == OptionalSourceObject))
			{
				return Spec.Handle;
			}
		}
	}

	return FGameplayAbilitySpecHandle();
}

void UOFAbilitySystemComponent::ExecuteGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	UAbilitySystemGlobals::Get().GetGameplayCueManager()->HandleGameplayCue(GetOwner(), GameplayCueTag, EGameplayCueEvent::Type::Executed, GameplayCueParameters);
}

void UOFAbilitySystemComponent::AddGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	UAbilitySystemGlobals::Get().GetGameplayCueManager()->HandleGameplayCue(GetOwner(), GameplayCueTag, EGameplayCueEvent::Type::OnActive, GameplayCueParameters);
	UAbilitySystemGlobals::Get().GetGameplayCueManager()->HandleGameplayCue(GetOwner(), GameplayCueTag, EGameplayCueEvent::Type::WhileActive, GameplayCueParameters);
}

void UOFAbilitySystemComponent::RemoveGameplayCueLocal(const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	UAbilitySystemGlobals::Get().GetGameplayCueManager()->HandleGameplayCue(GetOwner(), GameplayCueTag, EGameplayCueEvent::Type::Removed, GameplayCueParameters);
}

FGameplayAbilitySpecHandle UOFAbilitySystemComponent::GrantAbilityOfType(const TSubclassOf<UGameplayAbility> AbilityType, const bool bRemoveAfterActivation)
{
	FGameplayAbilitySpecHandle AbilityHandle;
	if (AbilityType)
	{
		FGameplayAbilitySpec AbilitySpec(AbilityType);
		AbilitySpec.RemoveAfterActivation = bRemoveAfterActivation;

		AbilityHandle = GiveAbility(AbilitySpec);
	}
	return AbilityHandle;
}

void UOFAbilitySystemComponent::GrantDefaultAbilitiesAndAttributes()
{
	for(UAttributeSet* AttributeSet : AddedAttributes)
	{
		GetSpawnedAttributes_Mutable().Remove(AttributeSet);
	}

	for(const FGameplayAbilitySpecHandle AbilitySpecHandle : DefaultAbilitySpecHandles)
	{
		SetRemoveAbilityOnEnd(AbilitySpecHandle);
	}

	AddedAttributes.Reset(DefaultAttributes.Num());
	DefaultAbilitySpecHandles.Reset(DefaultAbilities.Num() + DefaultAbilitiesWithInputBinding.Num());

	for(const TSubclassOf<UGameplayAbility>& Ability : DefaultAbilities)
	{
		if(*Ability)
		{
			DefaultAbilitySpecHandles.Add(GiveAbility(FGameplayAbilitySpec(Ability)));
		}
	}

	for(const FOFAbilityInputCombo& AbilityInputCombo : DefaultAbilitiesWithInputBinding)
	{
		if(AbilityInputCombo.Ability)
		{
			FGameplayAbilitySpec GameplayAbilitySpec = FGameplayAbilitySpec(AbilityInputCombo.Ability, 1, 0, this);
			GameplayAbilitySpec.RemoveAfterActivation = AbilityInputCombo.bRemoveAbilityAfterActivation;
			
			const FGameplayAbilitySpecHandle AbilitySpecHandle = GiveAbility(GameplayAbilitySpec);
			DefaultAbilitySpecHandles.Add(AbilitySpecHandle);
			
			if(AbilityInputCombo.ActivationInput)
			{
				const AActor* ACSOwnerActor = GetOwnerActor();
				if(UOFAbilityInputBindingComponent* OFAbilityInputBindingComponent = ACSOwnerActor ? ACSOwnerActor->FindComponentByClass<UOFAbilityInputBindingComponent>() : nullptr)
				{
					OFAbilityInputBindingComponent->SetInputBinding(AbilityInputCombo.ActivationInput, AbilitySpecHandle); 
				}
			}
		}
	}
	
	for(const FOFAttributeApplication& AttributeApplication : DefaultAttributes)
	{
		if(AttributeApplication.AttributeSetType)
		{
			UAttributeSet* NewAttributeSet = NewObject<UAttributeSet>(AbilityActorInfo.Get()->OwnerActor.Get(), AttributeApplication.AttributeSetType, AttributeApplication.AttributeName);
			AddedAttributes.Add(NewAttributeSet);
			AddAttributeSetSubobject(NewAttributeSet);
								
			if(AttributeApplication.Attributes)
			{
				NewAttributeSet->InitFromMetaDataTable(AttributeApplication.Attributes);
			}
		}
	}
	
	const TArray<FOFAttributeApplication>& CustomAttributeApplications = GetCustomDefaultAttributes();
	for(const FOFAttributeApplication& AttributeApplication : CustomAttributeApplications)
	{
		if(AttributeApplication.AttributeSetType)
		{
			UAttributeSet* NewAttributeSet = NewObject<UAttributeSet>(AbilityActorInfo.Get()->OwnerActor.Get(), AttributeApplication.AttributeSetType, AttributeApplication.AttributeName);
			AddedAttributes.Add(NewAttributeSet);
			AddAttributeSetSubobject(NewAttributeSet);
								
			if(AttributeApplication.Attributes)
			{
				NewAttributeSet->InitFromMetaDataTable(AttributeApplication.Attributes);
			}
		}
	}

	OnDefaultAbilitiesAndAttributesGranted.Broadcast();
}

void UOFAbilitySystemComponent::OnPawnControllerChanged(APawn* Pawn, AController* NewController)
{
	if (AbilityActorInfo && AbilityActorInfo->OwnerActor == Pawn && AbilityActorInfo->PlayerController != NewController)
	{
		AbilityActorInfo->InitFromActor(AbilityActorInfo->OwnerActor.Get(), AbilityActorInfo->AvatarActor.Get(), this);
	}
}

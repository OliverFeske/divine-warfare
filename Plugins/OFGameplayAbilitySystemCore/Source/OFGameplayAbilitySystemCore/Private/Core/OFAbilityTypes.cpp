﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFAbilityTypes.h"

void FOFGameplayEffectContainerSpec::AddTargets(const TArray<FGameplayAbilityTargetDataHandle>& InTargetData, const TArray<FHitResult>& InHitResults, const TArray<AActor*>& InTargetActors)
{
	for(const FGameplayAbilityTargetDataHandle& TargetDataHandle : InTargetData)
	{
		TargetData.Append(TargetDataHandle);
	}

	for(const FHitResult& HitResult : InHitResults)
	{
		FGameplayAbilityTargetData_SingleTargetHit* SingleTargetHit = new FGameplayAbilityTargetData_SingleTargetHit(HitResult);
		TargetData.Add(SingleTargetHit);
	}

	if(InTargetActors.Num() > 0)
	{
		FGameplayAbilityTargetData_ActorArray* ActorArray = new FGameplayAbilityTargetData_ActorArray();
		ActorArray->TargetActorArray.Append(InTargetActors);
		TargetData.Add(ActorArray);
	}
}

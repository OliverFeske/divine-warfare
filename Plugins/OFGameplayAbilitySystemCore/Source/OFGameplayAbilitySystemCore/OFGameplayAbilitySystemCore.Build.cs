// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class OFGameplayAbilitySystemCore : ModuleRules
{
	public OFGameplayAbilitySystemCore(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
				ModuleDirectory + "/Public/AbilityTasks",
				ModuleDirectory + "/Public/Animation",
				ModuleDirectory + "/Public/Core",
				ModuleDirectory + "/Public/GameplayCues",
				ModuleDirectory + "/Public/Input",
				ModuleDirectory + "/Public/UtilitiesAndLibraries",
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"InputCore",
				"GameplayAbilities",
				"GameplayTags",
				"GameplayTasks",
				"EnhancedInput",
				"ModularGameplay",
				"ModularGameplayActors", "ModularGameplay",
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}

// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class AncientGameSource : ModuleRules
{
	public AncientGameSource(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
				ModuleDirectory + "/Public/AbilitySystem",
				ModuleDirectory + "/Public/Animation",
				ModuleDirectory + "/Public/Camera",
				ModuleDirectory + "/Public/Character",
				ModuleDirectory + "/Public/Framework",
				ModuleDirectory + "/Public/GameFeatures",
				ModuleDirectory + "/Public/Input"
			});
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				// ... add other private include paths required here ...
			});
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"EnhancedInput",
				"ModularGameplay",
				"ModularGameplayActors",
				"GameplayAbilities",
				"GameplayTasks"
				// ... add other public dependencies that you statically link with here ...
			});
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"GameFeatures"
				// ... add private dependencies that you statically link with here ...	
			});
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			});
	}
}

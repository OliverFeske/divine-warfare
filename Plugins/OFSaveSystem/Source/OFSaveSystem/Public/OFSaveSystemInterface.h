﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "OFSaveSystemInterface.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UOFSaveSystemInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class OFSAVESYSTEM_API IOFSaveSystemInterface
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnPreSave();
	virtual void OnPreSave_Implementation() {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnPostSave();
	virtual void OnPostSave_Implementation() {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnPreLoad();
	virtual void OnPreLoad_Implementation() {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnPostLoad();
	virtual void OnPostLoad_Implementation() {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	FGuid GetSaveID();
	virtual FGuid GetSaveID_Implementation() { return FGuid(0, 0, 0, 0); }
};

﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "OFSaveSystemGameDataStructs.h"

#include "OFSaveGameStatics.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogOFSaveSystem, Log, All);

UENUM(BlueprintType)
enum class EOFSaveGameType : uint8
{
	QuickSave,
	ManualSave
};

USTRUCT(BlueprintType)
struct FOFSaveGameInfo
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadWrite)
	EOFSaveGameType SaveGameType = EOFSaveGameType::QuickSave;
	
	UPROPERTY(BlueprintReadWrite)
	FString SaveLocation = TEXT("None");
	
	UPROPERTY(BlueprintReadWrite)
	int64 CurrentCharacterLevel = -1;
	
	static FString Separator;
	
	FString CreateSaveGameName() const
	{
		return FString(*StaticEnum<EOFSaveGameType>()->GetValueAsString(SaveGameType) + Separator + SaveLocation + Separator + TEXT("Level ")
			+ FString::FromInt(CurrentCharacterLevel) + Separator + FDateTime::Now().ToString());
	}
};

/**
 * 
 */
UCLASS()
class OFSAVESYSTEM_API UOFSaveGameStatics : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "SaveSystem")
	static TArray<FString> GetSaveGamesForDirectory(FString Directory, FString Filter);
	
	UFUNCTION(BlueprintCallable, Category = "SaveSystem")
	static FString GenerateSaveFileName(const FOFSaveGameInfo& SaveGameInfo) { return SaveGameInfo.CreateSaveGameName(); }
	
	UFUNCTION(BlueprintCallable, Category = "SaveSystem", meta = (WorldContext = "WorldContextObject"))
	static AActor* FindActorBySaveID(UObject* WorldContextObject, const FGuid SaveID);
	
	template<typename TActor>
	UFUNCTION(BlueprintCallable, Category = "SaveSystem", meta = (WorldContext = "WorldContextObject"))
	static TActor* FindActorBySaveID(UObject* WorldContextObject, const FGuid SaveID) { return Cast<TActor>(FindActorBySaveID(WorldContextObject, SaveID)); }
	
	UFUNCTION(BlueprintCallable, Category = "SaveSystem", meta = (WorldContext = "WorldContextObject"))
	bool SerializeWorld(UObject* WorldContextObject, FOFWorldSaveData& OutWorldSaveData);
	
	static bool IsActorValidForSaving(AActor* Actor);
};

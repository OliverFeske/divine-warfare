﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/SaveGame.h"

#include "OFSaveSystemGameDataStructs.generated.h"

USTRUCT(BlueprintType)
struct FOFActorComponentSaveData
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ComponentName = NAME_None;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UClass* ComponentClass = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<uint8> ComponentData;

	bool operator==(const FOFActorComponentSaveData& Other) const { return ComponentName == Other.ComponentName; }
	friend uint32 GetTypeHash(const FOFActorComponentSaveData& Other) { return GetTypeHash(Other.ComponentName); }
};

USTRUCT(BlueprintType)
struct FOFSceneComponentSaveData : public FOFActorComponentSaveData
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTransform ComponentTransform = FTransform::Identity;
};

USTRUCT(BlueprintType)
struct FOFActorSaveData
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ActorName = NAME_None;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTransform ActorTransform = FTransform::Identity;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UClass* ActorClass = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSet<FName> ActorTags;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGuid SaveID = FGuid(0, 0, 0, 0);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<uint8> ActorData;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSet<FOFActorComponentSaveData> SavedComponents;

	bool operator==(const FOFActorSaveData& Other) const { return ActorName == Other.ActorName; }
	friend uint32 GetTypeHash(const FOFActorSaveData& Other) { return GetTypeHash(Other.ActorName); }
};

USTRUCT(BlueprintType)
struct FOFWorldDataLayerSaveData
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName DataLayerName = NAME_None;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSet<FOFActorSaveData> SavedActors;

	bool operator==(const FOFWorldDataLayerSaveData& Other) const { return DataLayerName == Other.DataLayerName; }
	friend uint32 GetTypeHash(const FOFWorldDataLayerSaveData& Other) { return GetTypeHash(Other.DataLayerName); }
};

/**
 *	todo: Figure out how to save the world with world partition enabled.
 *	The most appropriate approach would be to save a data layer as soon as it is exited. So there needs to be a place
 *	where all data layers saves are stored and then add them back in one big save.
 */
USTRUCT(BlueprintType)
struct FOFWorldSaveData
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSoftObjectPtr<UWorld> SavedWorld = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSet<FOFWorldDataLayerSaveData> WorldDataLayerSaveData;
	
	/**
	 *	GetEffectiveActiveDataLayerNames()
	 *	GetEffectiveLoadedDataLayerNames()
	 */
	
	bool operator==(const FOFWorldSaveData& Other) const { return SavedWorld == Other.SavedWorld; }
	friend uint32 GetTypeHash(const FOFWorldSaveData& Other) { return GetTypeHash(Other.SavedWorld); }
};

UCLASS()
class OFSAVESYSTEM_API UOFSaveGameExample : public USaveGame
{
	GENERATED_BODY()
	
public:
	UPROPERTY()
	TSet<FOFWorldSaveData> SavedWorlds;
};
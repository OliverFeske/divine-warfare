﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "OFSaveSystemMapErrorChecksSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class OFSAVESYSTEM_API UOFSaveSystemMapErrorChecksSubsystem : public UWorldSubsystem
{
	GENERATED_BODY()
	
public:
	//~ Begin USubsystem Interface
	virtual bool DoesSupportWorldType(const EWorldType::Type WorldType) const override;
	virtual void PostInitialize() override;
	//~ End USubsystem Interface
};

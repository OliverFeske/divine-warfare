﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFSaveSystemMapErrorChecksSubsystem.h"

#include "EngineUtils.h"
#include "OFSaveGameStatics.h"

bool UOFSaveSystemMapErrorChecksSubsystem::DoesSupportWorldType(const EWorldType::Type WorldType) const
{
	return EWorldType::Editor == WorldType || EWorldType::PIE == WorldType;
}

void UOFSaveSystemMapErrorChecksSubsystem::PostInitialize()
{
#if WITH_EDITOR
	for(TActorIterator<AActor> Itr(GetWorld()); Itr; ++Itr)
	{
		UOFSaveGameStatics::IsActorValidForSaving(*Itr);
	}
#endif
}

﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFSaveGameStatics.h"

#include "EngineUtils.h"
#include "Misc/UObjectToken.h"

#include "OFSaveSystemInterface.h"

DEFINE_LOG_CATEGORY(LogOFSaveSystem);

FString FOFSaveGameInfo::Separator = TEXT("_");

TArray<FString> UOFSaveGameStatics::GetSaveGamesForDirectory(FString Directory, FString Filter)
{
	FString SaveGamesFolder = FPaths::Combine(*FPaths::ProjectSavedDir(), TEXT("SaveGames"), Directory);
	
	IFileManager &FileManager = IFileManager::Get();
	
	TArray<FString> AllSaveGamePaths;
	FileManager.FindFiles(AllSaveGamePaths, *SaveGamesFolder, TEXT(".sav"));
	
	AllSaveGamePaths.Sort([&FileManager, SaveGamesFolder](const FString& LHS, const FString& RHS)
	{
		return FileManager.GetTimeStamp(*FPaths::Combine(SaveGamesFolder, *LHS)) > FileManager.GetTimeStamp(*FPaths::Combine(SaveGamesFolder, *RHS));
	});
	
	TArray<FString> ValidSaveGames;
	for (const FString &SaveGamePath : AllSaveGamePaths)
	{
		const FString BaseFileName = FPaths::GetBaseFilename(SaveGamePath);
		if (Filter.IsEmpty() || BaseFileName.Contains(Filter))
		{
			ValidSaveGames.Add(BaseFileName);
		}
	}
	
	return ValidSaveGames;
}

AActor* UOFSaveGameStatics::FindActorBySaveID(UObject* WorldContextObject, const FGuid SaveID)
{
	for(TActorIterator<AActor> Itr(GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull)); Itr; ++Itr)
	{
		AActor* Actor = *Itr;
		
		if(!Actor || !Actor->Implements<UOFSaveSystemInterface>()) { continue; }
		
		if(IOFSaveSystemInterface* OFSaveSystemInterface = Cast<IOFSaveSystemInterface>(Actor))
		{
			const FGuid ActorSaveID = OFSaveSystemInterface->GetSaveID();
			if(ActorSaveID.IsValid() && ActorSaveID == SaveID)
			{
				return Actor;
			}
		}
	}
	
	return nullptr;
}

bool UOFSaveGameStatics::SerializeWorld(UObject* WorldContextObject, FOFWorldSaveData& OutWorldSaveData)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	if(!World || !World->IsGameWorld()) { return false; }

	//todo: Get all data layers and save the loaded ones. Get the save data from unloaded ones from a cache.
	return false;
}

bool UOFSaveGameStatics::IsActorValidForSaving(AActor* Actor)
{
	if(!Actor || !Actor->Implements<UOFSaveSystemInterface>() || !Actor->IsChildActor()) { return false; }
	
#if WITH_EDITOR
	FMessageLog SaveSystemCheck = FMessageLog(TEXT("SaveSystemCheck"));
#endif
	
	if(IOFSaveSystemInterface* SaveSystemInterface = Cast<IOFSaveSystemInterface>(Actor))
	{
		const FGuid SaveID = SaveSystemInterface->GetSaveID();
		if(!SaveID.IsValid())
		{
			UE_LOG(LogOFSaveSystem, Warning, TEXT("%s has invalid SaveID or does not override GetSaveID()."), *Actor->GetName());
			
#if WITH_EDITOR
			SaveSystemCheck.Error()
				->AddToken(FUObjectToken::Create(Actor))
				->AddToken(FTextToken::Create(FText::FromString(TEXT(" has invalid SaveID or does not override GetSaveID()."))));
#endif
			
			return false;
		}

		if(SaveID != SaveSystemInterface->GetSaveID())
		{
			UE_LOG(LogOFSaveSystem, Warning, TEXT("%s does have a deterministic implementation of GetSaveId(). This actor will be skipped in save games."),
				*Actor->GetClass()->GetName());
#if WITH_EDITOR
			SaveSystemCheck.Error()
				->AddToken(FUObjectToken::Create(Actor))
				->AddToken(FTextToken::Create(FText::FromString(TEXT(" does have a deterministic implementation of GetSaveId(). This actor will be skipped in save games."))));
#endif
			
			return false;
		}
	}
	
	return false;
}

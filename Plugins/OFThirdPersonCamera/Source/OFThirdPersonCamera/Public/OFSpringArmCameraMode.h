﻿// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "OFSpringArm.h"
#include "OFInterpolators.h"
#include "OFCameraMode.h"
#include "OFSpringArmCameraMode.generated.h"

UCLASS(Blueprintable)
class OFTHIRDPERSONCAMERA_API UOFSpringArmCameraMode : public UOFCameraMode
{
	GENERATED_BODY()

public:
	UOFSpringArmCameraMode();

	//~ Begin ULyraCameraMode interface
	virtual void OnActivation(AActor* TargetActor) override;
	//~ End ULyraCameraMode interface

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	FOFSpringArm SpringArm;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	FVector Offset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	FVector PivotOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	FOFCritDampSpringInterpolatorVector LocationSpringInterpolator = FOFCritDampSpringInterpolatorVector(15.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	FOFCritDampSpringInterpolatorRotator RotationSpringInterpolator = FOFCritDampSpringInterpolatorRotator(15.f);

protected:

	//~ Begin UAncientGameCameraMode interface
	virtual FOFCameraModeView UpdateView_Implementation(float DeltaTime, AActor* TargetActor) override;
	//~ End ULyraCameraMode interface
};

﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OFCameraModeView.generated.h"

/**
 *	This is based on the FAncientGameCameraModeView from the AncientGame.
 *
 *	View data produced by the camera mode that is used to blend camera modes.
 */
USTRUCT(BlueprintType)
struct FOFCameraModeView
{
public:
	GENERATED_BODY()

	FOFCameraModeView() {}

	void Blend(const FOFCameraModeView& Other, const float OtherWeight);

public:
	// The desired world-space pivot location of the camera view
	UPROPERTY(BlueprintReadWrite)
	FVector PivotLocation = FVector::ZeroVector;

	// The desired world-space location of the camera view
	UPROPERTY(BlueprintReadWrite)
	FVector Location = FVector::ZeroVector;

	// The desired world-space rotation of the camera view
	UPROPERTY(BlueprintReadWrite)
	FRotator Rotation = FRotator::ZeroRotator;

	// The desired world-space rotation to apply to the player controller (if one exists, associated with the target actor)
	UPROPERTY(BlueprintReadWrite)
	FRotator ControlRotation = FRotator::ZeroRotator;

	// The desired field-of-view for the camera view
	UPROPERTY(BlueprintReadWrite)
	float FieldOfView = 80.f;
};
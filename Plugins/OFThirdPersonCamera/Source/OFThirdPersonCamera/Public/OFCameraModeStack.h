﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OFCameraModeStack.generated.h"

class UOFCameraMode;
class AActor;

struct FOFCameraModeView;

/**
 *	This is based on the FAncientGameCameraModeStack from the AncientGame.
 *
 *	Stack used for blending camera modes.
 */
USTRUCT()
struct FOFCameraModeStack
{
	GENERATED_BODY()
	
	void PushCameraMode(UOFCameraMode* CameraModeInstance, AActor* TargetActor);
	
	bool EvaluateStack(float DeltaTime, AActor* TargetActor, FOFCameraModeView& OutCameraModeView);
	
protected:
	bool UpdateStack(float DeltaTime, AActor* TargetActor);
	void BlendStack(FOFCameraModeView& OutCameraModeView) const;
	
	UPROPERTY()
	TArray<UOFCameraMode*> CameraModeStack;
};

﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OFCameraModeView.h"
#include "UObject/Object.h"
#include "OFCameraMode.generated.h"

/**
 *	This is based on the EAncientGameCameraModeBlendFunction from the AncientGame.
 */
UENUM(BlueprintType)
enum class EOFCameraModeBlendFunction : uint8
{
	// Does a simple linear interpolation.
	Linear,

	// Immediately accelerates, but smoothly decelerates into the target.  Ease amount controlled by the exponent.
	EaseIn,

	// Smoothly accelerates, but does not decelerate into the target.  Ease amount controlled by the exponent.
	EaseOut,

	// Smoothly accelerates and decelerates.  Ease amount controlled by the exponent.
	EaseInOut,

	Cubic,

	COUNT	UMETA(Hidden)
};

/**
 *	This is based on the the UAncientGameCameraMode from the AncientGame.
 */
UCLASS(Blueprintable)
class OFTHIRDPERSONCAMERA_API UOFCameraMode : public UObject
{
	GENERATED_BODY()
	
protected:
	// The horizontal field of view (in degrees).
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "View", Meta = (UIMin = "5.0", UIMax = "170", ClampMin = "5.0", ClampMax = "170.0"))
	float FieldOfView = 80.f;

	// Minimum view pitch (in degrees).
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "View", Meta = (UIMin = "-89.9", UIMax = "89.9", ClampMin = "-89.9", ClampMax = "89.9"))
	float ViewPitchMin = -89.f;

	// Maximum view pitch (in degrees).
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "View", Meta = (UIMin = "-89.9", UIMax = "89.9", ClampMin = "-89.9", ClampMax = "89.9"))
	float ViewPitchMax = 89.f;

	// How long it takes to blend in this mode.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Blending", meta = (ExposeOnSpawn))
	float BlendTime = 0.5f;

	// Function used for blending.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Blending", meta = (ExposeOnSpawn))
	EOFCameraModeBlendFunction BlendFunction = EOFCameraModeBlendFunction::EaseOut;

	// Exponent used by blend functions to control the shape of the curve.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Blending", meta = (ExposeOnSpawn))
	float BlendExponent = 4.f;

public:
	// Called when this camera mode is activated on the camera mode stack.
	virtual void OnActivation(AActor* TargetActor) { ReceiveActivation(TargetActor); }
	// Called when this camera mode is deactivated on the camera mode stack.
	virtual void OnDeactivation() { ReceiveDeactivation(); }

	void UpdateCameraMode(float DeltaTime, AActor* TargetActor);

	const FOFCameraModeView& GetCameraModeView() const { return View; }

	UFUNCTION(BlueprintPure)
	float GetBlendTime() const   { return BlendTime; }
	UFUNCTION(BlueprintPure)
	float GetBlendWeight() const { return BlendWeight; }

	void SetBlendWeight(float Weight);

protected:
	UFUNCTION(BlueprintImplementableEvent, DisplayName = "OnActivation")
	void ReceiveActivation(AActor* TargetActor);

	UFUNCTION(BlueprintImplementableEvent, DisplayName = "OnDeactivation")
	void ReceiveDeactivation();

	UFUNCTION(BlueprintNativeEvent, DisplayName = "UpdateView")
	FOFCameraModeView UpdateView(float DeltaTime, AActor* TargetActor);

	UFUNCTION(BlueprintNativeEvent, BlueprintPure, meta = (BlueprintProtected))
	FVector GetPivotLocation(AActor* TargetActor) const;

	UFUNCTION(BlueprintNativeEvent, BlueprintPure, meta = (BlueprintProtected))
	FRotator GetPivotRotation(AActor* TargetActor) const;
	
	void UpdateBlending(float DeltaTime);

protected:
	// View output produced by the camera mode.
	FOFCameraModeView View;

	// Linear blend alpha used to determine the blend weight.
	float BlendAlpha = 1.f;

	// Blend weight calculated using the blend alpha and function.
	float BlendWeight = 1.f;

private:
	void ActivateInternal(AActor* TargetActor);
	void DeactivateInternal();
	bool bIsActivated = false;

	friend struct FOFCameraModeStack;
};

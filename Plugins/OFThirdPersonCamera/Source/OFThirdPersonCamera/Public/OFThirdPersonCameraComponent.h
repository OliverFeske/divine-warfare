﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OFCameraMode.h"
#include "OFCameraModeStack.h"
#include "Camera/CameraComponent.h"
#include "OFThirdPersonCameraComponent.generated.h"

class UOFCameraMode;

USTRUCT(BlueprintType)
struct FOFCameraModeHandle
{
	GENERATED_BODY()

public:
	bool IsValid() const
	{
		return Owner.IsValid() && HandleID != 0;
	}
	
	void Reset()
	{
		Owner.Reset();
		HandleID = 0;
	}

private:
	friend class UOFThirdPersonCameraComponent;

	UPROPERTY()
	TWeakObjectPtr<UOFThirdPersonCameraComponent> Owner;

	UPROPERTY()
	int32 HandleID = 0;
};

USTRUCT()
struct FOFCameraModeStackEntry
{
	GENERATED_BODY()

	int32 HandleID = 0;
	int32 Priority = 0;

	UPROPERTY()
	UOFCameraMode* CameraMode = nullptr;
};

/**
 *	This is a copy of the UAncientGameCameraComponent from the AncientGame.
 */
UCLASS(Blueprintable, BlueprintType, meta=(BlueprintSpawnableComponent))
class OFTHIRDPERSONCAMERA_API UOFThirdPersonCameraComponent : public UCameraComponent
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UOFCameraMode> DefaultCameraMode;
	
public:
	UOFThirdPersonCameraComponent();
	
	//~ Begin UActorComponent interface
	virtual void InitializeComponent() override;
	//~ End UActorComponent interface
	
	//~ Begin UCameraComponent interface
	virtual void GetCameraView(float DeltaTime, FMinimalViewInfo& DesiredView) override;
	//~ End UCameraComponent interface
	
	UFUNCTION(BlueprintCallable)
	FOFCameraModeHandle PushCameraMode(const TSubclassOf<UOFCameraMode> CameraModeClass, int32 Priority = 0);
	
	UFUNCTION(BlueprintCallable)
	FOFCameraModeHandle PushCameraModeUsingInstance(UOFCameraMode* CameraModeInstance, int32 Priority = 0);
	
	UFUNCTION(BlueprintCallable)
	bool PullCameraMode(UPARAM(ref) FOFCameraModeHandle& ModeHandle);
	
	UFUNCTION(BlueprintCallable)
	bool PullCameraModeInstance(UOFCameraMode* CameraMode);
	
	UFUNCTION(BlueprintCallable)
	UOFCameraMode* GetActiveCameraMode() const;
	
	UFUNCTION(BlueprintPure)
	static bool IsValid(const FOFCameraModeHandle& ModeHandle) { return ModeHandle.IsValid(); }
	
	UFUNCTION(BlueprintCallable)
	static FOFCameraModeHandle PushCameraModeForActor(const AActor* Actor, TSubclassOf<UOFCameraMode> CameraModeType, int32 Priority = 0);
	
	UFUNCTION(BlueprintCallable)
	static FOFCameraModeHandle PushCameraModeForActorUsingInstance(const AActor* Actor, UOFCameraMode* CameraModeInstance, int32 Priority = 0);
	
	UFUNCTION(BlueprintCallable)
	static bool PullCameraModeByHandle(UPARAM(ref) FOFCameraModeHandle& ModeHandle);
	
	UFUNCTION(BlueprintCallable)
	static bool PullCameraModeInstanceFromActor(const AActor* Actor, UOFCameraMode* CameraMode);
	
	UFUNCTION(BlueprintCallable)
	static UOFCameraMode* GetActiveCameraModeForActor(const AActor* Actor);
	
protected:
	void UpdateBlendingStack();
	UOFCameraMode* GetPooledCameraModeInstance(TSubclassOf<UOFCameraMode> CameraModeClass);
	
	bool PullCameraModeAtIndex(int32 Index);
	
	// Stack of active camera modes, sorted in priority order.
	UPROPERTY()
	TArray<FOFCameraModeStackEntry> CameraModePriorityStack;
	
	// Pool of unique camera modes for reuse (to cut down on re-allocating the same mode over and over).
	UPROPERTY()
	TArray<UOFCameraMode*> CameraModeInstancePool;
	
	// Stack used to blend the camera modes.
	UPROPERTY()
	FOFCameraModeStack BlendingStack;
};

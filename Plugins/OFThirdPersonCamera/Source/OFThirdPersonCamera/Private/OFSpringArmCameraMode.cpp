﻿// Copyright Epic Games, Inc. All Rights Reserved.
#include "OFSpringArmCameraMode.h"

UOFSpringArmCameraMode::UOFSpringArmCameraMode()
{
	SpringArm.bDoCollisionTest = true;
}

void UOFSpringArmCameraMode::OnActivation(AActor* TargetActor)
{
	Super::OnActivation(TargetActor);
	SpringArm.Initialize();
	LocationSpringInterpolator.Reset();
	RotationSpringInterpolator.Reset();
}

FOFCameraModeView UOFSpringArmCameraMode::UpdateView_Implementation(float DeltaTime, AActor* TargetActor)
{
	const UObject* WorldContext = this;
	TArray<const AActor*> AllIgnoreActors;

	if (TargetActor)
	{
		WorldContext = TargetActor;

		// Skip this actor and any attached actors when testing for collision
		AllIgnoreActors.Add(TargetActor);
		TargetActor->ForEachAttachedActors([&AllIgnoreActors](const AActor* Actor) { AllIgnoreActors.AddUnique(Actor); return true; });
	}

	FOFCameraModeView NewView = Super::UpdateView_Implementation(DeltaTime, TargetActor);

	// update springs
	const FVector GoalCameraLoc = NewView.PivotLocation + PivotOffset;
	const FVector SmoothedLocation = LocationSpringInterpolator.Eval(GoalCameraLoc, DeltaTime);
	const FRotator GoalCameraRot = NewView.Rotation;
	const FRotator SmoothedRotation = RotationSpringInterpolator.Eval(GoalCameraRot, DeltaTime);

	// Update the spring arm using the Pivot and offset provided
	FTransform CameraTransform(SmoothedRotation, SmoothedLocation);
	SpringArm.Tick(WorldContext->GetWorld(), AllIgnoreActors, CameraTransform, Offset);

	CameraTransform = SpringArm.GetCameraTransform();
	NewView.Location = CameraTransform.GetLocation();
	NewView.Rotation = CameraTransform.Rotator();

	return NewView;
}

﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFCameraMode.h"

#include "Components/CapsuleComponent.h"
#include "GameFramework/Character.h"

void UOFCameraMode::UpdateCameraMode(float DeltaTime, AActor* TargetActor)
{
	View = UpdateView(DeltaTime, TargetActor);
	UpdateBlending(DeltaTime);
}

void UOFCameraMode::SetBlendWeight(float Weight)
{
	BlendWeight = FMath::Clamp(Weight, 0.0f, 1.0f);

	// Since we're setting the blend weight directly, we need to calculate the blend alpha to account for the blend function.
	const float InvExponent = (BlendExponent > 0.0f) ? (1.0f / BlendExponent) : 1.0f;

	switch (BlendFunction)
	{
	case EOFCameraModeBlendFunction::Linear:
		BlendAlpha = BlendWeight;
		break;

	case EOFCameraModeBlendFunction::EaseIn:
		BlendAlpha = FMath::InterpEaseIn(0.0f, 1.0f, BlendWeight, InvExponent);
		break;

	case EOFCameraModeBlendFunction::EaseOut:
		BlendAlpha = FMath::InterpEaseOut(0.0f, 1.0f, BlendWeight, InvExponent);
		break;

	case EOFCameraModeBlendFunction::EaseInOut:
		BlendAlpha = FMath::InterpEaseInOut(0.0f, 1.0f, BlendWeight, InvExponent);
		break;

	case EOFCameraModeBlendFunction::Cubic:
		BlendAlpha = FMath::CubicInterp(0.f, 0.f, 1.f, 0.f, BlendWeight);
		break;

	default:
		checkf(false, TEXT("SetBlendWeight: Invalid BlendFunction [%d]\n"), (uint8)BlendFunction);
		break;
	}
}

FOFCameraModeView UOFCameraMode::UpdateView_Implementation(float DeltaTime, AActor* TargetActor)
{
	FOFCameraModeView NewView;
	NewView.Location = NewView.PivotLocation = GetPivotLocation(TargetActor);
	NewView.Rotation = GetPivotRotation(TargetActor);
	NewView.Rotation.Pitch = FMath::ClampAngle(NewView.Rotation.Pitch, ViewPitchMin, ViewPitchMax);
	NewView.FieldOfView = FieldOfView;

	if (const APawn* TargetPawn = Cast<APawn>(TargetActor))
	{
		NewView.ControlRotation = TargetPawn->GetControlRotation();
	}
	else
	{
		NewView.ControlRotation = NewView.Rotation;
	}

	return NewView;
}

FVector UOFCameraMode::GetPivotLocation_Implementation(AActor* TargetActor) const
{
	FVector ViewLocation(ForceInit);

	if (const APawn* TargetPawn = Cast<APawn>(TargetActor))
	{
		// Height adjustments for characters to account for crouching.
		if (const ACharacter* TargetCharacter = Cast<ACharacter>(TargetPawn))
		{
			const ACharacter* TargetCharacterCDO = TargetCharacter->GetClass()->GetDefaultObject<ACharacter>();
			check(TargetCharacterCDO);

			const UCapsuleComponent* CapsuleComp = TargetCharacter->GetCapsuleComponent();
			check(CapsuleComp);

			const UCapsuleComponent* CapsuleCompCDO = TargetCharacterCDO->GetCapsuleComponent();
			check(CapsuleCompCDO);

			const float DefaultHalfHeight = CapsuleCompCDO->GetUnscaledCapsuleHalfHeight();
			const float ActualHalfHeight = CapsuleComp->GetUnscaledCapsuleHalfHeight();
			const float HeightAdjustment = (DefaultHalfHeight - ActualHalfHeight) + TargetCharacterCDO->BaseEyeHeight;

			ViewLocation = TargetCharacter->GetActorLocation() + (FVector::UpVector * HeightAdjustment);
		}
		else
		{
			ViewLocation = TargetPawn->GetPawnViewLocation();
		}
	}
	else if (TargetActor)
	{
		ViewLocation = TargetActor->GetActorLocation();
	}

	return ViewLocation;
}

FRotator UOFCameraMode::GetPivotRotation_Implementation(AActor* TargetActor) const
{
	FRotator ViewRotation(ForceInit);

	if (const APawn* TargetPawn = Cast<APawn>(TargetActor))
	{
		ViewRotation = TargetPawn->GetViewRotation();
	}
	else if (TargetActor)
	{
		ViewRotation = TargetActor->GetActorRotation();
	}

	ViewRotation.Pitch = FMath::ClampAngle(ViewRotation.Pitch, ViewPitchMin, ViewPitchMax);
	return ViewRotation;
}

void UOFCameraMode::UpdateBlending(float DeltaTime)
{
	if (BlendTime > 0.0f)
	{
		BlendAlpha += (DeltaTime / BlendTime);
		BlendAlpha = FMath::Min(BlendAlpha, 1.0f);
	}
	else
	{
		BlendAlpha = 1.0f;
	}

	const float Exponent = (BlendExponent > 0.0f) ? BlendExponent : 1.0f;

	switch (BlendFunction)
	{
	case EOFCameraModeBlendFunction::Linear:
		BlendWeight = BlendAlpha;
		break;

	case EOFCameraModeBlendFunction::EaseIn:
		BlendWeight = FMath::InterpEaseIn(0.0f, 1.0f, BlendAlpha, Exponent);
		break;

	case EOFCameraModeBlendFunction::EaseOut:
		BlendWeight = FMath::InterpEaseOut(0.0f, 1.0f, BlendAlpha, Exponent);
		break;

	case EOFCameraModeBlendFunction::EaseInOut:
		BlendWeight = FMath::InterpEaseInOut(0.0f, 1.0f, BlendAlpha, Exponent);
		break;

	case EOFCameraModeBlendFunction::Cubic:
		BlendWeight = FMath::CubicInterp(0.f, 0.f, 1.f, 0.f, BlendAlpha);
		break;

	default:
		checkf(false, TEXT("UpdateBlending: Invalid BlendFunction [%d]\n"), (uint8)BlendFunction);
		break;
	}
}


void UOFCameraMode::ActivateInternal(AActor* TargetActor)
{
	OnActivation(TargetActor);
	bIsActivated = true;
}

void UOFCameraMode::DeactivateInternal()
{
	OnDeactivation();
	bIsActivated = false;
}
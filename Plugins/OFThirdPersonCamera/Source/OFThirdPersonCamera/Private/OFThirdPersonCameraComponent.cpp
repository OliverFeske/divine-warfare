﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFThirdPersonCameraComponent.h"

#include "OFCameraModeView.h"
#include "OFCameraMode.h"

namespace OFCameraModeHandle_Impl
{
	static int32 LastHandleId = 0;
	static int32 GetNextQueuedHandleIdForUse() { return ++LastHandleId; }
}

UOFThirdPersonCameraComponent::UOFThirdPersonCameraComponent()
	: DefaultCameraMode(UOFCameraMode::StaticClass())
{
	bWantsInitializeComponent = true;
}

void UOFThirdPersonCameraComponent::InitializeComponent()
{
	Super::InitializeComponent();
	PushCameraMode(DefaultCameraMode);
}

void UOFThirdPersonCameraComponent::GetCameraView(float DeltaTime, FMinimalViewInfo& DesiredView)
{
	AActor* TargetActor = GetOwner();

	FOFCameraModeView CameraModeView;
	if (!BlendingStack.EvaluateStack(DeltaTime, TargetActor, CameraModeView))
	{
		Super::GetCameraView(DeltaTime, DesiredView);
		return;
	}

	// Keep camera component in sync with the latest view.
	FieldOfView = CameraModeView.FieldOfView;

	// Keep player controller in sync with the latest view.
	if (const APawn* TargetPawn = Cast<APawn>(TargetActor))
	{
		if (APlayerController* PC = TargetPawn->GetController<APlayerController>())
		{
			PC->SetControlRotation(CameraModeView.ControlRotation);
		}
	}

	// Keep camera component in sync with the latest view.
	SetWorldLocationAndRotation(CameraModeView.Location, CameraModeView.Rotation);
	FieldOfView = CameraModeView.FieldOfView;

	// Fill in desired view.
	DesiredView.Location = CameraModeView.Location;
	DesiredView.Rotation = CameraModeView.Rotation;
	DesiredView.FOV = CameraModeView.FieldOfView;
	DesiredView.OrthoWidth = OrthoWidth;
	DesiredView.OrthoNearClipPlane = OrthoNearClipPlane;
	DesiredView.OrthoFarClipPlane = OrthoFarClipPlane;
	DesiredView.AspectRatio = AspectRatio;
	DesiredView.bConstrainAspectRatio = bConstrainAspectRatio;
	DesiredView.bUseFieldOfViewForLOD = bUseFieldOfViewForLOD;
	DesiredView.ProjectionMode = ProjectionMode;

	// See if the CameraActor wants to override the PostProcess settings used.
	DesiredView.PostProcessBlendWeight = PostProcessBlendWeight;
	if (PostProcessBlendWeight > 0.0f)
	{
		DesiredView.PostProcessSettings = PostProcessSettings;
	}
}

FOFCameraModeHandle UOFThirdPersonCameraComponent::PushCameraMode(const TSubclassOf<UOFCameraMode> CameraModeClass, int32 Priority)
{
	return PushCameraModeUsingInstance(GetPooledCameraModeInstance(CameraModeClass), Priority);
}

FOFCameraModeHandle UOFThirdPersonCameraComponent::PushCameraModeUsingInstance(UOFCameraMode* CameraModeInstance, int32 Priority)
{
	FOFCameraModeHandle ModeHandle;
	ModeHandle.Owner = this;
	ModeHandle.HandleID = OFCameraModeHandle_Impl::GetNextQueuedHandleIdForUse();

	int32 StackIndex = 0;
	for (; StackIndex < CameraModePriorityStack.Num(); ++StackIndex)
	{
		if (CameraModePriorityStack[StackIndex].Priority > Priority)
		{
			break;
		}
	}

	CameraModePriorityStack.Insert({ ModeHandle.HandleID, Priority, CameraModeInstance }, StackIndex);
	UpdateBlendingStack();

	return ModeHandle;
}

bool UOFThirdPersonCameraComponent::PullCameraMode(FOFCameraModeHandle& ModeHandle)
{
	bool bSuccess = false;
	if (ModeHandle.IsValid() && ModeHandle.Owner == this)
	{
		const int32 HandleID = ModeHandle.HandleID;
		const int32 FoundIndex = CameraModePriorityStack.IndexOfByPredicate([HandleID](const FOFCameraModeStackEntry& CameraModeEntry)
			{
				return (CameraModeEntry.HandleID == HandleID);
			});
		bSuccess = PullCameraModeAtIndex(FoundIndex);
		ModeHandle.Reset();
	}
	return bSuccess;
}

bool UOFThirdPersonCameraComponent::PullCameraModeInstance(UOFCameraMode* CameraMode)
{
	const int32 FoundIndex = CameraModePriorityStack.IndexOfByPredicate([CameraMode](const FOFCameraModeStackEntry& CameraModeEntry)
		{
			return (CameraModeEntry.CameraMode == CameraMode);
		});
	return PullCameraModeAtIndex(FoundIndex);
}

UOFCameraMode* UOFThirdPersonCameraComponent::GetActiveCameraMode() const
{
	UOFCameraMode* ActiveCamera = nullptr;
	if (ensureAlways(CameraModePriorityStack.Num() > 0))
	{
		ActiveCamera = CameraModePriorityStack.Top().CameraMode;
	}
	return ActiveCamera;
}

FOFCameraModeHandle UOFThirdPersonCameraComponent::PushCameraModeForActor(const AActor* Actor, TSubclassOf<UOFCameraMode> CameraModeType, int32 Priority)
{
	FOFCameraModeHandle ModeHandle;

	if (Actor)
	{
		if (UOFThirdPersonCameraComponent* CamComponent = Actor->FindComponentByClass<UOFThirdPersonCameraComponent>())
		{
			ModeHandle = CamComponent->PushCameraMode(CameraModeType, Priority);
		}
	}
	return ModeHandle;
}

FOFCameraModeHandle UOFThirdPersonCameraComponent::PushCameraModeForActorUsingInstance(const AActor* Actor, UOFCameraMode* CameraModeInstance, int32 Priority)
{
	FOFCameraModeHandle ModeHandle;

	if (Actor)
	{
		if (UOFThirdPersonCameraComponent* CamComponent = Actor->FindComponentByClass<UOFThirdPersonCameraComponent>())
		{
			ModeHandle = CamComponent->PushCameraModeUsingInstance(CameraModeInstance, Priority);
		}
	}
	return ModeHandle;
}

bool UOFThirdPersonCameraComponent::PullCameraModeByHandle(FOFCameraModeHandle& ModeHandle)
{
	if (ModeHandle.IsValid())
	{
		return ModeHandle.Owner->PullCameraMode(ModeHandle);
	}
	return false;
}

bool UOFThirdPersonCameraComponent::PullCameraModeInstanceFromActor(const AActor* Actor, UOFCameraMode* CameraMode)
{
	if (UOFThirdPersonCameraComponent* CamComponent = Actor->FindComponentByClass<UOFThirdPersonCameraComponent>())
	{
		return CamComponent->PullCameraModeInstance(CameraMode);
	}
	return false;
}

UOFCameraMode* UOFThirdPersonCameraComponent::GetActiveCameraModeForActor(const AActor* Actor)
{
	UOFCameraMode* ActiveCamera = nullptr;
	if (Actor)
	{
		if (const UOFThirdPersonCameraComponent* CamComponent = Actor->FindComponentByClass<UOFThirdPersonCameraComponent>())
		{
			ActiveCamera = CamComponent->GetActiveCameraMode();
		}
	}
	return ActiveCamera;
}

void UOFThirdPersonCameraComponent::UpdateBlendingStack()
{
	if (!ensureAlways(CameraModePriorityStack.Num() > 0))
	{
		PushCameraMode(DefaultCameraMode);
	}

	AActor* TargetActor = GetOwner();
	BlendingStack.PushCameraMode(CameraModePriorityStack.Top().CameraMode, TargetActor);
}

UOFCameraMode* UOFThirdPersonCameraComponent::GetPooledCameraModeInstance(TSubclassOf<UOFCameraMode> CameraModeClass)
{
	check(CameraModeClass);

	// First see if we already created one.
	for (UOFCameraMode* CameraMode : CameraModeInstancePool)
	{
		if ((CameraMode != nullptr) && (CameraMode->GetClass() == CameraModeClass))
		{
			return CameraMode;
		}
	}

	// Not found, so we need to create it.
	UOFCameraMode* NewCameraMode = NewObject<UOFCameraMode>(this, CameraModeClass, NAME_None, RF_NoFlags);
	check(NewCameraMode);

	CameraModeInstancePool.Add(NewCameraMode);

	return NewCameraMode;
}

bool UOFThirdPersonCameraComponent::PullCameraModeAtIndex(int32 Index)
{
	if (CameraModePriorityStack.IsValidIndex(Index))
	{
		CameraModePriorityStack.RemoveAt(Index);
		UpdateBlendingStack();

		return true;
	}
	return false;
}
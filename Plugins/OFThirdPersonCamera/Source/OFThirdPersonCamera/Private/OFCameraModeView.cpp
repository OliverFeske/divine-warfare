﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFCameraModeView.h"

void FOFCameraModeView::Blend(const FOFCameraModeView& Other, const float OtherWeight)
{
	if (OtherWeight <= 0.0f) { return; }
	
	else if (OtherWeight >= 1.0f)
	{
		*this = Other;
		return;
	}

	// Rotate the direction from the camera location to the pivot.  
	const FRotator OffsetRotation = (Location - PivotLocation).GetSafeNormal().Rotation();
	const FRotator OtherOffsetRotation = (Other.Location - Other.PivotLocation).GetSafeNormal().Rotation();
	const FRotator DiffRotation = FMath::Lerp(OffsetRotation, OtherOffsetRotation, OtherWeight);

	// Calculate the distance the camera should be, using the Location and the Pivot location
	// and blend those together
	const float Distance = FVector::Distance(Location, PivotLocation);
	const float OtherDistance = FVector::Distance(Other.Location, Other.PivotLocation);
	const float NewDistance = FMath::Lerp(Distance, OtherDistance, OtherWeight);

	PivotLocation = FMath::Lerp(PivotLocation, Other.PivotLocation, OtherWeight);

	// Use the blended pivot location, blended rotated offset, and the blended distance to calculate the new location
	Location = PivotLocation + DiffRotation.Vector() * NewDistance;

	const FRotator DeltaRotation = (Other.Rotation - Rotation).GetNormalized();
	Rotation = Rotation + (OtherWeight * DeltaRotation);

	const FRotator DeltaControlRotation = (Other.ControlRotation - ControlRotation).GetNormalized();
	ControlRotation = ControlRotation + (OtherWeight * DeltaControlRotation);

	FieldOfView = FMath::Lerp(FieldOfView, Other.FieldOfView, OtherWeight);
}
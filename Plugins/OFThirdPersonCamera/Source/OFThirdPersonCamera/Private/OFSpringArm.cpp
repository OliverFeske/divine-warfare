﻿// Copyright Epic Games, Inc. All Rights Reserved.

#include "OFSpringArm.h"
#include "CollisionQueryParams.h"
#include "WorldCollision.h"
#include "Engine/World.h"

//////////////////////////////////////////////////////////////////////////
// FSpringArm

void FOFSpringArm::UpdateDesiredArmLocation(const UWorld* WorldContext, const TArray<const AActor*>& IgnoreActors, const FTransform& InitialTransform, const FVector OffsetLocation, bool bDoTrace)
{
	FVector PivotLocation = InitialTransform.GetLocation();
	FRotator DesiredRot = InitialTransform.Rotator();

	// Recoverable error if the world is not provided, but an error should occur
	bool NeedsWorld = bDoTrace;
	ensureMsgf(!NeedsWorld || WorldContext != nullptr, TEXT("World is required for spring arm to trace against"));

	bDoTrace = bDoTrace && WorldContext != nullptr;
	
	// Get the spring arm 'origin', the target we want to look at
	FVector ArmOrigin = PivotLocation;
	FVector DesiredLoc = ArmOrigin;
	
	// Now offset camera position back along our rotation
	DesiredLoc -= DesiredRot.Vector() * TargetArmLength;
	// Add socket offset in local space
	DesiredLoc += FRotationMatrix(DesiredRot).TransformVector(OffsetLocation);

	// Do a sweep to ensure we are not penetrating the world
	FVector ResultLoc;
	if (bDoTrace && (TargetArmLength != 0.0f) && WorldContext != nullptr)
	{
		bIsCameraFixed = true;
		FCollisionQueryParams QueryParams(SCENE_QUERY_STAT(SpringArm), false);
		QueryParams.AddIgnoredActors(IgnoreActors);

		FHitResult Result;
		WorldContext->SweepSingleByChannel(Result, ArmOrigin, DesiredLoc, FQuat::Identity, ProbeChannel, FCollisionShape::MakeSphere(ProbeSize), QueryParams);
		
		UnfixedCameraPosition = DesiredLoc;

		ResultLoc = BlendLocations(DesiredLoc, Result.Location, Result.bBlockingHit);

		if (ResultLoc == DesiredLoc) 
		{	
			bIsCameraFixed = false;
		}
	}
	else
	{
		ResultLoc = DesiredLoc;
		bIsCameraFixed = false;
		UnfixedCameraPosition = ResultLoc;
	}

	CameraTransform.SetLocation(ResultLoc);
	CameraTransform.SetRotation(DesiredRot.Quaternion());

	StateIsValid = true;
}

void FOFSpringArm::Initialize()
{
	bIsCameraFixed = false;
	StateIsValid = false;
}

void FOFSpringArm::Tick(const UWorld* WorldContext, const AActor* IgnoreActor, const FTransform& InitialTransform, const FVector OffsetLocation)
{
	TArray<const AActor*> IgnoreActorArray;
	IgnoreActorArray.Add(IgnoreActor);
	Tick(WorldContext, IgnoreActorArray, InitialTransform, OffsetLocation);
}

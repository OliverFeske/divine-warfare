﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OFASCActorBase.h"
#include "OFItemInterface.h"
#include "OFItemTags.h"

#include "OFItem.generated.h"

class UGameplayEffect;

struct FOFAbilityInputCombo;

UCLASS(Abstract)
class OFINVENTORYSYSTEM_API AOFItem : public AOFASCActorBase, public IOFItemInterface
{
	GENERATED_BODY()
	
public:
	AOFItem();
	
protected:
	//~ Begin IOFItemInterface
	virtual void UpdateItemData_Implementation(const FOFItemData& NewItemData) override { ItemData = NewItemData; }
	virtual void GetItemData_Implementation(FOFItemData& OutItemData) const override { OutItemData = ItemData; }
	virtual bool CanBePickedUp_Implementation() const override { return ItemData.ItemTags.HasTag(TAG_ITEM_PICKUPPABLE); }
	virtual bool CanBeDropped_Implementation() const override { return ItemData.ItemTags.HasTag(TAG_ITEM_DROPPABLE); }
	virtual bool CanBeStacked_Implementation() const override { return ItemData.MaxStackSize > 1; }
	virtual bool CanBeConsumed_Implementation() const override { return ItemData.ItemTags.HasTag(TAG_ITEM_CONSUMABLE); }
	virtual bool CanBeEquipped_Implementation() const override { return ItemData.ItemTags.HasTag(TAG_ITEM_EQUIPPABLE); }
	virtual bool CanBeSold_Implementation() const override { return ItemData.ItemTags.HasTag(TAG_ITEM_SELLABLE); }
	virtual bool CanBeUsedForCrafting_Implementation() const override { return ItemData.ItemTags.HasTag(TAG_ITEM_CRAFTINGINGREDIENT); }
	virtual void OnPickUp_Implementation(AActor* InInstigator) override;
	virtual void OnDrop_Implementation(AActor* InInstigator) override;
	virtual void OnAddedToInventory_Implementation(AActor* InInstigator) override;
	virtual void OnRemovedFromInventory_Implementation(AActor* InInstigator) override;
	virtual void OnConsume_Implementation(AActor* InInstigator) override;
	virtual void OnEquip_Implementation(AActor* InInstigator) override;
	virtual void OnUnequip_Implementation(AActor* InInstigator) override;
	virtual void OnSell_Implementation(AActor* InInstigator) override;
	//~ End IOFItemInterface

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFItemBase")
	FOFItemData ItemData;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OFItemBase")
	FOFItemApplicationDataHolder ItemApplications;
};
﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#define TAG_ITEM_CONSUMABLE FGameplayTag::RequestGameplayTag(TEXT("Item.Consumable"), true)
#define TAG_ITEM_CRAFTINGINGREDIENT FGameplayTag::RequestGameplayTag(TEXT("Item.CraftingIngredient"), true)
#define TAG_ITEM_DROPPABLE FGameplayTag::RequestGameplayTag(TEXT("Item.Dropppable"), true)
#define TAG_ITEM_EQUIPPABLE FGameplayTag::RequestGameplayTag(TEXT("Item.Equippable"), true)
#define TAG_ITEM_PICKUPPABLE FGameplayTag::RequestGameplayTag(TEXT("Item.Pickuppable"), true)
#define TAG_ITEM_SELLABLE FGameplayTag::RequestGameplayTag(TEXT("Item.Sellable"), true)

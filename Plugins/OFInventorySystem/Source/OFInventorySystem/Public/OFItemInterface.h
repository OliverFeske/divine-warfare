﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OFItemData.h"
#include "UObject/Interface.h"
#include "OFItemInterface.generated.h"

UINTERFACE(Blueprintable)
class UOFItemInterface : public UInterface
{
	GENERATED_BODY()
};

class OFINVENTORYSYSTEM_API IOFItemInterface
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	void UpdateItemData(const FOFItemData& NewItemData);
	virtual void UpdateItemData_Implementation(const FOFItemData& NewItemData) {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	void GetItemData(FOFItemData& OutItemData) const;
	virtual void GetItemData_Implementation(FOFItemData& OutItemData) const {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	bool CanBePickedUp() const;
	virtual bool CanBePickedUp_Implementation() const { return false; }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	bool CanBeDropped() const;
	virtual bool CanBeDropped_Implementation() const { return false; }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	bool CanBeAddedToInventory() const;
	virtual bool CanBeAddedToInventory_Implementation() const { return false; }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	bool CanBeRemovedFromInventory() const;
	virtual bool CanBeRemovedFromInventory_Implementation() const { return false; }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	bool CanBeStacked() const;
	virtual bool CanBeStacked_Implementation() const { return false; }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	bool CanBeConsumed() const;
	virtual bool CanBeConsumed_Implementation() const { return false; }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	bool CanBeEquipped() const;
	virtual bool CanBeEquipped_Implementation() const { return false; }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	bool CanBeSold() const;
	virtual bool CanBeSold_Implementation() const { return false; }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	bool CanBeUsedForCrafting() const;
	virtual bool CanBeUsedForCrafting_Implementation() const { return false; }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	void OnPickUp(AActor* InInstigator);
	virtual void OnPickUp_Implementation(AActor* InInstigator) {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	void OnDrop(AActor* InInstigator);
	virtual void OnDrop_Implementation(AActor* InInstigator) {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	void OnAddedToInventory(AActor* InInstigator);
	virtual void OnAddedToInventory_Implementation(AActor* InInstigator) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	void OnRemovedFromInventory(AActor* Instigator);
	virtual void OnRemovedFromInventory_Implementation(AActor* Instigator) {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	void OnConsume(AActor* InInstigator);
	virtual void OnConsume_Implementation(AActor* InInstigator) {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	void OnEquip(AActor* InInstigator);
	virtual void OnEquip_Implementation(AActor* InInstigator) {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	void OnUnequip(AActor* InInstigator);
	virtual void OnUnequip_Implementation(AActor* InInstigator) {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemInterface")
	void OnSell(AActor* InInstigator);
	virtual void OnSell_Implementation(AActor* InInstigator) {}
};
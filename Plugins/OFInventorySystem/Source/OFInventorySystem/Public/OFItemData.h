﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "OFGASUtilities.h"
#include "OFItemData.generated.h"

class UGameplayEffect;
class UGameplayAbility;

struct FOFAbilityInputCombo;

USTRUCT(BlueprintType)
struct FOFItemData
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Name = FString(TEXT("None"));
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Description = FString(TEXT("None"));
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UTexture2D* Thumbnail = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (GameplayTagFilter = "Item"))
	FGameplayTagContainer ItemTags = FGameplayTagContainer();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 MaxStackSize = 1;
};

USTRUCT(BlueprintType)
struct FOFItemApplicationDataHolder
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FOFGenericGASApplicationData ApplyOnPickUp;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FOFGenericGASApplicationData ApplyOnDrop;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FOFGenericGASApplicationData ApplyOnAddedToInventory;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FOFGenericGASApplicationData ApplyOnRemovedFromInventory;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FOFGenericGASApplicationData ApplyOnConsume;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FOFGenericGASApplicationData ApplyOnEquip;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FOFGenericGASApplicationData ApplyOnUnequip;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FOFGenericGASApplicationData ApplyOnSell;
};
﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "UObject/Interface.h"
#include "OFItemHolderInterface.generated.h"

class IOFItemInterface;

UINTERFACE(Blueprintable)
class UOFItemHolderInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 *	
 */
class OFINVENTORYSYSTEM_API IOFItemHolderInterface
{
	GENERATED_BODY()
	
	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemHolderInterface")
	void GetItems(TArray<TScriptInterface<IOFItemInterface>>& OutItems) const;
	virtual void GetItems_Implementation(TArray<TScriptInterface<IOFItemInterface>>& OutItems) const {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemHolderInterface")
	bool AddItem(const TScriptInterface<IOFItemInterface>& ItemToAdd);
	virtual bool AddItem_Implementation(const TScriptInterface<IOFItemInterface>& ItemToAdd) { return false; }

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemHolderInterface")
	bool RemoveItem(const TScriptInterface<IOFItemInterface>& ItemToRemove);
	virtual bool RemoveItem_Implementation(const TScriptInterface<IOFItemInterface>& ItemToRemove) { return false; }

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemHolderInterface")
	bool ContainsItem(const TScriptInterface<IOFItemInterface>& Item) const;
	virtual bool ContainsItem_Implementation(const TScriptInterface<IOFItemInterface>& Item) const = 0;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemHolderInterface")
	void GetItemsOfClass(TSubclassOf<UObject> ItemClass, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const;
	virtual void GetItemsOfClass_Implementation(TSubclassOf<UObject> ItemClass, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemHolderInterface")
	void GetItemsWithTag(FGameplayTag ItemTag, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const;
	virtual void GetItemsWithTag_Implementation(FGameplayTag ItemTag, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemHolderInterface")
	void GetItemsWithAnyTag(FGameplayTagContainer ItemTags, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const;
	virtual void GetItemsWithAnyTag_Implementation(FGameplayTagContainer ItemTags, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemHolderInterface")
	void GetItemsWithAllTags(FGameplayTagContainer ItemTags, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const;
	virtual void GetItemsWithAllTags_Implementation(FGameplayTagContainer ItemTags, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemHolderInterface")
	void GetCurrentEquippedItem(TScriptInterface<IOFItemInterface>& EquippedItem) const;
	virtual void GetCurrentEquippedItem_Implementation(TScriptInterface<IOFItemInterface>& EquippedItem) const {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFItemHolderInterface")
	void SetCurrentEquippedItem(const TScriptInterface<IOFItemInterface>& ItemToEquip);
	virtual void SetCurrentEquippedItem_Implementation(const TScriptInterface<IOFItemInterface>& ItemToEquip) { }
};
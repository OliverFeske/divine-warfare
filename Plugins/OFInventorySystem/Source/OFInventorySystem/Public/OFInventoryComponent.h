﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "OFItemHolderInterface.h"
#include "Components/ActorComponent.h"
#include "OFInventoryComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class OFINVENTORYSYSTEM_API UOFInventoryComponent : public UActorComponent, public IOFItemHolderInterface
{
	GENERATED_BODY()
	
public:
	UOFInventoryComponent();
	
	//~ Begin IOFItemHolderInterface
	virtual void GetItems_Implementation(TArray<TScriptInterface<IOFItemInterface>>& OutItems) const override { OutItems = ItemsInInventory; }
	virtual bool AddItem_Implementation(const TScriptInterface<IOFItemInterface>& ItemToAdd) override;
	virtual bool RemoveItem_Implementation(const TScriptInterface<IOFItemInterface>& ItemToRemove) override;
	virtual bool ContainsItem_Implementation(const TScriptInterface<IOFItemInterface>& Item) const override { return ItemsInInventory.Contains(Item); }
	virtual void GetItemsOfClass_Implementation(TSubclassOf<UObject> ItemClass, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const override;
	virtual void GetItemsWithTag_Implementation(FGameplayTag ItemTag, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const override;
	virtual void GetItemsWithAnyTag_Implementation(FGameplayTagContainer ItemTags, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const override;
	virtual void GetItemsWithAllTags_Implementation(FGameplayTagContainer ItemTags, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const override;
	//~ End IOFItemHolderInterface
	
protected:
	UPROPERTY(BlueprintReadOnly)
	TArray<TScriptInterface<IOFItemInterface>> ItemsInInventory;
};
﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFItem.h"

#include "OFGASUtilities.h"

AOFItem::AOFItem()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AOFItem::OnPickUp_Implementation(AActor* InInstigator)
{
	FOFGenericGASApplicationDataInfo GenericGASApplicationDataInfo;
	UOFGASUtilities::ApplyGASApplicationDataToTarget(this, InInstigator, this, InInstigator,
		ItemApplications.ApplyOnPickUp, GenericGASApplicationDataInfo);
	
#if WITH_EDITOR
	UOFGASUtilities::PrintGenericGASApplicationDataToLog(GenericGASApplicationDataInfo);
#endif
}

void AOFItem::OnDrop_Implementation(AActor* InInstigator)
{
	FOFGenericGASApplicationDataInfo GenericGASApplicationDataInfo;
	UOFGASUtilities::ApplyGASApplicationDataToTarget(this, InInstigator, this, InInstigator,
		ItemApplications.ApplyOnDrop, GenericGASApplicationDataInfo);
	
#if WITH_EDITOR
	UOFGASUtilities::PrintGenericGASApplicationDataToLog(GenericGASApplicationDataInfo);
#endif
}

void AOFItem::OnAddedToInventory_Implementation(AActor* InInstigator)
{
	FOFGenericGASApplicationDataInfo GenericGASApplicationDataInfo;
	UOFGASUtilities::ApplyGASApplicationDataToTarget(this, InInstigator, this, InInstigator,
		ItemApplications.ApplyOnAddedToInventory, GenericGASApplicationDataInfo);
	
#if WITH_EDITOR
	UOFGASUtilities::PrintGenericGASApplicationDataToLog(GenericGASApplicationDataInfo);
#endif
}

void AOFItem::OnRemovedFromInventory_Implementation(AActor* InInstigator)
{
	FOFGenericGASApplicationDataInfo GenericGASApplicationDataInfo;
	UOFGASUtilities::ApplyGASApplicationDataToTarget(this, InInstigator, this, InInstigator,
		ItemApplications.ApplyOnRemovedFromInventory, GenericGASApplicationDataInfo);
	
#if WITH_EDITOR
	UOFGASUtilities::PrintGenericGASApplicationDataToLog(GenericGASApplicationDataInfo);
#endif
}

void AOFItem::OnConsume_Implementation(AActor* InInstigator)
{
	FOFGenericGASApplicationDataInfo GenericGASApplicationDataInfo;
	UOFGASUtilities::ApplyGASApplicationDataToTarget(this, InInstigator, this, InInstigator,
		ItemApplications.ApplyOnConsume, GenericGASApplicationDataInfo);
	
#if WITH_EDITOR
	UOFGASUtilities::PrintGenericGASApplicationDataToLog(GenericGASApplicationDataInfo);
#endif
}

void AOFItem::OnEquip_Implementation(AActor* InInstigator)
{
	FOFGenericGASApplicationDataInfo GenericGASApplicationDataInfo;
	UOFGASUtilities::ApplyGASApplicationDataToTarget(this, InInstigator, this, InInstigator,
		ItemApplications.ApplyOnEquip, GenericGASApplicationDataInfo);
	
#if WITH_EDITOR
	UOFGASUtilities::PrintGenericGASApplicationDataToLog(GenericGASApplicationDataInfo);
#endif
}

void AOFItem::OnUnequip_Implementation(AActor* InInstigator)
{
	FOFGenericGASApplicationDataInfo GenericGASApplicationDataInfo;
	UOFGASUtilities::ApplyGASApplicationDataToTarget(this, InInstigator, this, InInstigator,
		ItemApplications.ApplyOnUnequip, GenericGASApplicationDataInfo);
	
#if WITH_EDITOR
	UOFGASUtilities::PrintGenericGASApplicationDataToLog(GenericGASApplicationDataInfo);
#endif
}

void AOFItem::OnSell_Implementation(AActor* InInstigator)
{
	FOFGenericGASApplicationDataInfo GenericGASApplicationDataInfo;
	UOFGASUtilities::ApplyGASApplicationDataToTarget(this, InInstigator, this, InInstigator,
		ItemApplications.ApplyOnSell, GenericGASApplicationDataInfo);
	
#if WITH_EDITOR
	UOFGASUtilities::PrintGenericGASApplicationDataToLog(GenericGASApplicationDataInfo);
#endif
}
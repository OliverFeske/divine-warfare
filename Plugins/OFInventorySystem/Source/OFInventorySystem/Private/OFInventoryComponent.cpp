﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFInventoryComponent.h"

#include "GameplayTagContainer.h"
#include "OFItemData.h"
#include "OFItemInterface.h"

UOFInventoryComponent::UOFInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

bool UOFInventoryComponent::AddItem_Implementation(const TScriptInterface<IOFItemInterface>& ItemToAdd)
{
	ItemsInInventory.Remove(nullptr);
	if(!ItemToAdd) { return false; }
	
	ItemsInInventory.Add(ItemToAdd);
	IOFItemInterface::Execute_OnAddedToInventory(ItemToAdd.GetObject(), GetOwner());
	return true;
}

bool UOFInventoryComponent::RemoveItem_Implementation(const TScriptInterface<IOFItemInterface>& ItemToRemove)
{
	ItemsInInventory.Remove(nullptr);
	if(!ItemToRemove) { return false; }
	
	if(ItemsInInventory.Remove(ItemToRemove) > 0)
	{
		IOFItemInterface::Execute_OnRemovedFromInventory(ItemToRemove.GetObject(), GetOwner());
		return true;
	}
	
	return false;
}

void UOFInventoryComponent::GetItemsOfClass_Implementation(TSubclassOf<UObject> ItemClass, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const
{
	OutItems.Reset();
	
	for(const TScriptInterface<IOFItemInterface>& Item : ItemsInInventory)
	{
		if(const UObject* Object = Item.GetObject())
		{
			if(Object->IsA(ItemClass))
			{
				OutItems.AddUnique(Item);
			}
		}
	}
}

void UOFInventoryComponent::GetItemsWithTag_Implementation(FGameplayTag ItemTag, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const
{
	OutItems.Reset();
	
	for(const TScriptInterface<IOFItemInterface>& Item : ItemsInInventory)
	{
		if(Item.GetInterface())
		{
			FOFItemData ItemData;
			Item->Execute_GetItemData(Item.GetObject(), ItemData);
			const FGameplayTagContainer& GameplayTags = ItemData.ItemTags;
			if(bMatchExact ? GameplayTags.HasTagExact(ItemTag) : GameplayTags.HasTag(ItemTag))
			{
				OutItems.AddUnique(Item);
			}
		}
	}
}

void UOFInventoryComponent::GetItemsWithAnyTag_Implementation(FGameplayTagContainer ItemTags, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const
{
	OutItems.Reset();
	
	for(const TScriptInterface<IOFItemInterface>& Item : ItemsInInventory)
	{
		if(Item.GetInterface())
		{
			FOFItemData ItemData;
			Item->Execute_GetItemData(Item.GetObject(), ItemData);
			const FGameplayTagContainer& GameplayTags = ItemData.ItemTags;
			if(bMatchExact ? GameplayTags.HasAnyExact(ItemTags) : GameplayTags.HasAny(ItemTags))
			{
				OutItems.AddUnique(Item);
			}
		}
	}
}

void UOFInventoryComponent::GetItemsWithAllTags_Implementation(FGameplayTagContainer ItemTags, const bool bMatchExact, TArray<TScriptInterface<IOFItemInterface>>& OutItems) const
{
	OutItems.Reset();
	
	for(TScriptInterface<IOFItemInterface> Item : ItemsInInventory)
	{
		if(Item.GetInterface())
		{
			FOFItemData ItemData;
			Item->Execute_GetItemData(Item.GetObject(), ItemData);
			const FGameplayTagContainer& GameplayTags = ItemData.ItemTags;
			if(bMatchExact ? GameplayTags.HasAllExact(ItemTags) : GameplayTags.HasAll(ItemTags))
			{
				OutItems.AddUnique(Item);
			}
		}
	}
}
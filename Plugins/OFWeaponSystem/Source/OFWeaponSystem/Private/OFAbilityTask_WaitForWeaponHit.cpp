﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFAbilityTask_WaitForWeaponHit.h"

UOFAbilityTask_WaitForWeaponHit* UOFAbilityTask_WaitForWeaponHit::WaitForWeaponHit(UGameplayAbility* OwningAbility, FName TaskInstanceName,
	TScriptInterface<IOFWeaponInterface> Weapon, const bool bOnlyFireOnce)
{
	UOFAbilityTask_WaitForWeaponHit* NewTask = NewAbilityTask<UOFAbilityTask_WaitForWeaponHit>(OwningAbility, TaskInstanceName);
	NewTask->ObservedWeapon = Weapon;
	NewTask->bShouldOnlyFireOnce = bOnlyFireOnce;

	return NewTask;
}

void UOFAbilityTask_WaitForWeaponHit::Activate()
{
	if(IOFWeaponInterface* WeaponInterface = ObservedWeapon.GetInterface())
	{
		WeaponInterface->GetOnWeaponHitDelegate().AddDynamic(this, &UOFAbilityTask_WaitForWeaponHit::Func_OnWeaponHit);
	}
	else
	{
		EndTask();
	}
	
	Super::Activate();
}

void UOFAbilityTask_WaitForWeaponHit::OnDestroy(bool bInOwnerFinished)
{
	if(IOFWeaponInterface* WeaponInterface = ObservedWeapon.GetInterface())
	{
		WeaponInterface->GetOnWeaponHitDelegate().RemoveDynamic(this, &UOFAbilityTask_WaitForWeaponHit::Func_OnWeaponHit);
	}
	
	Super::OnDestroy(bInOwnerFinished);
}

void UOFAbilityTask_WaitForWeaponHit::Func_OnWeaponHit(UPrimitiveComponent* WeaponCollision, const FHitResult& HitResult, const FGameplayEventData& AdditionalData)
{
	OnWeaponHit.Broadcast(WeaponCollision, HitResult, AdditionalData);

	if(bShouldOnlyFireOnce)
	{
		if(IOFWeaponInterface* WeaponInterface = ObservedWeapon.GetInterface())
		{
			WeaponInterface->GetOnWeaponHitDelegate().RemoveDynamic(this, &UOFAbilityTask_WaitForWeaponHit::Func_OnWeaponHit);
		}
		EndTask();
	}
}

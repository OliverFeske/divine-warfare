﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OFAsyncTask_WaitWeaponHit.h"

void UOFAsyncTask_WaitWeaponHit::Activate()
{
	if(IOFWeaponInterface* WeaponInterface = ObservedWeapon.GetInterface())
	{
		WeaponInterface->GetOnWeaponHitDelegate().AddDynamic(this, &UOFAsyncTask_WaitWeaponHit::Func_OnWeaponHit);
	}
	else
	{
		SetReadyToDestroy();
	}
	
	Super::Activate();
}

void UOFAsyncTask_WaitWeaponHit::BeginDestroy()
{
	if(IOFWeaponInterface* WeaponInterface = ObservedWeapon.GetInterface())
	{
		WeaponInterface->GetOnWeaponHitDelegate().RemoveDynamic(this, &UOFAsyncTask_WaitWeaponHit::Func_OnWeaponHit);
	}
	
	Super::BeginDestroy();
}

UOFAsyncTask_WaitWeaponHit* UOFAsyncTask_WaitWeaponHit::WaitForWeaponHit(TScriptInterface<IOFWeaponInterface> Weapon, const bool bOnlyFireOnce)
{
	UOFAsyncTask_WaitWeaponHit* NewTask = NewObject<UOFAsyncTask_WaitWeaponHit>();
	NewTask->ObservedWeapon = Weapon;
	NewTask->bShouldOnlyFireOnce = bOnlyFireOnce;
	
	return NewTask;
}

void UOFAsyncTask_WaitWeaponHit::Func_OnWeaponHit(UPrimitiveComponent* WeaponCollision, const FHitResult& HitResult,
	const FGameplayEventData& AdditionalData)
{
	OnWeaponHit.Broadcast(WeaponCollision, HitResult, AdditionalData);

	if(bShouldOnlyFireOnce)
	{
		if(IOFWeaponInterface* WeaponInterface = ObservedWeapon.GetInterface())
		{
			WeaponInterface->GetOnWeaponHitDelegate().RemoveDynamic(this, &UOFAsyncTask_WaitWeaponHit::Func_OnWeaponHit);
		}
		SetReadyToDestroy();
	}
}
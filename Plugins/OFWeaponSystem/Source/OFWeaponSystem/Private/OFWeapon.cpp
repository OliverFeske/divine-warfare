﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "OFWeapon.h"

#include "OFAbilitySystemComponent.h"
#include "OFWeaponTags.h"

AOFWeapon::AOFWeapon()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AOFWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	
	if(!ensure(AbilitySystemComponent)) { return; }
	
	AttackStartHandle = AbilitySystemComponent->GenericGameplayEventCallbacks.FindOrAdd(TAG_ANIMEVENT_WEAPON_ATTACK_START).AddUObject(this, &AOFWeapon::OnAttackStartEventReceived);
	AttackEndHandle = AbilitySystemComponent->GenericGameplayEventCallbacks.FindOrAdd(TAG_ANIMEVENT_WEAPON_ATTACK_END).AddUObject(this, &AOFWeapon::OnAttackEndEventReceived);
}

void AOFWeapon::BeginDestroy()
{
	if(AbilitySystemComponent)
	{
		AbilitySystemComponent->GenericGameplayEventCallbacks.Remove(TAG_ANIMEVENT_WEAPON_ATTACK_START);
		AbilitySystemComponent->GenericGameplayEventCallbacks.Remove(TAG_ANIMEVENT_WEAPON_ATTACK_END);
	}
	
	Super::BeginDestroy();
}

void AOFWeapon::BroadcastOnWeaponDamage_Implementation(UPrimitiveComponent* WeaponCollision, const FHitResult& HitResult, const FGameplayEventData& AdditionalData)
{
	OnWeaponHit.Broadcast(WeaponCollision, HitResult, AdditionalData);
}

void AOFWeapon::OnAttackStartEventReceived(const FGameplayEventData* Payload)
{
	if(!ensure(Payload)) { return; }
	
	IOFWeaponInterface::Execute_OnAttackStarted(this, *Payload);
}

void AOFWeapon::OnAttackEndEventReceived(const FGameplayEventData* Payload)
{
	if(!ensure(Payload)) { return; }
	
	IOFWeaponInterface::Execute_OnAttackEnded(this, *Payload);
}


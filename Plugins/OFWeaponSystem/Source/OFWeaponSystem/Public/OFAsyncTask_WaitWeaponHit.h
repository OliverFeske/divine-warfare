﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OFWeaponInterface.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "OFAsyncTask_WaitWeaponHit.generated.h"

UCLASS()
class OFWEAPONSYSTEM_API UOFAsyncTask_WaitWeaponHit : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()
	
public:
	//~ Begin UBlueprintAsyncActionBase Interface
	virtual void Activate() override;
	virtual void BeginDestroy() override;
	//~ End UBlueprintAsyncActionBase Interface

	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true"))
	static UOFAsyncTask_WaitWeaponHit* WaitForWeaponHit(TScriptInterface<IOFWeaponInterface> Weapon, const bool bOnlyFireOnce = false);
	
protected:
	UFUNCTION()
	void Func_OnWeaponHit(UPrimitiveComponent* WeaponCollision, const FHitResult& HitResult, const FGameplayEventData& AdditionalData);
	
	UPROPERTY(BlueprintAssignable)
	FOnWeaponHit OnWeaponHit;
	
	UPROPERTY()
	TScriptInterface<IOFWeaponInterface> ObservedWeapon = nullptr;

	bool bShouldOnlyFireOnce = false;
};
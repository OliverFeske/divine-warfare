﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Abilities/GameplayAbilityTypes.h"
#include "OFWeaponInterface.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnWeaponHit, UPrimitiveComponent*, WeaponCollision, const FHitResult&, HitResult,
	const FGameplayEventData&, AdditionalData);

class UDataTable;

// This class does not need to be modified.
UINTERFACE(Blueprintable)
class UOFWeaponInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 *	
 */
class OFWEAPONSYSTEM_API IOFWeaponInterface
{
	GENERATED_BODY()
	
	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual FOnWeaponHit& GetOnWeaponHitDelegate() = 0;
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFWeaponInterface")
	void BroadcastOnWeaponDamage(UPrimitiveComponent* WeaponCollision, const FHitResult& HitResult, const FGameplayEventData& AdditionalData);
	virtual void BroadcastOnWeaponDamage_Implementation(UPrimitiveComponent* WeaponCollision, const FHitResult& HitResult, const FGameplayEventData& AdditionalData) = 0;
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFWeaponInterface")
	float GetDamage() const;
	virtual float GetDamage_Implementation() const { return 0.f; }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFWeaponInterface")
	float GetDamageByName(const FString& DamageName) const;
	virtual float GetDamageByName_Implementation(const FString& DamageName) const { return 0.f; }
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFWeaponInterface")
	void OnAttackStarted(const FGameplayEventData& EventData);
	virtual void OnAttackStarted_Implementation(const FGameplayEventData& EventData) {}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OFWeaponInterface")
	void OnAttackEnded(const FGameplayEventData& EventData);
	virtual void OnAttackEnded_Implementation(const FGameplayEventData& EventData) {}
};

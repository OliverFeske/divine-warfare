﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OFItem.h"
#include "OFWeaponInterface.h"
#include "OFWeapon.generated.h"

DECLARE_DYNAMIC_DELEGATE(FTestDelegate);

UCLASS()
class OFWEAPONSYSTEM_API AOFWeapon : public AOFItem, public IOFWeaponInterface
{
	GENERATED_BODY()
	
public:
	AOFWeapon();
	
	//~ Begin UObject Interface
	virtual void BeginDestroy() override;
	//~ End UObject Interface
	
	//~ Begin AActor Interface
	virtual void PostInitializeComponents() override;
	//~ End AActor Interface

	//~ Begin IOFWeaponInterface
	virtual FOnWeaponHit& GetOnWeaponHitDelegate() override { return OnWeaponHit; }
	virtual void BroadcastOnWeaponDamage_Implementation(UPrimitiveComponent* WeaponCollision, const FHitResult& HitResult,
		const FGameplayEventData& AdditionalData) override;
	//~ End IOFWeaponInterface
	
protected:
	void OnAttackStartEventReceived(const FGameplayEventData* Payload);
	void OnAttackEndEventReceived(const FGameplayEventData* Payload);
	
	FDelegateHandle AttackStartHandle;
	FDelegateHandle AttackEndHandle;

	UPROPERTY()
	FOnWeaponHit OnWeaponHit;
};

﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OFWeaponInterface.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "OFAbilityTask_WaitForWeaponHit.generated.h"

/**
 * 
 */
UCLASS()
class OFWEAPONSYSTEM_API UOFAbilityTask_WaitForWeaponHit : public UAbilityTask
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = "Ability|Tasks", meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "true"))
	static UOFAbilityTask_WaitForWeaponHit* WaitForWeaponHit(UGameplayAbility* OwningAbility, FName TaskInstanceName,
		TScriptInterface<IOFWeaponInterface> Weapon, const bool bOnlyFireOnce = false);

	//~ Begin UAbilityTask Interface
	virtual void Activate() override;
	virtual void OnDestroy(bool bInOwnerFinished) override;
	//~ End UAbilityTask Interface
	
protected:
	UFUNCTION()
	void Func_OnWeaponHit(UPrimitiveComponent* WeaponCollision, const FHitResult& HitResult, const FGameplayEventData& AdditionalData);

	UPROPERTY(BlueprintAssignable)
	FOnWeaponHit OnWeaponHit;
	
	UPROPERTY()
	TScriptInterface<IOFWeaponInterface> ObservedWeapon = nullptr;

	bool bShouldOnlyFireOnce = false;
};

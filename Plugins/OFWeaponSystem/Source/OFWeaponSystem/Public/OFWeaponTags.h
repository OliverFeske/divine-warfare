﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#define TAG_ANIMEVENT_WEAPON_ATTACK_START FGameplayTag::RequestGameplayTag(TEXT("AnimEvent.Weapon.Attack.Start"), true)
#define TAG_ANIMEVENT_WEAPON_ATTACK_END FGameplayTag::RequestGameplayTag(TEXT("AnimEvent.Weapon.Attack.End"), true)
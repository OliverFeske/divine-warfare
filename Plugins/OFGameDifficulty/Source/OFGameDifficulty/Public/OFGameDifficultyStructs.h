﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

UENUM(BlueprintType)
enum class EOFGameDifficulty : uint8
{
	/*
	 *	This is mostly used for things that do not differ between difficulties.
	 */
	Shared,
	
	/**
	 *	The easiest difficulty. Designed for those who just want to experience the story.
	 */
	Story,
	
	/**
	 *	Designed for those who want to enjoy the story, but also like a small amount of challenges.
	 */
	Easy,
	
	/**
	 *	Challenging, but forgiving.
	 */
	Normal,
	
	/**
	 *	Challenging.
	 */
	Hard,
	
	/**
	 *	Even the easiest mobs will kill you if you don't care!
	 */
	VeryHard
};
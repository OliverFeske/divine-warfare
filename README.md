# Divine-Warfare

This is just a small part of the project for storage reasons. It will contain all of the neccesary features though.
The actual project is hosted on a local perforce server, but I will update this repository, as soon as there are some major changes to the main project.

To run this project you will need a source build of UE5 Preview or the prebuilt Early Access 2 version from the Epic Launcher.

# This version is currently stopped due to the release of UE5 and the new Lyra project. My current process is to cherry pick features from the Lyra project and converting them into reusable plugins. As soon as this process is finished there will be further updates.
